package dk.s4.phg.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.google.protobuf.MessageLite;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import dk.s4.phg.baseplate.ApplicationState;
import dk.s4.phg.baseplate.Callback;
import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.baseplate.EventImplementation;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.MetaData;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.baseplate.Peer;
import dk.s4.phg.baseplate.master.ReflectorContext;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.Bond;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.BondSuccess;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.SetStandardPasskey;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.Unbond;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.UnbondSuccess;
import dk.s4.phg.messages.interfaces.device_control.Search;
import dk.s4.phg.messages.interfaces.device_control.SearchInitiated;
import dk.s4.phg.messages.interfaces.device_control.SearchResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({BluetoothAdapter.class, Log.class, IntentFilterFactory.class})
public class BluetoothModuleTest {
    private BluetoothModule bluetoothModule;
    private callableModuleBase callableModule;

    @Before
    public void setup() {
        PowerMockito.mockStatic(BluetoothAdapter.class);
        when(BluetoothAdapter.getDefaultAdapter()).thenReturn(mock(BluetoothAdapter.class));

        PowerMockito.mockStatic(Log.class);
        when(Log.d(anyString(), anyString())).then(invocation -> {
            System.out.println(invocation.getArgument(0) + " " + invocation.getArgument(1));
            return 42;
        });

        PowerMockito.mockStatic(IntentFilterFactory.class);
        when(IntentFilterFactory.classic()).thenReturn(mock(IntentFilter.class));

        Activity activity = mock(Activity.class);
        when(activity.getApplicationContext()).thenReturn(mock(android.content.Context.class));

        Context context = new ReflectorContext(42);

        this.bluetoothModule = new BluetoothModule(context, activity);
        this.callableModule = new callableModuleBase(context, "TestModule");
    }

    @Test
    public void isInitialized() {
        assertThat(bluetoothModule).isNotNull();
        assertThat(bluetoothModule.getApplicationState()).isEqualTo(ApplicationState.INITIALIZING);

        assertThat(callableModule).isNotNull();
        assertThat(callableModule.getApplicationState()).isEqualTo(ApplicationState.INITIALIZING);
    }

    @Test
    public void callBluetoothBondWithoutDevice() throws InterruptedException {
        AtomicReference<CoreError> errorReference = new AtomicReference<>();
        CountDownLatch latch = new CountDownLatch(1);
        Callback<BondSuccess> callback = new Callback<BondSuccess>() {
            @Override
            public void success(BondSuccess payload, boolean lastCall) {
                System.out.println("Should fail");
            }

            @Override
            public void error(CoreError error, boolean lastCall) {
                errorReference.set(error);
                latch.countDown();
            }
        };

        callableModule.bluetoothDeviceControlCall("Bond", Bond.newBuilder().setHandle(42).build(), bluetoothModule.getId(), callback);
        latch.await(5000, TimeUnit.SECONDS);
        assertThat(errorReference.get().message).contains("unknown device");
    }

    @Test
    public void callBluetoothUnbondWithoutDevice() throws InterruptedException {
        AtomicReference<CoreError> errorReference = new AtomicReference<>();
        CountDownLatch latch = new CountDownLatch(1);
        Callback<UnbondSuccess> callback = new Callback<UnbondSuccess>() {
            @Override
            public void success(UnbondSuccess payload, boolean lastCall) {
                System.out.println("Should fail");
            }

            @Override
            public void error(CoreError error, boolean lastCall) {
                errorReference.set(error);
                latch.countDown();
            }
        };

        callableModule.bluetoothDeviceControlCall("Unbond", Unbond.newBuilder().setHandle(42).build(), bluetoothModule.getId(), callback);
        latch.await(5000, TimeUnit.SECONDS);
        assertThat(errorReference.get().message).contains("unknown device");
    }

    @Test
    public void callSearch() throws InterruptedException {
        CountDownLatch searchInitatedLatch = new CountDownLatch(1);
        callableModule.registerSearchInitiatedListener(() -> {
            System.out.println("SearchInitiated");
            searchInitatedLatch.countDown();
        });
        callableModule.deviceControlPostEvent("Search", Search.newBuilder().setSearchId(42).build(), bluetoothModule.getId());
        searchInitatedLatch.await(5000, TimeUnit.SECONDS);
    }

    @Test
    public void callSearchAndCreateDevice() throws InterruptedException {
        AtomicLong atomicHandle = new AtomicLong();
        CountDownLatch searchInitiatedLatch = new CountDownLatch(1);
        callableModule.registerSearchInitiatedListener(() -> {
            System.out.println("SearchInitiated");
            searchInitiatedLatch.countDown();
        });
        CountDownLatch searchResultLatch = new CountDownLatch(1);
        callableModule.registerSearchResultListener((searchResult) -> {
            long handle = searchResult.getResult().getDeviceTransport().getHandle();
            System.out.println("SearchResultId: " + searchResult.getSearchId() + ", handle: " + handle);
            atomicHandle.set(handle);
            searchResultLatch.countDown();
        });
        callableModule.deviceControlPostEvent("Search", Search.newBuilder().setSearchId(42).build(), bluetoothModule.getId());
        searchInitiatedLatch.await(5000, TimeUnit.SECONDS);
        bluetoothModule.discoverNewDevice(null, intentWith(BluetoothDevice.ACTION_FOUND));
        searchResultLatch.await(5000, TimeUnit.SECONDS);
    }

    @Test
    public void callSetPasskeyWithoutDevice() {
        callableModule.bluetoothDeviceControlPostEvent("SetStandardPasskey", SetStandardPasskey.newBuilder().setHandle(42).build(), bluetoothModule.getId());
    }

    private Intent intentWith(String action) {
        BluetoothDevice device = aDevice();
        Intent intent = mock(Intent.class);
        when(intent.getAction()).thenReturn(action);
        when(intent.getParcelableExtra(anyString())).thenReturn(device);
        return intent;
    }

    private BluetoothDevice aDevice() {
        BluetoothDevice device = mock(BluetoothDevice.class);
        when(device.getAddress()).thenReturn("somedeviceadress");
        return device;
    }

    static class callableModuleBase extends ModuleBase {
        private final InterfaceEndpoint bluetoothDeviceControlEndpoint;
        private final InterfaceEndpoint deviceControlEndpoint;

        private final List<Runnable> searchInitiatedListeners = new ArrayList<>();
        private final List<Consumer<SearchResult>> searchResultListeners = new ArrayList<>();

        protected callableModuleBase(Context context, String moduleTag) {
            super(context, moduleTag);
            bluetoothDeviceControlEndpoint = implementsConsumerInterface("BluetoothDeviceControl");
            deviceControlEndpoint = implementsConsumerInterface("DeviceControl");
            deviceControlEndpoint.addEventHandler("SearchInitiated", new EventImplementation<SearchInitiated>() {
                @Override
                public void accept(SearchInitiated args, MetaData meta) {
                    searchInitiated();
                }
            });
            deviceControlEndpoint.addEventHandler("SearchResult", new EventImplementation<SearchResult>() {
                @Override
                public void accept(SearchResult args, MetaData meta) {
                    searchResult(args);
                }
            });

            start();
        }

        public void registerSearchResultListener(Consumer<SearchResult> consumer) {
            searchResultListeners.add(consumer);
        }

        public void searchResult(SearchResult searchResult) {
            for (Consumer<SearchResult> listener : searchResultListeners) {
                listener.accept(searchResult);
            }
        }

        public void registerSearchInitiatedListener(Runnable r) {
            searchInitiatedListeners.add(r);
        }

        public void searchInitiated() {
            for (Runnable searchInitiatedListener : searchInitiatedListeners) {
                searchInitiatedListener.run();
            }
        }

        public <A extends MessageLite, R extends MessageLite> void bluetoothDeviceControlCall(String functionName, A args, Peer receiver, Callback<? super R> callback) {
            bluetoothDeviceControlEndpoint.callFunction(functionName, args, receiver, callback);
        }

        public <A extends MessageLite> void deviceControlPostEvent(String functionName, A args, Peer receiver) {
            deviceControlEndpoint.postEvent(functionName, args, receiver);
        }

        public <A extends MessageLite> void bluetoothDeviceControlPostEvent(String functionName, A args, Peer receiver) {
            bluetoothDeviceControlEndpoint.postEvent(functionName, args, receiver);
        }
    }
}