package dk.s4.phg.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.IntentFilter;
import android.util.Log;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import dk.s4.phg.baseplate.ApplicationState;
import dk.s4.phg.baseplate.Callback;
import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.baseplate.Peer;
import dk.s4.phg.baseplate.master.ReflectorContext;
import dk.s4.phg.messages.interfaces.serial_port.Close;
import dk.s4.phg.messages.interfaces.serial_port.Open;
import dk.s4.phg.messages.interfaces.serial_port.OpenSuccess;
import dk.s4.phg.messages.interfaces.serial_port.Write;
import dk.s4.phg.messages.interfaces.serial_port.WriteSuccess;
import dk.s4.phg.messages.support.serial_port.SerialPortRepresentation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({BluetoothAdapter.class, Log.class, IntentFilterFactory.class})
public class SerialPortModuleTest {
    private BluetoothModule bluetoothModule;
    private callableModuleBase callableModule;

    @Before
    public void setup() {
        PowerMockito.mockStatic(BluetoothAdapter.class);
        when(BluetoothAdapter.getDefaultAdapter()).thenReturn(mock(BluetoothAdapter.class));

        PowerMockito.mockStatic(Log.class);
        when(Log.d(anyString(), anyString())).then(invocation -> {
            System.out.println(invocation.getArgument(0) + " " + invocation.getArgument(1));
            return 42;
        });

        PowerMockito.mockStatic(IntentFilterFactory.class);
        when(IntentFilterFactory.classic()).thenReturn(mock(IntentFilter.class));

        Activity activity = mock(Activity.class);
        when(activity.getApplicationContext()).thenReturn(mock(android.content.Context.class));

        Context context = new ReflectorContext(42);

        this.bluetoothModule = new BluetoothModule(context, activity);
        this.callableModule = new callableModuleBase(context, "TestModule");
    }

    @Test
    public void isInitialized() {
        assertThat(bluetoothModule).isNotNull();
        assertThat(bluetoothModule.getApplicationState()).isEqualTo(ApplicationState.INITIALIZING);

        assertThat(callableModule).isNotNull();
        assertThat(callableModule.getApplicationState()).isEqualTo(ApplicationState.INITIALIZING);
    }

    @Test
    public void callSerialPortOpenWithoutHandle() throws InterruptedException {
        AtomicReference<CoreError> errorReference = new AtomicReference<>();
        CountDownLatch latch = new CountDownLatch(1);
        Callback<OpenSuccess> callback = new Callback<OpenSuccess>() {
            @Override
            public void success(OpenSuccess payload, boolean lastCall) {
                System.out.println("Should fail");
            }

            @Override
            public void error(CoreError error, boolean lastCall) {
                errorReference.set(error);
                latch.countDown();
            }
        };

        callableModule.serialPortCall("Open", Open.newBuilder().setSerialPortHandle(42).build(), bluetoothModule.getId(), callback);
        latch.await(5000, TimeUnit.SECONDS);
        assertThat(errorReference.get().message).contains("Unknown handle");
    }

    @Test
    public void callSerialPortWriteWithoutHandle() throws InterruptedException {
        AtomicReference<CoreError> errorReference = new AtomicReference<>();
        CountDownLatch latch = new CountDownLatch(1);
        Callback<WriteSuccess> callback = new Callback<WriteSuccess>() {
            @Override
            public void success(WriteSuccess payload, boolean lastCall) {
                System.out.println("Should fail");
            }

            @Override
            public void error(CoreError error, boolean lastCall) {
                errorReference.set(error);
                latch.countDown();
            }
        };

        callableModule.serialPortCall("Write", Write.newBuilder().setSerialPortHandle(42).build(), bluetoothModule.getId(), callback);
        latch.await(5000, TimeUnit.SECONDS);
        assertThat(errorReference.get().message).contains("Unknown handle");
    }

    @Test
    public void callSerialPortOpenWriteAndClose() throws InterruptedException {
        CountDownLatch openLatch = new CountDownLatch(1);
        Callback<OpenSuccess> openCallback = new Callback<OpenSuccess>() {
            @Override
            public void success(OpenSuccess payload, boolean lastCall) {
                System.out.println("Open success callback received");
                openLatch.countDown();
            }

            @Override
            public void error(CoreError error, boolean lastCall) {
            }
        };


        CountDownLatch writeLatch = new CountDownLatch(1);
        Callback<WriteSuccess> writeCallback = new Callback<WriteSuccess>() {
            @Override
            public void success(WriteSuccess payload, boolean lastCall) {
                System.out.println("Write success callback received");
                writeLatch.countDown();
            }

            @Override
            public void error(CoreError error, boolean lastCall) {
            }
        };

        AtomicReference<String> messageReference = new AtomicReference<>();
        CountDownLatch closeLatch = new CountDownLatch(1);
        SerialPortRepresentation serialPort = new SerialPortRepresentation(bluetoothModule.getSerialPortEndpoint()) {
            @Override
            protected void open() {
                System.out.println("Open");
                this.openSuccess();
            }

            @Override
            protected void close() {
                System.out.println("Close");
                closeLatch.countDown();
            }

            @Override
            protected void write(byte[] buffer, boolean flush) {
                String msg = new String(buffer, StandardCharsets.UTF_8);
                System.out.println("Wrote: " + msg);
                messageReference.set(msg);
                this.writeSuccess();
            }
        };
        callableModule.serialPortCall("Open", Open.newBuilder().setSerialPortHandle(serialPort.getHandle().toInt64()).build(), bluetoothModule.getId(), openCallback);
        openLatch.await(5000, TimeUnit.SECONDS);

        String msg = "Greetings from sunny Spain";
        callableModule.serialPortCall("Write", Write.newBuilder().setSerialPortHandle(serialPort.getHandle().toInt64()).setMessage(ByteString.copyFromUtf8(msg)).build(), bluetoothModule.getId(), writeCallback);
        writeLatch.await(5000, TimeUnit.SECONDS);
        assertThat(msg).isEqualTo(messageReference.get());

        callableModule.serialPortPostEvent("Close", Close.newBuilder().setSerialPortHandle(serialPort.getHandle().toInt64()).build(), bluetoothModule.getId());
        closeLatch.await(5000, TimeUnit.SECONDS);
    }

    static class callableModuleBase extends ModuleBase {
        private final InterfaceEndpoint serialPortEndpoint;

        protected callableModuleBase(Context context, String moduleTag) {
            super(context, moduleTag);
            serialPortEndpoint = implementsConsumerInterface("SerialPort");
            start();
        }

        public <A extends MessageLite, R extends MessageLite> void serialPortCall(String functionName, A args, Peer receiver, Callback<? super R> callback) {
            serialPortEndpoint.callFunction(functionName, args, receiver, callback);
        }

        public <A extends MessageLite> void serialPortPostEvent(String functionName, A args, Peer receiver) {
            serialPortEndpoint.postEvent(functionName, args, receiver);
        }
    }
}