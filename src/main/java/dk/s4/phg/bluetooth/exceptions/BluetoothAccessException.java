package dk.s4.phg.bluetooth.exceptions;

/**
 * Bluetooth access was denied.
 *
 * Access to Bluetooth was restricted or denied for some reason,
 * possibly indicated by a subclass type, e.g. because the Bluetooth
 * adapter was off or the app was not permitted access.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:thomas.holst@alexandra.dk">Thomas
 *         Holst</a>, The Alexandra Institute.
 *
 */
public class BluetoothAccessException extends Exception {
    //    private static final long serialVersionUID = -3585496042502595745L;

    public BluetoothAccessException(String message) {
        super(message);
    }

    public BluetoothAccessException(String message, Exception cause) {
        super(message, cause);
    }
}
