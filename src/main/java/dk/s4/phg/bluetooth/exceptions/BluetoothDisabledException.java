package dk.s4.phg.bluetooth.exceptions;


public class BluetoothDisabledException extends BluetoothAccessException {
    //    private static final long serialVersionUID = 2891236021447001533L;

    public BluetoothDisabledException() {
        super("Bluetooth was not enabled on your device");
    }
}
