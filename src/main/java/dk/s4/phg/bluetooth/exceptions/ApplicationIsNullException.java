package dk.s4.phg.bluetooth.exceptions;

public class ApplicationIsNullException extends Exception {
    public ApplicationIsNullException(String s){
        super(s);
    }
}
