package dk.s4.phg.bluetooth.exceptions;

public class BluetoothNotAvailableException extends BluetoothAccessException {
    //    private static final long serialVersionUID = 9143749052078385153L;

    public BluetoothNotAvailableException() {
        super("Bluetooth is not available on your device");
    }
}
