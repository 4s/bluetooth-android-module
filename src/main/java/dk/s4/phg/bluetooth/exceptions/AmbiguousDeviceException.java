package dk.s4.phg.bluetooth.exceptions;

public class AmbiguousDeviceException extends Exception {
    private static final long serialVersionUID = 3844299737919967942L;

    public AmbiguousDeviceException() {
        super("More than one device match");
    }
}
