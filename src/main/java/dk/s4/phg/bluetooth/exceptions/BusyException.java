package dk.s4.phg.bluetooth.exceptions;

public class BusyException extends Exception {

    public BusyException(String msg){
        super(msg);
    }

}
