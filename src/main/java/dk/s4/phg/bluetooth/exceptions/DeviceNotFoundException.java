package dk.s4.phg.bluetooth.exceptions;

public class DeviceNotFoundException extends Exception {
    private static final long serialVersionUID = 610000703324426626L;

    public DeviceNotFoundException() {
        super("Did not find relevant device in list of paired devices");
    }
}
