package dk.s4.phg.bluetooth;

import android.app.Activity;
import android.content.Intent;

import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.messages.support.bluetooth_device_control.BluetoothDeviceControlProducer;
import dk.s4.phg.messages.support.bluetooth_device_control.BluetoothDeviceControlProducerEndpoint;
import dk.s4.phg.messages.support.device.BluetoothDeviceRepresentation;
import dk.s4.phg.messages.support.device_control.DeviceControlProducerEndpoint;
import dk.s4.phg.messages.support.gatt.GATTProducerEndpoint;
import dk.s4.phg.messages.support.serial_port.SerialPortProducerEndpoint;




/**
 * Platform-specific Bluetooth implementation for the Android
 * platform.
 *
 * This class implements the platform-specific PHG core Bluetooth
 * module for the Android platform.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author Mads Møller Jensen, The Alexandra Institute.
 * @author <a href="mailto:thomas.holst@alexandra.dk">Thomas
 *         Holst</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public class BluetoothModule extends ModuleBase {

    // The moduleTag must be the PascalCase-styled name of the module
    private static final String moduleTag = "Bluetooth";

    // The business functionality is encapsulated by the controller
    private BluetoothDeviceController controller;

    // The interface endpoints
    private DeviceControlProducerEndpoint<BluetoothDeviceRepresentation> devCtrlEndpoint;
    private BluetoothDeviceControlProducerEndpoint btDevCtrlEndpoint;
    private SerialPortProducerEndpoint spEndpoint;
    private GATTProducerEndpoint gattEndpoint;

    
    /**
     * Construct and start the Bluetooth module.
     *
     * @param context      The PHG Core common Context object shared by
     *                     all modules connected to the JVM baseplate
     * @param appActivity  The Android activity
     */
    public BluetoothModule(Context context, Activity appActivity) {
        super(context, moduleTag);

        if (appActivity == null) {
            throw new IllegalArgumentException(
                    "Android application activity is null");
        }


        // Factory for producing and keeping track of all known
        // Bluetooth devices
        BluetoothDeviceRepresentation.BluetoothDeviceRepresentationFactory
                deviceFactory = new BluetoothDeviceRepresentation.BluetoothDeviceRepresentationFactory(this, "BT") {
            @Override
            protected DeviceControlProducerEndpoint getDeviceControlProducerEndpoint() {
                return devCtrlEndpoint;
            }
        };

        // The SerialPort producer interface
        spEndpoint = new SerialPortProducerEndpoint(endpointFactory(), enqueueActionConsumer());

        // The GATT producer interface
        gattEndpoint = new GATTProducerEndpoint(endpointFactory());

        // The business functionality of the DeviceControl and
        // BluetoothDeviceControl interfaces as well as general
        // Bluetooth device management is encapsulated by a
        // BluetoothDeviceController object.
        controller = new BluetoothDeviceController(appActivity, getLogger(), enqueueActionConsumer(), deviceFactory,
                spEndpoint, gattEndpoint) {
            @Override
            public BluetoothDeviceControlProducer.BluetoothPairing getBluetoothPairing() {
                return btDevCtrlEndpoint;
            }
        };

        // The DeviceControl producer interface
        devCtrlEndpoint = new DeviceControlProducerEndpoint<>(controller, this.endpointFactory(), deviceFactory);

        // The BluetoothDeviceControl producer interface
        btDevCtrlEndpoint = new BluetoothDeviceControlProducerEndpoint(controller, this.endpointFactory(), deviceFactory);


        getLogger().debug("Starting the Bluetooth Android module");
// FIXME: Remove when the ModuleBase collects log messages from the system startup
        android.util.Log.d("*****", "Starting the Bluetooth Android module");


        // Interfaces are connected and ready to use, so notify the
        // baseplate that we are done initializing.
        start();
    }

    /**
     * Used for testing
     *
     * @return the {@link SerialPortProducerEndpoint} instance created during initialization
     */
    @Deprecated
    protected SerialPortProducerEndpoint getSerialPortEndpoint() {
        return spEndpoint;
    }

    /**
     * Only used for testing
     */
    @Deprecated
    protected void discoverNewDevice(android.content.Context context, Intent intent) {
        controller.discoverNewDevice(context, intent);
    }
}
