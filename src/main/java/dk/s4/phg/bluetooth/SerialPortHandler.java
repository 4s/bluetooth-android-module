package dk.s4.phg.bluetooth;

import dk.s4.phg.baseplate.Logger;
import dk.s4.phg.messages.support.serial_port.SerialPortRepresentation;
import dk.s4.phg.messages.support.serial_port.SerialPortProducerEndpoint;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;




/**
 * Handler of the Bluetooth classic Serial Port Profile (SPP).
 *
 * This handler is limited to one SPP profile per device. It is
 * currently unclear if - and then how - Android supports multiple
 * serial (SPP) ports served by the same device. It seems that Android
 * will choose either the first or a random port in that case. Since
 * we do not expect to ever encounter a device that serves multiple
 * serial ports, this will likely never become a problem...
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:thomas.holst@alexandra.dk">Thomas
 *         Holst</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public class SerialPortHandler extends SerialPortRepresentation {

    /**
     * The module logger
     */
    private final Logger log;
    
    /**
     * The Bluetooth device providing the serial port.
     */
    private final BluetoothDevice device;

    /**
     * The Bluetooth device's serial port socket
     * 
     * This value is managed by the reader thread and has the same
     * life span. It will be set to a concrete object before reporting
     * openSuccess and will be cleared to null before reporting
     * closed. Between closed and opening the port (starting a new
     * reader thread), the value must be null.
     */
    private volatile BluetoothSocket socket = null;

    /**
     * The Bluetooth device's serial port output stream
     * 
     * This value is managed by the reader thread and has the same
     * life span. It will be set to a concrete object before reporting
     * openSuccess and will be cleared to null before reporting
     * closed. Between closed and opening the port (starting a new
     * reader thread), the value must be null.
     */
    private volatile OutputStream outputStream;

    /**
     * Flag used to notify the reader thread that our end has
     * closed the connection.
     */
    private volatile boolean closed;


    
    
    /**
     * Serve a device's serial port on a producer endpoint.
     *
     * Creates a serial port served by the given Bluetooth device
     * (which must expose the Serial Port Profile). The serial port
     * will be made available on the given SerialPortProducerEndpoint.
     * @param device      The Bluetooth device exposing a classical
     *                    SPP serial port.
     * @param sppEndpoint The serial port producer interface endpoint
     *                    on which we will be making this serial port
     *                    available to consumers.
     */
    public SerialPortHandler(BluetoothDevice device,
                             SerialPortProducerEndpoint sppEndpoint) {
        super(sppEndpoint);
        this.device = device;
        this.log = getLogger();
        
        log.debug("Serving serial port of Bluetooth device: " +
                  device.getName());
    }

    
    /**
     * A subclass may inject code before opening a serial port begins
     */
    protected void beforeOpenHook(BluetoothDevice device) {}

    
    /**
     * A subclass may inject code after opening a serial port is
     * complete (success or not)
     */
    protected void afterOpenHook(BluetoothDevice device) {}

    
    /**
     * Open command received from the consumer
     */
    @Override
    protected void open() {

        log.debug("Serial port opening");

        // Flag that the connection has not yet been closed by close()
        closed = false;
        
        // A new thread is started to handle the open command
        (new Thread("SerialPortReaderThread") {
            public void run() {

                beforeOpenHook(device);
                
                // The reader input stream
                InputStream inputStream;
                
                // Open the serial port socket
                try {
                    socket = device.createRfcommSocketToServiceRecord
                        (BluetoothUUID.SERIAL_PORT_PROFILE);
                    if (socket == null) {
                        throw new IOException
                            ("null returned for BluetoothSocket!");
                    }
                    
                    // Connect will block until the connection has
                    // been established or a timeout of 5 seconds
                    // have elapsed
                    socket.connect();
                    
                    inputStream = socket.getInputStream();
                    outputStream = socket.getOutputStream();
                    
                    runAsync(() -> log.debug("Serial port open"));
                    
                } catch (IOException e){
                    // Open failed. Clean up
                    outputStream = null;
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (IOException ignore) {
                            // Ignore errors on close
                        }
                        socket = null;
                    }

                    //  And report back with openFailed
                    runAsync(() -> log.debug("Could not open connection", e));
                    openFailedAsync(OpenError.IO_ERROR,"Could not open connection",
                                    log.createError(e));
                    // Notify that the port has been closed
                    closedAsync();
                    return;
                } finally {
                    afterOpenHook(device);
                }

                // Return open success message
                openSuccessAsync();

                // Reader loop
                
                byte[] buffer = new byte[256];
                try {
                    while (socket.isConnected()) {
                        int readCount = inputStream.read(buffer);
                        if (readCount < 0) {
                            // End-of-stream - quit the loop
                            break;
                        } else if (readCount > 0) {
                            // Copy payload to a new array...
                            byte[] bufferCopy = Arrays.copyOf(buffer,
                                                              readCount);
                            
                            // Log 
                            runAsync(() -> log.debug("Serial port read " +
                                                     readCount + " bytes: " +
                                                     new String(bufferCopy)));
                            
                            // ... and send it on
                            readAsync(bufferCopy);
                            
                        }
                    }
                } catch (IOException e) {
                    // We will not log the error if the connection
                    // has been closed from our end.
                    if (!closed) {
                        // Error during read -> log and then close the
                        // serial port
                        runAsync(() -> log.error
                                 ("Bluetooth serial port read failed", e));
                    }
                } finally {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // This is most likely not a problem. But
                        // report the error on the log anyway - just
                        // in case anyone is interested
                        runAsync(() -> log.error
                                 ("Bluetooth serial port close failed", e));
                    }
                    outputStream = null;
                    socket = null;
                    // Notify that the port has been closed
                    closedAsync();
                }
            }
        }).start();
    }

    
    /**
     * Write command received from the consumer
     *
     * The write method makes an exception to the rule of module
     * implementation being non-blocking, as it may indeed block if
     * flush is true or the output buffer is full. Since we are in a
     * multi-threaded Java world, this is not a problem.
     * @param bytes The message to write
     * @param flush true if the message should be sent immediately,
     *              otherwise the message may be delayed at the
     *              discretion of the underlying implementation.
     */
    @Override
    protected void write(byte[] bytes, boolean flush) {
        
        // Local cache of the volatile outputStream, as this may otherwise
        // disappear under our feet.
        OutputStream theStream = outputStream;

        if (theStream != null) {
            try {
                theStream.write(bytes);

                if (flush){
                    theStream.flush();
                }
                
                log.debug("Wrote data to serial port");
                
                writeSuccess();
            } catch (IOException ioe) {
                BluetoothSocket theSocket = socket;
                
                if (theSocket == null || !theSocket.isConnected()) {

                    // The socket was closed
                    log.debug("Serial port closed during write on " +
                              device.getName(), ioe);
                    writeFailed(WriteError.NOT_OPEN,
                                "Serial port closed during write on " +
                                device.getName(), log.createError(ioe));

                } else {

                    // Something else went wrong...
                    log.debug("Writing to serial port on " + device.getName() +
                              " failed", ioe);
                    writeFailed(WriteError.IO_ERROR,
                                "Writing to serial port on " + device.getName()
                                + " failed", log.createError(ioe));
                }
            }
        } else {

            // The port was not open
            writeFailed(WriteError.NOT_OPEN,
                        "Attempt to write to a closed serial port on " +
                        device.getName(), null);
        }
    }

    
    /**
     * Close command received from the consumer
     */
    @Override
    protected void close() {

        // Local cache of the volatile socket, as this may otherwise
        // disappear under our feet.
        BluetoothSocket theSocket = socket;

        if (theSocket != null) {
            // Flag will notify the reader thread that we have closed
            // the connection
            closed = true;
            
            try {
                // This will force the reader thread to stop and clean up.
                theSocket.close();
                log.debug("Bluetooth serial port closed");
            } catch (IOException e) {
                // This is most likely not a problem. But report the
                // error on the log anyway - just in case anyone is
                // interested
                log.error("Bluetooth serial port close failed", e);
            }
        }
    }
}
