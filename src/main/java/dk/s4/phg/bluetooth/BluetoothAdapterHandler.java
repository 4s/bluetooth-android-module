package dk.s4.phg.bluetooth;

import dk.s4.phg.baseplate.ObjId;
import dk.s4.phg.bluetooth.exceptions.*;
import dk.s4.phg.bluetooth.gatt.GATTCallback;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;

import dk.s4.phg.baseplate.Callback;


import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.HashSet;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.concurrent.ConcurrentHashMap;
import java.nio.charset.StandardCharsets;

/**
 * Class for working with the Bluetooth adapter
 *
 * The adapter handler responsible for the interface to the
 * Android Bluetooth adapter API
 */



@SuppressLint("SimpleDateFormat")
public class BluetoothAdapterHandler {

    private final String TAG = "BluetoothAdapterHandler";
    private Context appContext;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private static String deviceAddress = null; // used when bonding with a device
    //private Callback callback;
    private Map<String, BluetoothDevice> devices;
    private Handler mHandler;
    private boolean mScanning;
    private int SCAN_PERIOD = 10000;
    private BluetoothAdapterCallback bluetoothCallback;
    private ConcurrentHashMap<BluetoothDevice, String> knownPINs = new ConcurrentHashMap<>();
    private final HashSet<BluetoothDevice> interceptPairingPopup = new HashSet<>();
    private Consumer<Runnable> enqueueActionConsumer;
    
    /**
     * Constructor
     * @param appContext The Android application context
     * @throws DeviceInitialisationException if the device does not support Bluetooth or it is not enabled
     */

    public BluetoothAdapterHandler(Context appContext, BluetoothAdapterCallback bluetoothCallback, Consumer<Runnable> enqueueActionConsumer){

        this.appContext = appContext;
        this.bluetoothCallback = bluetoothCallback;
        this.enqueueActionConsumer = enqueueActionConsumer;

        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.devices = new HashMap<>();

        appContext.registerReceiver(mReceiver, IntentFilterFactory.classic());

        //Debugging
        Log.d(TAG, "Bonded devices: ");
        for(BluetoothDevice device : mBluetoothAdapter.getBondedDevices()){
            Log.d(TAG, device.getName() + ": " + device.getAddress());
            if(device.getName().contains("Contour")){
                BluetoothDevice actualDevice = mBluetoothAdapter.getRemoteDevice(device.getAddress());
                //BluetoothGatt gatt = actualDevice.connectGatt(appContext,false, new GATTHandler().getGattCallback());

            }
        }
    }

    /**
     * Returns a list of bonded Bluetooth devices
     * @return List of bonded Bluetooth devices
     * @throws BusyException
     */

    public Set<BluetoothDevice> getBondedDevices() throws BluetoothAccessException {

        return mBluetoothAdapter.getBondedDevices();

    }

    public void setStandardPin(BluetoothDevice device, String pin){
        if (pin == null || pin == "") {
            throw new IllegalArgumentException("pin was empty");
        } else {
            knownPINs.put(device, pin);
        }
    }

    /**
     * Method for bonding with a device
     * @param deviceHandle The uuid of the device
     * @param pin The pin code of the device
     */

    public void bond(BluetoothDevice device) throws BluetoothAccessException  /* and maybe another exception covering problems with bonding */ {
        throw new Error("Not implemented");
            //Method method = dDevice.getClass().getMethod("createBond", (Class[]) null);
            //method.invoke(device, (Object[]) null);

            //BluetoothGatt gatt = device.connectGatt(appContext, true, new GATTHandler().getGattCallback());
 //            boolean deviceBonded = device.createBond();

    }

    /**
     * Method for unbonding with a device
     * @param address The address of the device to unbond from
     */

    public void unbond(BluetoothDevice device) throws BluetoothAccessException /* and maybe another exception covering problems with unbonding */ , Exception {
        Method method = device.getClass().getMethod("removeBond", (Class[]) null);
        method.invoke(device , (Object[]) null);
    }

    /**
     * Begin intercepting pairing pop-up for this device.
     */
    public void interceptPairingBegin(BluetoothDevice device) {
        synchronized (interceptPairingPopup) {
            interceptPairingPopup.add(device);
        }
    }

    /**
     * Stop intercepting pairing pop-up for this device.
     */
    public void interceptPairingEnd(BluetoothDevice device) {
        synchronized (interceptPairingPopup) {
            interceptPairingPopup.remove(device);
        }
    }
    
    /**
     * Tests if device is Bluetooth friendly
     * @throws BluetoothNotAvailableException
     * @throws BluetoothDisabledException
     */

    public void checkBluetooth()throws BluetoothNotAvailableException, BluetoothDisabledException, BusyException{

        if (mBluetoothAdapter == null) {
            Log.d(TAG, "This device does not support Bluetooth");
            throw new BluetoothNotAvailableException();
        }
        else if(mBluetoothAdapter.isEnabled() == false){
            Log.d(TAG, "Bluetooth is not enabled");
            throw new BluetoothDisabledException();
        }
        else if (mBluetoothAdapter.isDiscovering()){
            throw new BusyException("The bluetooth adapter is busy");
        }

    }

    /**
     * Get the BluetoothAdapter or throw a BluetoothAccessException if
     * Bluetooth is not available.
     */
    private BluetoothAdapter getAdapter() throws BluetoothAccessException {
        return mBluetoothAdapter;
    }


    /**
     * Returns the BluetoothDevice matching a given BD_ADDR.
     *
     * A device is always constructed and returned when the argument
     * is correctly formatted.
     * @param address The BD_ADDR.
     * @return The Android Bluetooth device.
     * @throws IllegalArgumentException if the hwString format was
     *                                  invalid.
     * @throws BluetoothAccessException if Bluetooth cannot be
     *                                  accessed for some (specified
     *                                  by exception subclass) reason.
     */
    public BluetoothDevice getDeviceForAddress(String address) 
             throws IllegalArgumentException, BluetoothAccessException {
        return getAdapter().getRemoteDevice(address);
    }

    
    /**
     * Initiates a search for available Bluetooth devices
     * //@param callback The callback to send the devices to
     */

    public void search() throws BluetoothAccessException {

        Log.d(TAG, "Starting search...");

        //Initiating BLE scan

        //TODO: Send error if already scanning
        if(mScanning){  }

        mScanning = true;
        // mHandler = new Handler();
        // mHandler.postDelayed(this::stopScan, SCAN_PERIOD);

        // if(mBluetoothLeScanner == null){
        //     mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        // }

        // mBluetoothLeScanner.startScan(mScanCallback);

        //Initiating Classic BT scan
        mBluetoothAdapter.startDiscovery();
    }


    /**
     * Cancels a discovery
     */

    public void cancelSearch(){
       // callback = null;
        this.mBluetoothAdapter.cancelDiscovery();
        //stopScan();
        Log.d(TAG, "Cancelling search");

        //slet ref til searchresultcolector
    }

    /**
     * Stops LeScan
     */
    private void stopScan() {
        if (mScanning && mBluetoothAdapter != null && mBluetoothAdapter.isEnabled() && mBluetoothLeScanner != null) {
            mBluetoothLeScanner.stopScan(mScanCallback);
        }

        mScanCallback = null;
        mScanning = false;
        mHandler = null;
    }

    /**
     * Returns a Bluetooth device
     * @param deviceAddress the address of the device
     * @return the device object
     * @throws DeviceNotFoundException if the device was not found
     */

    public BluetoothDevice getDevice(String deviceAddress)  throws DeviceNotFoundException {

        Optional<BluetoothDevice> deviceOpt = mBluetoothAdapter.getBondedDevices().stream().filter(d -> d.getAddress().startsWith(deviceAddress)).findFirst();

        if (deviceOpt.isPresent() == false){
            Log.e(TAG, "GetDevice: Device not found");
            throw new DeviceNotFoundException();
        }

        return deviceOpt.get();

    }

    /**
     * Cleans up before closing the module
     */

    public void destroy(){
        appContext.unregisterReceiver(mReceiver); // todo Don't forget to unregister during onDestroy
    }

    /**
     * Receiver for BluetoothAdapter events
     */

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                UUID uuid = UUID.nameUUIDFromBytes(device.getAddress().getBytes());

               BluetoothDevice deviceInMap = devices.put(uuid.toString(), device);
               if(deviceInMap == null){
                   bluetoothCallback.onSearchResult(device);
               }

               String typeString = "N/A";
               switch (device.getType()) {
                    case BluetoothDevice.DEVICE_TYPE_LE:
                        typeString = "LE";
                        break;
                   case BluetoothDevice.DEVICE_TYPE_CLASSIC:
                       typeString = "CLASSIC";
                       break;
                   case BluetoothDevice.DEVICE_TYPE_DUAL:
                       typeString = "DUAL";
                       break;
                   case BluetoothDevice.DEVICE_TYPE_UNKNOWN:
                       typeString = "UNKNOWN";
                       break;
                }

                //Log.d(TAG, "Of type: " + typeString);

            }
            else if(BluetoothDevice.ACTION_PAIRING_REQUEST.equals(action))
            {

                BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (bluetoothDevice == null) return;

                // Do we have a PIN for this device?
                String pin = knownPINs.get(bluetoothDevice);
                if (pin == null) return;

                // Bail out if we are not expecting to bond with this device now
                synchronized (interceptPairingPopup) {
                    if (!interceptPairingPopup.contains(bluetoothDevice)) return;
                }

                // Set the PIN and flag that we have handled the request
                bluetoothDevice.setPin(pin.getBytes(StandardCharsets.UTF_8));
                abortBroadcast();
            }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){

                //This is the time to fetch the UUIDS, if device is SerialPort: https://stackoverflow.com/questions/14812326/android-bluetooth-get-uuids-of-discovered-devices

//                if (callback == null){ // search was cancelled
                    Log.d(TAG, "Search stopped");

                    //For Debugging purposes:
//                    for(String uuid : devices.keySet()){
////                        Log.d(TAG, "Found device: " + devices.get(uuid).getName());
////                        Log.d(TAG, "With uuid: " + uuid.toString());
////                        Log.d(TAG, "With address: " + devices.get(uuid).getAddress());
//
//
//                        String typeString = "N/A";
//                        switch (devices.get(uuid).getType()) {
//                            case BluetoothDevice.DEVICE_TYPE_LE:
//                                typeString = "LE";
//                                break;
//                            case BluetoothDevice.DEVICE_TYPE_CLASSIC:
//                                typeString = "CLASSIC";
//                                break;
//                            case BluetoothDevice.DEVICE_TYPE_DUAL:
//                                typeString = "DUAL";
//                                break;
//                            case BluetoothDevice.DEVICE_TYPE_UNKNOWN:
//                                typeString = "UNKNOWN";
//                                break;
//                        }
//
//                    }


                    //stopScan();

                    ////FIXME: FOR TESTING / DEBUGGING WITHOUT BASEPLATE
                Intent searchResultIntent = new Intent();
                searchResultIntent.setAction("PHG_BLUETOOTH_SEARCH_DONE");
                appContext.sendBroadcast(searchResultIntent);



//                    return;
//                }

            } else if(BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)){
                int bondState = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, 0);

                //CHECK IF DEVICE IS CORRECT
                BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                Log.d(TAG, "Bond State Changed:");
                if(bondState == BluetoothDevice.BOND_BONDED){
                    Log.d(TAG, "Device bonded");

                    //Callback -> things went well
                    bluetoothCallback.onBondResponse(bondState);
                } else if(bondState == BluetoothDevice.BOND_BONDING){
                    Log.d(TAG, "Bonding with device...");
                } else if(bondState == BluetoothDevice.BOND_NONE){
                    Log.d(TAG, "Not bonded");

                    bluetoothCallback.onBondResponse(bondState);
                }

            }

        }

    };

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            addScanResult(result);
        }
        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult result : results) {
                addScanResult(result);
            }
        }
        @Override
        public void onScanFailed(int errorCode) {
            Log.e(TAG, "BLE Scan Failed with code " + errorCode);
        }

        private void addScanResult(ScanResult result) {
            BluetoothDevice device = result.getDevice();


            //Create device UUID from scan record
            UUID uuid = UUID.nameUUIDFromBytes(result.getScanRecord().getBytes());
            String handle = device.getAddress();

            BluetoothDevice deviceInMap = devices.put(handle, device);
            if(deviceInMap == null){
                bluetoothCallback.onSearchResult(device);

                //Debugging
                if(device.getName() != null && device.getName().contains("Contour")){
                    Log.d(TAG, "Contour found");
                }
            }
        }
    };


    
    /* ********************************************************************** */
    /*                                                                        */
    /*                           GATT communication                           */
    /*                                                                        */
    /* ********************************************************************** */

    /** Active GATT connections: mapping between the Android callback and internal callback objects */
    private final HashMap<BluetoothGatt, GATTCallback> gattConnections = new HashMap<>();

    /**
     * Open a connection to the GATT server on the given Bluetooth device
     */
    public BluetoothGatt connectGatt(BluetoothDevice device, boolean autoConnect, GATTCallback callback) {
        // Sanity check
        if (device == null) throw new NullPointerException("Device is null");
        if (gattConnections.containsValue(callback)) throw new IllegalStateException("Device GATT server connection exists");

        BluetoothGatt retval = device.connectGatt(appContext, autoConnect, bluetoothGattCallback);

        callback = gattConnections.put(retval, callback);
        // Sanity check; should not be possible
        if (callback != null) throw new java.util.ConcurrentModificationException("GATTCallback double-registered");

        return retval;
    }

    /**
     * Re-open a connection to the GATT server
     */
    public void connectGatt(BluetoothGatt gattServer) {
        gattServer.connect();
    }

    /**
     * Close the connection to the GATT server
     */
    public void disconnectGatt(BluetoothGatt gattServer) {
        gattServer.disconnect();
    }

    /**
     * Start a full service discovery on the GATT server
     */
    public boolean discoverServices(BluetoothGatt gattServer) {
        return gattServer.discoverServices();
    }

    /**
     * Get services from the GATT server
     */
    public List<BluetoothGattService> getServices(BluetoothGatt gattServer) {
        return gattServer.getServices();
    }

    /**
     * Read characteristic from the GATT server
     */
    public boolean readCharacteristic(BluetoothGatt gattServer, BluetoothGattCharacteristic characteristic) {
        return gattServer.readCharacteristic(characteristic);
    }

    /**
     * Write characteristic to the GATT server
     */
    public boolean writeCharacteristic(BluetoothGatt gattServer, BluetoothGattCharacteristic characteristic,
                                       byte[] value, boolean withResponse) {
        characteristic.setWriteType(withResponse ? BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
                                    : BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        characteristic.setValue(value);
        return gattServer.writeCharacteristic(characteristic);
    }

    /**
     * Enable/disable characteristic notifications from the GATT server
     */
    public boolean setCharacteristicNotification(BluetoothGatt gattServer, BluetoothGattCharacteristic characteristic, boolean enable) {
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
        if (descriptor == null) {
            return false;
        }
        if (!gattServer.setCharacteristicNotification(characteristic, enable)) {
            return false;
        }
        return descriptor.setValue(enable ? ((characteristic.getProperties() & 0x20) == 0x20 ?
                                             BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
                                             : BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)
                                   : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE)
            && gattServer.writeDescriptor(descriptor);
    }

    /**
     * Dispatch a task on the module thread with the GATTCallback
     * associated with the provided GATT server object.
     */
    private void dispatchGATT(BluetoothGatt gatt, Consumer<GATTCallback> task) {
        enqueueActionConsumer.accept(() -> {
                GATTCallback callback = gattConnections.get(gatt);
                if (callback != null) task.accept(callback);
            });
    }

    /**
     * Translate the Android status code to the GATTCallback status enum 
     */
    private GATTCallback.Status translateGATTStatus(int status) {
        switch (status) {
        case 0:
            return GATTCallback.Status.SUCCESS;
        case 2:
            return GATTCallback.Status.READ_NOT_PERMITTED;
        case 3:
            return GATTCallback.Status.WRITE_NOT_PERMITTED;
        case 5:
            return GATTCallback.Status.INSUFFICIENT_AUTHENTICATION;
        case 6:
            return GATTCallback.Status.REQUEST_NOT_SUPPORTED;
        case 7:
            return GATTCallback.Status.INVALID_OFFSET;
        case 8:
            return GATTCallback.Status.OUT_OF_RANGE;
        case 13:
            return GATTCallback.Status.INVALID_ATTRIBUTE_LENGTH;
        case 15:
            return GATTCallback.Status.INSUFFICIENT_ENCRYPTION;
        case 19:
            return GATTCallback.Status.DISCONNECTED_BY_PEER;
        case 22:
            return GATTCallback.Status.BONDING_PROBLEM;
        case 62:
        case 133:
            return GATTCallback.Status.DEVICE_NOT_FOUND;
        case 143:
            return GATTCallback.Status.CONNECTION_CONGESTED;
        default:
            return GATTCallback.Status.FAILURE;
        }
    }
    
    // Dispatch all GATT events to the corresponding GATT callback on the module thread
    private final BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                // Ignore CONNECTING/DISCONNECTING states
                if (newState != BluetoothProfile.STATE_CONNECTED && newState != BluetoothProfile.STATE_DISCONNECTED) return;

                dispatchGATT(gatt, (GATTCallback gc) -> gc.onConnectionStateChange(translateGATTStatus(status),
                                                                               newState == BluetoothProfile.STATE_CONNECTED));
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                dispatchGATT(gatt, (GATTCallback gc) -> gc.onServicesDiscovered(translateGATTStatus(status)));
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                dispatchGATT(gatt, (GATTCallback gc) -> gc.onCharacteristicRead(characteristic, translateGATTStatus(status)));
            }

            @Override
            public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                dispatchGATT(gatt, (GATTCallback gc) -> gc.onCharacteristicWrite(characteristic, translateGATTStatus(status)));
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                byte[] value = characteristic.getValue();
                dispatchGATT(gatt, (GATTCallback gc) -> gc.onCharacteristicChanged(characteristic, value));
            }

            @Override
            public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                dispatchGATT(gatt, (GATTCallback gc) -> gc.onDescriptorRead(descriptor, translateGATTStatus(status)));
            }

            @Override
            public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                dispatchGATT(gatt, (GATTCallback gc) -> gc.onDescriptorWrite(descriptor, translateGATTStatus(status)));
            }

            @Override
            public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
                dispatchGATT(gatt, (GATTCallback gc) -> gc.onReliableWriteCompleted(translateGATTStatus(status)));
            }
        };

    

    /* ********************************************************************** */
    /*                                                                        */
    /*                            Testing handles                             */
    /*                                                                        */
    /* ********************************************************************** */


    /**
     * Only used for testing
     */
    @Deprecated
    protected void discoverNewDevice(Context context, Intent intent) {
        mReceiver.onReceive(context, intent);
    }
}
