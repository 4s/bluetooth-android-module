package dk.s4.phg.bluetooth;

import java.util.UUID;


/**
 * Bluetooth UUIDs
 *
 * This class contains a collection of Bluetooth standard UUID which
 * can also be found on
 * https://www.bluetooth.com/specifications/assigned-numbers/
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author Mads Møller Jensen, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public class BluetoothUUID {
    
    /**
     * Produces the UUID corresponding to the given 16-bit short value.
     *
     * @param shortValue A 4-digit hex string. The string MUST be
     *                   exactly 4 hex-digits with no other characters
     *                   or whitespace.
     */
    public static UUID from16BitString(String shortValue) {
        return from32BitString("0000" + shortValue);
    }
    
    /**
     * Produces the UUID corresponding to the given 32-bit short value.
     *
     * @param shortValue A 8-digit hex string. The string MUST be
     *                   exactly 8 hex-digits with no other characters
     *                   or whitespace.
     */
    public static UUID from32BitString(String shortValue) {
        return UUID.fromString(shortValue + "-0000-1000-8000-00805F9B34FB");
    }
    
    
    
    
    //
    // Service Discovery
    // https://www.bluetooth.com/specifications/assigned-numbers/service-discovery
    // Applies to Bluetooth classic
    //
    
    // Service Class Profile Identifiers
    
    /** Serial Port Profile (SPP) */
    public static final UUID SERIAL_PORT_PROFILE = from16BitString("1101");
    
    /** Health Device Profile (HDP) */
    public static final UUID HEALTH_DEVICE_PROFILE = from16BitString("1400");
    
    /** Health Device Profile, Source (HDP-Source) */
    public static final UUID HDP_SOURCE = from16BitString("1401");
    
    /** Health Device Profile, Sink (HDP-Sink) */
    public static final UUID HDP_SINK = from16BitString("1402");
    
    
    
    
    //
    // GATT Services
    // https://www.bluetooth.com/specifications/gatt/services
    // Applies (primarily) to Bluetooth Low Energy
    //
    
    /** Generic Access Service */
    public static final UUID GENERIC_ACCESS_SERVICE = from16BitString("1800");
    
    /** Glucose Service */
    public static final UUID GLUCOSE_SERVICE = from16BitString("1808");
    
    /** Device Information Service */
    public static final UUID DEVICE_INFORMATION_SERVICE = from16BitString("180A");
    
    
    
    
    //
    // GATT Attribute Types
    //
    
    /** GATT Primary Service */
    public static final UUID PRIMARY_SERVICE = from16BitString("2800");

    /** GATT Secondary Service */
    public static final UUID SECONDARY_SERVICE = from16BitString("2801");

    /** GATT Included Service */
    public static final UUID INCLUDE = from16BitString("2802");

    /** GATT Characteristic */
    public static final UUID CHARACTERISTIC = from16BitString("2803");

    /** GATT Characteristic Extended Properties Descriptor */
    public static final UUID CHARACTERISTIC_EXTENDED_PROPERTIES
        = from16BitString("2900");

    /** GATT Characteristic User Description Descriptor */
    public static final UUID CHARACTERISTIC_USER_DESCRIPTION
        = from16BitString("2901");

    /** GATT Client Characteristic Configuration Descriptor */
    public static final UUID CLIENT_CHARACTERISTIC_CONFIGURATION
        = from16BitString("2902");

    /** GATT Server Characteristic Configuration Descriptor */
    public static final UUID SERVER_CHARACTERISTIC_CONFIGURATION
        = from16BitString("2903");

    /** GATT Characteristic Format Descriptor */
    public static final UUID CHARACTERISTIC_FORMAT
        = from16BitString("2904");
    
    

    
    
    // FIXME: The following stuff does not belong to the
    // platform-specific bluetooth module!!! All of the following
    // shall be moved to platform-independent protocol modules.

    public static final UUID APPEARANCE = from16BitString("2a01");

    //DEVICE INFORMATION SERVICE CHARACTERISTICS
    public static final UUID SYSTEM_ID = from16BitString("2a23");
    public static final UUID MODEL_NUMBER_STRING = from16BitString("2a24");
    public static final UUID SERIAL_NUMBER_STRING = from16BitString("2a25");
    public static final UUID FIRMWARE_REVISION_STRING = from16BitString("2a26");
    public static final UUID HARDWARE_REVISION_STRING = from16BitString("2a27");
    public static final UUID SOFTWARE_REVISION_STRING = from16BitString("2a28");
    public static final UUID MANUFACTURER_NAME_STRING = from16BitString("2a29");
    public static final UUID REGULATORY_CERTIFICATION_DATA_LIST = from16BitString("2a2a");
    public static final UUID PNP_ID = from16BitString("2a50");

    //GLUCOSE SERVICE CHARACTERISTICS
    public static final  UUID GLUCOSE_MEASUREMENT = from16BitString("2a18");
    public static final  UUID GLUCOSE_MEASUREMENT_CONTEXT = from16BitString("2a34");
    public static final  UUID GLUCOSE_FEATURE = from16BitString("2a51");
    public static final  UUID RECORD_ACCESS_CONTROL_POINT = from16BitString("2a52");

}
