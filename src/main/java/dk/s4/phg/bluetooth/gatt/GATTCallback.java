package dk.s4.phg.bluetooth.gatt;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;

public interface GATTCallback {

    /**
     * The result of a GATT operation
     */
    public enum Status {
        SUCCESS,
        READ_NOT_PERMITTED,
        WRITE_NOT_PERMITTED,
        INSUFFICIENT_AUTHENTICATION,
        REQUEST_NOT_SUPPORTED,

        /** Read or write operation requested with invalid offset */
        INVALID_OFFSET,

        /** Link was disconnected due to weak signal strength */
        OUT_OF_RANGE,

        /** A write operation exceeds the maximum length of the attribute */
        INVALID_ATTRIBUTE_LENGTH,
        
        INSUFFICIENT_ENCRYPTION,

        /** Device disconnected the link (intentionally) */
        DISCONNECTED_BY_PEER,

        /** Unresolved issue with bonding */
        BONDING_PROBLEM,
        
        /** Initial attempt to connect to device failed (timeout) */
        DEVICE_NOT_FOUND,
        
        CONNECTION_CONGESTED,

        /** Unspecified I/O error that did not match any other category */
        FAILURE;
    }

    /**
     * A GATT server connected or disconnected.
     *
     * status==SUCCESS marks a successful operation requested by
     * us. Whether this operation was a connect or disconnect is
     * marked by isConnected. If status is anything else, the server
     * was disconnected by the server or some communication
     * problem. The reason is explained by status.
     */
    void onConnectionStateChange(Status status, boolean isConnected);
    void onServicesDiscovered(Status status);
    void onCharacteristicRead(BluetoothGattCharacteristic characteristic, Status status);
    void onCharacteristicWrite(BluetoothGattCharacteristic characteristic, Status status);
    void onCharacteristicChanged(BluetoothGattCharacteristic characteristic, byte[] value);
    void onDescriptorRead(BluetoothGattDescriptor descriptor, Status status);
    void onDescriptorWrite(BluetoothGattDescriptor descriptor, Status status);
    void onReliableWriteCompleted(Status status);

}
