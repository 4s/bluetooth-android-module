package dk.s4.phg.bluetooth.gatt;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.BiConsumer;

import dk.s4.phg.baseplate.CoreError;

import dk.s4.phg.messages.support.gatt.GATTCharacteristicRepresentation;
import dk.s4.phg.messages.support.gatt.GATTDescriptorRepresentation;
import dk.s4.phg.messages.support.gatt.GATTError;

class GATTCharacteristicHandler implements GATTCharacteristicRepresentation {

    private final BluetoothGattCharacteristic androidCharacteristic;
    private final GATTHandler parent;
    private Set<GATTDescriptorHandler> descriptors = null;
    
    GATTCharacteristicHandler(BluetoothGattCharacteristic androidCharacteristic, GATTHandler parent) {
        this.androidCharacteristic = androidCharacteristic;
        this.parent = parent;
    }
    
    @Override
    public UUID getType() {
        return androidCharacteristic.getUuid();
    }


    @Override
    public boolean canBroadcast() {
        return (androidCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_BROADCAST) != 0;
    }
    
    @Override
    public boolean canRead() {
        return (androidCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) != 0;
    }
    
    @Override
    public boolean canWriteWithoutResponse() {
        return (androidCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) != 0;
    }
    
    @Override
    public boolean canWriteWithResponse() {
        return (androidCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) != 0;
    }
    
    @Override
    public boolean canNotify() {
        return (androidCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0;
    }
    
    @Override
    public boolean canIndicate() {
        return (androidCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE) != 0;
    }
    
    @Override
    public boolean canAuthenticatedSignedWrite() {
        return (androidCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_SIGNED_WRITE) != 0;
    }
    
    @Override
    public boolean hasExtendedProperties() {
        return (androidCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_EXTENDED_PROPS) != 0;
    }
    
    @Override
    public void discoverDescriptors(Consumer<CoreError> callback) {
        // On Android, all service discovery happens at the main
        // service discovery, so no further action is needed.
        if (parent.isConnected()) {
            // Success
            callback.accept(null);
        } else {
            // Report that we are not connected
            callback.accept(parent.endpoint.createError(GATTError.NOT_CONNECTED));
        }
    }
    
    @Override
    public Set<? extends GATTDescriptorRepresentation> getDescriptors() {
        if (descriptors == null) {
            descriptors = new HashSet<>();
            for (BluetoothGattDescriptor descriptor : androidCharacteristic.getDescriptors()) {
                descriptors.add(parent.getDescriptorHandlerForAndroidDescriptor(descriptor));
            }
        }
        return descriptors;
    }
    
    @Override
    public void readValue(Consumer<CoreError> callback) {
        parent.enqueueTask(GATTTask.createReadCharacteristicTask(androidCharacteristic, callback));
    }
    
    @Override
    public byte[] getValue() {
        return androidCharacteristic.getValue();
    }
    
    @Override
    public void writeValueWithResponse(byte[] value, Consumer<CoreError> callback) {
        parent.enqueueTask(GATTTask.createWriteCharacteristicTask(androidCharacteristic, value, true, callback));
    }
    
    @Override
    public void writeValueWithoutResponse(byte[] value, Consumer<CoreError> callback) {
        parent.enqueueTask(GATTTask.createWriteCharacteristicTask(androidCharacteristic, value, false, callback));
    }
    
    @Override
    public void enableNI(Consumer<CoreError> callback, BiConsumer<GATTCharacteristicRepresentation, byte[]> niHandler) {
        parent.enqueueTask(GATTTask.createSetCharacteristicNotificationTask(androidCharacteristic, true, callback, niHandler));
    }
    
    @Override
    public void disableNI(Consumer<CoreError> callback) {
        parent.enqueueTask(GATTTask.createSetCharacteristicNotificationTask(androidCharacteristic, false, callback, null));
    }
}    
