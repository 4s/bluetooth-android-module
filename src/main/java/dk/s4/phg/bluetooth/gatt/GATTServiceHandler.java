package dk.s4.phg.bluetooth.gatt;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

import dk.s4.phg.baseplate.CoreError;

import dk.s4.phg.messages.support.gatt.GATTCharacteristicRepresentation;
import dk.s4.phg.messages.support.gatt.GATTError;
import dk.s4.phg.messages.support.gatt.GATTServiceRepresentation;

class GATTServiceHandler implements GATTServiceRepresentation {

    private final BluetoothGattService androidService;
    private final GATTHandler parent;
    private Set<GATTServiceHandler> includedServices = null;
    private Set<GATTCharacteristicHandler> characteristics = null;
    
    GATTServiceHandler(BluetoothGattService androidService, GATTHandler parent) {
        this.androidService = androidService;
        this.parent = parent;
    }
    
    @Override
    public UUID getType() {
        return androidService.getUuid();
    }
    
    @Override
    public boolean isPrimary() {
        return androidService.getType() == BluetoothGattService.SERVICE_TYPE_PRIMARY;
    }
    
    @Override
    public void findIncludedServices(Consumer<CoreError> callback) {
        // On Android, all service discovery happens at the main
        // service discovery, so no further action is needed.
        if (parent.isConnected()) {
            // Success
            callback.accept(null);
        } else {
            // Report that we are not connected
            callback.accept(parent.endpoint.createError(GATTError.NOT_CONNECTED));
        }
    }
    
    @Override
    public Set<? extends GATTServiceRepresentation> getIncludedServices() {
        if (includedServices == null) {
            includedServices = new HashSet<>();
            for (BluetoothGattService service : androidService.getIncludedServices()) {
                includedServices.add(parent.getServiceHandlerForAndroidService(service));
            }
        }
        return includedServices;
    }
    
    @Override
    public void discoverCharacteristics(UUID characteristicType, Consumer<CoreError> callback) {
        // On Android, all service discovery happens at the main
        // service discovery, so no further action is needed.
        if (parent.isConnected()) {
            // Success
            callback.accept(null);
        } else {
            // Report that we are not connected
            callback.accept(parent.endpoint.createError(GATTError.NOT_CONNECTED));
        }
    }
    
    @Override
    public Set<? extends GATTCharacteristicRepresentation> getCharacteristics() {
        if (characteristics == null) {
            characteristics = new HashSet<>();
            for (BluetoothGattCharacteristic characteristic : androidService.getCharacteristics()) {
                characteristics.add(parent.getCharacteristicHandlerForAndroidCharacteristic(characteristic));
            }
        }
    return characteristics;
    } 
}
