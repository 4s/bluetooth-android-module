package dk.s4.phg.bluetooth.gatt;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.BiConsumer;

import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.bluetooth.BluetoothAdapterHandler;

import dk.s4.phg.messages.support.device.BluetoothDeviceRepresentation;
import dk.s4.phg.messages.support.gatt.GATTCharacteristicRepresentation;
import dk.s4.phg.messages.support.gatt.GATTDescriptorRepresentation;
import dk.s4.phg.messages.support.gatt.GATTError;
import dk.s4.phg.messages.support.gatt.GATTProducerEndpoint;
import dk.s4.phg.messages.support.gatt.GATTServerRepresentation;
import dk.s4.phg.messages.support.gatt.GATTServiceRepresentation;



/**
 * The business logic of representing a single GATT server instance.
 *
 * Threading note: All methods in this class must be invoked on the module thread.
 */
public class GATTHandler implements GATTServerRepresentation, GATTCallback {

    // The Android-provided object representing the Bluetooth device offering the GATT server
    private final BluetoothDevice device;

    // The GATT producer InterfaceEndpoint exposing this GATT server to the PHG Core world
    final GATTProducerEndpoint endpoint;

    // The Android Bluetooth adapter wrapper managing all communication with Bluetooth on Android
    private final BluetoothAdapterHandler adapter;

    // The Android-provided object representing the GATT server connection
    private BluetoothGatt gattConnection = null;

    // Queue of active and pending tasks for this GATT server. Valid only in the CONNECTED state
    private final Queue<GATTTask> gattTasks = new LinkedList<>(); 

    // Services provided by this GATT server found so far
    private final Set<GATTServiceHandler> services = new HashSet<>();

    // Mapping BluetoothGattService objects to GATTServiceHandlers
    private final HashMap<BluetoothGattService, GATTServiceHandler> serviceHandlerByAndroidService = new HashMap<>();

    // Mapping BluetoothGattCharacteristic objects to GATTCharacteristicHandlers
    private final HashMap<BluetoothGattCharacteristic, GATTCharacteristicHandler>
        characteristicHandlerByAndroidCharacteristic = new HashMap<>();

    // Mapping BluetoothGattDescriptor objects to GATTDescriptorHandlers
    private final HashMap<BluetoothGattDescriptor, GATTDescriptorHandler>
        descriptorHandlerByAndroidDescriptor = new HashMap<>();

    // Mapping BluetoothGattCharacteristic objects to their notification handlers
    private final HashMap<BluetoothGattCharacteristic, BiConsumer<GATTCharacteristicRepresentation, byte[]> > niHandlers
        = new HashMap<>();
    
    // Connection state machine
    private enum ConnectionState {
        DISCONNECTED, CONNECTING, CONNECTED, DISCONNECT_PENDING, DISCONNECTING;
    }
    private ConnectionState connectionState = ConnectionState.DISCONNECTED;

    // connect() callback - valid only in the CONNECTING state
    private Consumer<CoreError> connectCallback = null; 

    // discoverServices has been completed for this GATT server
    private boolean fullServiceDiscoveryComplete = false;
    
    

    /**
     * The business logic of representing a single GATT server instance.
     *
     * @param device   The Android-provided object representing the
     *                 Bluetooth device offering the GATT server.
     * @param endpoint The GATT producer InterfaceEndpoint exposing
     *                 this GATT server to the PHG Core world.
     * @param adapter  The Android Bluetooth adapter wrapper managing
     *                 all communication with Bluetooth on Android.
     */
    public GATTHandler(BluetoothDevice device, GATTProducerEndpoint endpoint, BluetoothAdapterHandler adapter) {
        if (device == null || endpoint == null || adapter == null) {
            throw new IllegalArgumentException("No argument can be null");
        }
        this.device = device;
        this.endpoint = endpoint;
        this.adapter = adapter;
    }


    
    /* ********************************************************************** */
    /*                                                                        */
    /*                GATTServerRepresentation implementation                 */
    /*                                                                        */
    /* ********************************************************************** */

    @Override
    public void connect(Consumer<CoreError> callback) {
        if (connectionState != ConnectionState.DISCONNECTED) {
            callback.accept(endpoint.createError(GATTError.ALREADY_CONNECTED));
            return;
        }
        connectCallback = callback;
        connectionState = ConnectionState.CONNECTING;

        if (gattConnection == null) { // First and only time we are here!
            try {
                gattConnection = adapter.connectGatt(device, false, this);
                if (gattConnection == null) throw new NullPointerException("GATT server connection is null");
            } catch (RuntimeException e) {
                // As far as we know, this is not even possible
                connectionState = ConnectionState.DISCONNECTED;
                connectCallback = null;
                callback.accept(endpoint.createError(GATTError.IO_ERROR));
                return;
            }
        } else {
            adapter.connectGatt(gattConnection);
        }
    }

    @Override
    public void disconnect() {
        if (connectionState == ConnectionState.CONNECTED) {
            connectionState = ConnectionState.DISCONNECTING;
            adapter.disconnectGatt(gattConnection);
        } else if (connectionState == ConnectionState.CONNECTING) {
            Consumer<CoreError> callback = connectCallback;
            connectCallback = null;
            connectionState = ConnectionState.DISCONNECT_PENDING;
            if (callback != null) callback.accept(endpoint.createError(GATTError.ABORTED));
        }
    }

    @Override
    public void discoverServices(UUID serviceType, Consumer<CoreError> callback) {
        // Ignore the serviceType argument. Currently, Android does not support
        // lightweight service discovery.
        enqueueTask(GATTTask.createDiscoverServicesTask(callback));
    }

    @Override
    public Set<? extends GATTServiceRepresentation> getServices() {
        return services;
    }



    /* ********************************************************************** */
    /*                                                                        */
    /*                      GATTCallback implementation                       */
    /*                                                                        */
    /* ********************************************************************** */
    
    @Override
    public void onConnectionStateChange(Status status, boolean isConnected) {
        if (status == Status.SUCCESS) {
            if (isConnected) {
                // connect() succeeded
                switch (connectionState) {
                case CONNECTING:
                    Consumer<CoreError> callback = connectCallback;
                    connectCallback = null;
                    connectionState = ConnectionState.CONNECTED;
                    // connect() success
                    if (callback != null) callback.accept(null);
                    break;
                case CONNECTED:
                    // ignore
                    break;
                case DISCONNECT_PENDING:
                    connectionState = ConnectionState.DISCONNECTING;
                    //no break here!
                default: // DISCONNECTING, DISCONNECTED
                    // Shouldn't be connected now. Disconnect
                    adapter.disconnectGatt(gattConnection);
                }
                return;
            }
        }
        
        // disconnect succeeded (status == SUCCESS) or a connection
        // failure happened. This is used by Android to communicate
        // failed attempts to connect as well as disconnections for
        // any other reason (peer hung up, went out of range etc.)


        if (connectionState == ConnectionState.CONNECTING) {
            Consumer<CoreError> callback = connectCallback;
            connectCallback = null;
            connectionState = ConnectionState.DISCONNECTING;
            // connect() failed
            if (callback != null) callback.accept(endpoint.createError(status == Status.DEVICE_NOT_FOUND ?
                                                                       GATTError.TIMEOUT : GATTError.ABORTED));
            if (connectionState != ConnectionState.DISCONNECTING) return;
        }
        
        // Map the status received from Android to a GATT interface error code
        // for the Disconnected event
        GATTProducerEndpoint.DisconnectedReason reason;
        switch (status) {
        case SUCCESS:
            // Disconnect succeeded.
            reason = GATTProducerEndpoint.DisconnectedReason.MY_DISCONNECT;
            break;
        case OUT_OF_RANGE:
            // Disconnected because of weak signal / peer out of range
            reason = GATTProducerEndpoint.DisconnectedReason.OUT_OF_RANGE;
            break;
        case DISCONNECTED_BY_PEER:
            // The peer disconnected /  turned off
            reason = GATTProducerEndpoint.DisconnectedReason.PEER_DISCONNECT;
            break;
        case DEVICE_NOT_FOUND:
            // The peer device could not be found within the allotted time
            reason = GATTProducerEndpoint.DisconnectedReason.TIMEOUT;
            break;
        default:
            // Other I/O problems
            reason = GATTProducerEndpoint.DisconnectedReason.IO_ERROR;
        }
        
        connectionState = ConnectionState.DISCONNECTED;
        flushTaskQueue();
        endpoint.serverDisconnected(this, reason);
    }

    
    @Override
    public void onServicesDiscovered(Status status) {
        if (status == GATTCallback.Status.SUCCESS && !fullServiceDiscoveryComplete) {
            fullServiceDiscoveryComplete = true;
            for (BluetoothGattService s : adapter.getServices(gattConnection)) {
                services.add(getServiceHandlerForAndroidService(s));
            }
        }
        GATTError ge = status2gattError(status);
        CoreError ce = ge == null ? null : endpoint.createError(ge);
        finishTask(ce, GATTTask.DiscoverServicesTask.class);
    }

    
    @Override
    public void onCharacteristicRead(BluetoothGattCharacteristic characteristic, Status status) {
        GATTError ge = status2gattError(status);
        CoreError ce = ge == null ? null : endpoint.createError(ge);
        finishTask(ce, GATTTask.ReadCharacteristicTask.class);
    }

    @Override
    public void onCharacteristicWrite(BluetoothGattCharacteristic characteristic, Status status) {
        GATTError ge = status2gattError(status);
        CoreError ce = ge == null ? null : endpoint.createError(ge);
        finishTask(ce, GATTTask.WriteCharacteristicTask.class);
    }

    @Override
    public void onCharacteristicChanged(BluetoothGattCharacteristic characteristic, byte[] value) {
        BiConsumer<GATTCharacteristicRepresentation, byte[]> niHandler = niHandlers.get(characteristic);
        if (niHandler != null) {
            GATTCharacteristicHandler charHandler = getCharacteristicHandlerForAndroidCharacteristic(characteristic);
            niHandler.accept(charHandler, value);
        }
    }

    @Override
    public void onDescriptorRead(BluetoothGattDescriptor descriptor, Status status) {}
    @Override
    public void onDescriptorWrite(BluetoothGattDescriptor descriptor, Status status) {
        GATTError ge = status2gattError(status);
        CoreError ce = ge == null ? null : endpoint.createError(ge);
        finishTask(ce, GATTTask.SetCharacteristicNotificationTask.class);
}
    @Override
    public void onReliableWriteCompleted(Status status) {}



    /* ********************************************************************** */
    /*                                                                        */
    /*    Mapping between the represented and corresponding Android objects   */
    /*                                                                        */
    /* ********************************************************************** */

    GATTServiceHandler getServiceHandlerForAndroidService(BluetoothGattService androidService) {
        return serviceHandlerByAndroidService.computeIfAbsent
            (androidService, as -> new GATTServiceHandler(as, this));
    }

    GATTCharacteristicHandler getCharacteristicHandlerForAndroidCharacteristic(BluetoothGattCharacteristic
                                                                               androidCharacteristic) {
        return characteristicHandlerByAndroidCharacteristic.computeIfAbsent
            (androidCharacteristic, ac ->  new GATTCharacteristicHandler(ac, this));
    }

    GATTDescriptorHandler getDescriptorHandlerForAndroidDescriptor(BluetoothGattDescriptor androidDescriptor) {
        return descriptorHandlerByAndroidDescriptor.computeIfAbsent
            (androidDescriptor, ad -> GATTDescriptorHandler.create(ad, this));
    }
    

    /* ********************************************************************** */
    /*                                                                        */
    /*                                Tasks                                   */
    /*                                                                        */
    /* ********************************************************************** */

    private void discoverServices() {
        if (!fullServiceDiscoveryComplete) {
            if (!adapter.discoverServices(gattConnection)) {
                onServicesDiscovered(GATTCallback.Status.FAILURE);
            }
        } else {
            onServicesDiscovered(GATTCallback.Status.SUCCESS);
        }
    }

    private void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (!adapter.readCharacteristic(gattConnection, characteristic)) {
            onCharacteristicRead(characteristic, GATTCallback.Status.FAILURE);
        }
    }

    private void writeCharacteristic(BluetoothGattCharacteristic characteristic, byte[] value, boolean withResponse) {
        if (!adapter.writeCharacteristic(gattConnection, characteristic, value, withResponse)) {
            onCharacteristicWrite(characteristic, GATTCallback.Status.FAILURE);
        }
    }

    private void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enable,
                                               BiConsumer<GATTCharacteristicRepresentation, byte[]> niHandler) {
        if (!adapter.setCharacteristicNotification(gattConnection, characteristic, enable)) {
            CoreError ce = endpoint.createError(GATTError.IO_ERROR);
            finishTask(ce, GATTTask.SetCharacteristicNotificationTask.class);
        } else {
            niHandlers.put(characteristic,niHandler);
        }
    }


    
    /* ********************************************************************** */
    /*                                                                        */
    /*                         Task queue operations                          */
    /*                                                                        */
    /* ********************************************************************** */

    /** Is the server currently connected? */
    boolean isConnected() { return connectionState == ConnectionState.CONNECTED; }

    /** Add a task to the task queue - starting it if the queue was empty */
    void enqueueTask(GATTTask task) {
        if (!isConnected()) {
            task.callback.accept(endpoint.createError(GATTError.NOT_CONNECTED));
            return;
        }
        // Start the task immediately if the queue was empty
        boolean startTask = gattTasks.isEmpty();
        gattTasks.add(task);
        
        if (startTask) executeTask(task);
    }
    
    // Clear the task queue, sending the "Operation Aborted" error code to each pending task
    private void flushTaskQueue() {
        // Nothing to do with an empty queue
        if (gattTasks.isEmpty()) return;

        CoreError aborted = endpoint.createError(GATTError.ABORTED);
        do {
            GATTTask task = gattTasks.remove();
            task.callback.accept(aborted);
        } while (!gattTasks.isEmpty());
    }
    
    // Get the currently executing task
    private GATTTask currentTask() {
        if (isConnected()) {
            return gattTasks.peek();
        } else {
            return null;
        }
    }
    
    // Finish the currently executing task.
    private void finishTask(CoreError ce, Class<? extends GATTTask> clazz) {
        // Bail out if the current task (or state) is not what we expect
        GATTTask task = currentTask();
        if (task == null || !(clazz.isInstance(task))) return;

        task = gattTasks.remove();
        task.callback.accept(ce);

        // Read the next task in the queue - while checking that we
        // are still connected as the callback in the previous line
        // could have disconnected.
        task = currentTask();
        if (task != null) {
            // More tasks in queue. Start the next one
            executeTask(task);
        }
    }
    
    private void executeTask(GATTTask task) {
        if (task instanceof GATTTask.DiscoverServicesTask) {
            discoverServices();
        } else if (task instanceof GATTTask.ReadCharacteristicTask) {
            GATTTask.ReadCharacteristicTask rcTask = (GATTTask.ReadCharacteristicTask) task;
            readCharacteristic(rcTask.characteristic);
        } else if (task instanceof GATTTask.WriteCharacteristicTask) {
            GATTTask.WriteCharacteristicTask wcTask = (GATTTask.WriteCharacteristicTask) task;
            writeCharacteristic(wcTask.characteristic, wcTask.value, wcTask.withResponse);
        } else if (task instanceof GATTTask.SetCharacteristicNotificationTask) {
            GATTTask.SetCharacteristicNotificationTask scnTask = (GATTTask.SetCharacteristicNotificationTask) task;
            setCharacteristicNotification(scnTask.characteristic, scnTask.enable, scnTask.niHandler);
        }
    }
    
    
    
    /* ********************************************************************** */
    /*                                                                        */
    /*                     Convert Error / Status Codes                       */
    /*                                                                        */
    /* ********************************************************************** */

    GATTError status2gattError(Status status) {
        switch (status) {
        case SUCCESS:
            return null;
        case READ_NOT_PERMITTED:
        case WRITE_NOT_PERMITTED:
            return GATTError.NOT_PERMITTED;
        case INSUFFICIENT_AUTHENTICATION:
            return GATTError.BAD_AUTHENTICATION;
        case REQUEST_NOT_SUPPORTED:
            return GATTError.NOT_SUPPORTED;
        case INVALID_OFFSET:
        case INVALID_ATTRIBUTE_LENGTH:
            return GATTError.BAD_ATTRIBUTE;
        case INSUFFICIENT_ENCRYPTION:
            return GATTError.BAD_ENCRYPTION;
        default:
            return GATTError.IO_ERROR;
        }
    }

    
}
