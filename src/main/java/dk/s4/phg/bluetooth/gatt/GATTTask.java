package dk.s4.phg.bluetooth.gatt;

import dk.s4.phg.baseplate.CoreError;

import android.bluetooth.BluetoothGattCharacteristic;
import dk.s4.phg.messages.support.gatt.GATTCharacteristicRepresentation;

import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.BiConsumer;

abstract class GATTTask {

    final Consumer<CoreError> callback;

    private GATTTask(Consumer<CoreError> callback) {
        this.callback = callback;
    }
    
    static class DiscoverServicesTask extends GATTTask {
        private DiscoverServicesTask(Consumer<CoreError> callback) {
            super(callback);
        }
    }

    static GATTTask createDiscoverServicesTask(Consumer<CoreError> callback) {
        return new DiscoverServicesTask(callback);
    }

    static class ReadCharacteristicTask extends GATTTask {
        final BluetoothGattCharacteristic characteristic;
        private ReadCharacteristicTask(BluetoothGattCharacteristic characteristic, Consumer<CoreError> callback) {
            super(callback);
            this.characteristic = characteristic;
        }
    }

    static GATTTask createReadCharacteristicTask(BluetoothGattCharacteristic characteristic, Consumer<CoreError> callback) {
        return new ReadCharacteristicTask(characteristic, callback);
    }

    static class WriteCharacteristicTask extends GATTTask {
        final BluetoothGattCharacteristic characteristic;
        final byte[] value;
        final boolean withResponse;
        private WriteCharacteristicTask(BluetoothGattCharacteristic characteristic, byte[] value, boolean withResponse,
                                        Consumer<CoreError> callback) {
            super(callback);
            this.characteristic = characteristic;
            this.value = value;
            this.withResponse = withResponse;
        }
    }

    static GATTTask createWriteCharacteristicTask(BluetoothGattCharacteristic characteristic, byte[] value, boolean withResponse,
                                                  Consumer<CoreError> callback) {
        return new WriteCharacteristicTask(characteristic, value, withResponse, callback);
    }

    static class SetCharacteristicNotificationTask extends GATTTask {
        final BluetoothGattCharacteristic characteristic;
        final boolean enable;
        final BiConsumer<GATTCharacteristicRepresentation, byte[]> niHandler;
        private SetCharacteristicNotificationTask(BluetoothGattCharacteristic characteristic, boolean enable,
                                                  Consumer<CoreError> callback,
                                                  BiConsumer<GATTCharacteristicRepresentation, byte[]> niHandler) {
            super(callback);
            this.characteristic = characteristic;
            this.enable = enable;
            this.niHandler = niHandler;
        }
    }

    static GATTTask createSetCharacteristicNotificationTask(BluetoothGattCharacteristic characteristic, boolean enable,
                                                            Consumer<CoreError> callback,
                                                            BiConsumer<GATTCharacteristicRepresentation, byte[]> niHandler) {
        return new SetCharacteristicNotificationTask(characteristic, enable, callback, niHandler);
    }
    
    
}
