package dk.s4.phg.bluetooth.gatt;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

import dk.s4.phg.baseplate.CoreError;

import dk.s4.phg.messages.support.gatt.GATTDescriptorRepresentation;
import dk.s4.phg.messages.support.gatt.GATTError;

abstract class GATTDescriptorHandler implements GATTDescriptorRepresentation {

    private final BluetoothGattDescriptor androidDescriptor;
    private final GATTHandler parent;
    
    private GATTDescriptorHandler(BluetoothGattDescriptor androidDescriptor, GATTHandler parent) {
        this.androidDescriptor = androidDescriptor;
        this.parent = parent;
    }

    static GATTDescriptorHandler create(BluetoothGattDescriptor androidDescriptor, GATTHandler parent) {
        // Not yet implemented. Should switch on the androidDescriptor.getUuid() value and create a
        // subtype of GATTDescriptorHandler implementing the appropriate GATTDescriptorRepresentation interface
        throw new UnsupportedOperationException();
    }
    
    @Override
    public void readValue(Consumer<CoreError> callback) {

    }
}
