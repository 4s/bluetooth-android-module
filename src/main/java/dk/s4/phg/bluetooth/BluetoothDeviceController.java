package dk.s4.phg.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.ParcelUuid;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.baseplate.Logger;
import dk.s4.phg.bluetooth.exceptions.BluetoothAccessException;
import dk.s4.phg.bluetooth.gatt.GATTHandler;
import dk.s4.phg.messages.support.bluetooth_device_control.BluetoothDeviceControlProducer;
import dk.s4.phg.messages.support.device.BluetoothDeviceRepresentation;
import dk.s4.phg.messages.support.device_control.DeviceControlProducer;
import dk.s4.phg.messages.support.gatt.GATTProducerEndpoint;
import dk.s4.phg.messages.support.gatt.GATTServerRepresentation;
import dk.s4.phg.messages.support.serial_port.SerialPortProducerEndpoint;




/**
 * DeviceControl and BluetoothDeviceControl business functionality.
 *
 * This class implements the business functionality of the
 * DeviceControl and BluetoothDeviceControl producer endpoints for the
 * Android Bluetooth stack. Futhermore, it is responsible for
 * attaching the individual connected Bluetooth devices to the
 * appropriate protocol endpoints according to the Bluetooth profiles
 * discovered on the individual device.
 *
 * This abstract class expects a subclass to implement the
 * getBluetoothPairing() method of the BluetoothDeviceControlProducer.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author Mads Møller Jensen, The Alexandra Institute.
 * @author <a href="mailto:thomas.holst@alexandra.dk">Thomas
 *         Holst</a>, The Alexandra Institute.
 *
 */
public abstract class BluetoothDeviceController
             implements DeviceControlProducer<BluetoothDeviceRepresentation>,
                        BluetoothDeviceControlProducer {

    // The Android app activity
    private final Activity appActivity;

    // PHG core logger
    private final Logger logger;

    // Enqueue actions on the module thread
    private final Consumer<Runnable> enqueueActConsumer;

    // Factory for creating BluetoothDeviceRepresentation objects
    private final BluetoothDeviceRepresentation
        .BluetoothDeviceRepresentationFactory
        bluetoothDeviceRepresentationFactory;

    // The serial port producer endpoint (needed to create new
    // SerialPortHandler objects)
    private final SerialPortProducerEndpoint sppEndpoint;

    // The GATT producer endpoint (needed to create new
    // GATTHandler objects)
    private final GATTProducerEndpoint gattEndpoint;

    // The adapter handler responsible for the interface to the
    // Android Bluetooth adapter API
    private final BluetoothAdapterHandler bluetoothAdapterHandler;




    /**
     * Constructor
     * @param appActivity    The Android activity
     * @param logger         The PHG core module logger
     * @param enqueueActConsumer A thread-safe Consumer for enqueuing
     *                       runnable actions for execution on the module
     *                       thread.
     * @param bluetoothDeviceRepresentationFactory Factory for creating
     *                       BluetoothDeviceRepresentation objects.
     * @param sppEndpoint    The serial producer endpoint
     * @param gattEndpoint   The GATT producer endpoint
     *
     */
    public BluetoothDeviceController
        (Activity appActivity,
         Logger logger,
         Consumer<Runnable> enqueueActConsumer,
         BluetoothDeviceRepresentation.BluetoothDeviceRepresentationFactory
         bluetoothDeviceRepresentationFactory,
         SerialPortProducerEndpoint sppEndpoint,
         GATTProducerEndpoint gattEndpoint) {

        // Store arguments as class members
        this.logger = logger;
        this.enqueueActConsumer = enqueueActConsumer;
        this.appActivity = appActivity;
        this.bluetoothDeviceRepresentationFactory
            = bluetoothDeviceRepresentationFactory;
        this.sppEndpoint = sppEndpoint;
        this.gattEndpoint = gattEndpoint;



        // Increase the thread priority in order to minimize timing issues.
// FIXME: Skal opdateres -- N.B. Hvad indebærer "minimize timing issues" helt præcist??!
//        setPriority((NORM_PRIORITY + MAX_PRIORITY) / 2);


        // FIXME: What is these lines doing, and are they necessary??
        //    appActivity.requestPermissions( new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        //        Settings.Secure.getString(appActivity.getContentResolver(), Settings.Secure.ANDROID_ID);



        // Create the adapter handler responsible for the Bluetooth
        // adapter interface
        bluetoothAdapterHandler = new BluetoothAdapterHandler
            (appActivity.getApplicationContext(), mCallback, enqueueActConsumer);
    }




    /* ********************************************************************** */
    /*                                                                        */
    /*                  DeviceControlProducer implementation                  */
    /*                                                                        */
    /* ********************************************************************** */

    // Currently ongoing searches. Multiple overlapping searches could
    // be started concurrently by (different) consumers of the
    // DeviceControl interface, so we need to keep track of them all
    // and make sure all search results from the one search executed
    // by the BluetoothAdapterHandler are communicated to all.
    private Set<SearchResultCollector<BluetoothDeviceRepresentation> >
        activeSearches = new HashSet<>();

    // Devices found in the current search executed by the
    // BluetoothAdapterHandler. This set must be null if no search is
    // currently being executed.
    private Set<BluetoothDeviceRepresentation> foundDevices = null;

    @Override
    public void search(SearchResultCollector<BluetoothDeviceRepresentation>
                       collector) {

        // Store the search in the set of active searches
        // Return if this search has already been started
        if (!activeSearches.add(collector)) return;

        if (foundDevices == null) {
            // No current searches. We must initiate a search at the
            // BluetoothAdapterHandler

            // Get all bonded devices as they are always included in
            // search result sets
            Set<BluetoothDevice> bondedDevices;
            try {
                bondedDevices = bluetoothAdapterHandler.getBondedDevices();
                // Start searching at the BluetoothAdapterHandler
                bluetoothAdapterHandler.search();
            } catch (BluetoothAccessException e) {
                logger.error("Bluetooth not available", logger.createError(e));

                // FIXME: We currently have no way to report this
                // error back to the application

                return;
            }

            foundDevices = new HashSet<>();
            for (BluetoothDevice androidDev : bondedDevices) {
                foundDevices.add(deviceFromAndroidDevice(androidDev));
            }

        }

        // Send all devices in the foundDevices set
        for (BluetoothDeviceRepresentation device : foundDevices) {
            collector.addResult(device);
        }
    }

    @Override
    public void stopSearch(SearchResultCollector<BluetoothDeviceRepresentation>
                           collector) {

        // Remove the search collector from the set of active searches
        // Return if this search is not started
        if (!activeSearches.remove(collector)) return;

        if (activeSearches.isEmpty()) {
            // Last search was stopped. Stop searching at the
            // BluetoothAdapterHandler and clear the result set
            foundDevices.clear();
            foundDevices = null;
            bluetoothAdapterHandler.cancelSearch();
        }
    }

    @Override
    public void solicitDeviceTransport(BluetoothDeviceRepresentation device,
                                       Consumer<CoreError> callback) {

        BluetoothDevice btDev;

        try {
            // Lookup the Android Bluetooth device object
            btDev = androidDeviceFromDevice(device);
        } catch (Exception e) {
            // Device not available
            callback.accept(logger.createError(e));
            return;
        }
        // Update the BluetoothDeviceRepresentation object (this has
        // the side-effect of notifying the world about these changes)
        deviceFromAndroidDevice(btDev);

        // Reply to the solicitation if that has not already happened
        callback.accept(null);
    }




    /* ********************************************************************** */
    /*                                                                        */
    /*              BluetoothDeviceControlProducer implementation             */
    /*                                                                        */
    /* ********************************************************************** */

    // FIXME: Rather than just one bonding, we should maintain a queue of
    // pending bonding requests
    private Consumer<CoreError> bondCallback;


    @Override
    public void bond(BluetoothDeviceRepresentation device,
                     Consumer<CoreError> callback) {

        if (bondCallback != null){
            // FIXME: We should maintain a queue instead of failing here...
            callback.accept(logger.createError("Already bonding..."));
            return;
        }

        try {
            // Lookup the Android Bluetooth device object and start bonding
            bluetoothAdapterHandler.bond(androidDeviceFromDevice(device));
            bondCallback = callback;
        } catch (Exception e) {
            // FIXME: Proper exception definitions still missing in
            // BluetoothDeviceControl.proto and
            // BluetoothDeviceControlProducerEndpoint
            callback.accept(logger.createError(e));
            return;
        }

        ////FIXME: FOR TESTING / DEBUGGING WITHOUT BASEPLATE (do we
        // still need this??)
        // Intent searchResultIntent = new Intent();
        // searchResultIntent.setAction("PHG_BLUETOOTH_BOND_DONE");
        // appActivity.getApplicationContext().sendBroadcast(searchResultIntent);

    }

    @Override
    public void unbond(BluetoothDeviceRepresentation device,
                       Consumer<CoreError> callback) {

        BluetoothDevice btDev;

        try {
            // Lookup the Android Bluetooth device object
            btDev = androidDeviceFromDevice(device);
        } catch (BluetoothAccessException e) {
            // FIXME: Proper exception definitions still missing
            // in BluetoothDeviceControl.proto and
            // BluetoothDeviceControlProducerEndpoint
            callback.accept(logger.createError(e));
            return;
        }

        try {
            bluetoothAdapterHandler.unbond(btDev);
        } catch (Exception e) {
            // Report problem
            // FIXME see above
            callback.accept(logger.createError(e));
            return;
        }
        // Success
        callback.accept(null);
    }

    @Override
    public void setStandardPasskey(BluetoothDeviceRepresentation device) {
        try {
            bluetoothAdapterHandler.setStandardPin(androidDeviceFromDevice(device),
                                                   device.getStandardPasskey());
        } catch (Exception e) {
            // Ignore problems with the device argument - we cannot set the passkey in such cases
            logger.error("Failed attempt to set standard passkey", e);
        }
    }




    /* ********************************************************************** */
    /*                                                                        */
    /*  Conversions between BluetoothDevice and BluetoothDeviceRepresentation */
    /*                                                                        */
    /* ********************************************************************** */


    /**
     * Get the Bluetooth device matching a given hardwareString.
     *
     * Currently, the hardwareString is just the string representation
     * of the BD_ADDR (using upper-case letters). This may change when
     * Bluetooth LE support is properly implemented.
     *
     * A device is always constructed and returned when the argument
     * is correctly formatted.
     *
     * @param hwString The hardwareString (transportPersistentId
     *                 without the factory-specific prefix).
     * @return The Android Bluetooth device.
     * @throws IllegalArgumentException if the hwString format was
     *                                  invalid.
     * @throws BluetoothAccessException if Bluetooth cannot be
     *                                  accessed for some (specified
     *                                  by exception subclass) reason.
     */
    private BluetoothDevice androidDeviceFromHwString(String hwString)
             throws IllegalArgumentException, BluetoothAccessException {
        return bluetoothAdapterHandler.getDeviceForAddress(hwString);
    }

    /**
     * Get the hardwareString matching a given Bluetooth device.
     *
     * Currently, the hardwareString is just the string representation
     * of the BD_ADDR (using upper-case letters). This may change when
     * Bluetooth LE support is properly implemented.
     *
     * @param btDev The Android Bluetooth device.
     * @return      The hardwareString (transportPersistentId without
     *              the factory-specific prefix).

     */
    private String hwStringFromAndroidDevice(BluetoothDevice btDev) {
        return btDev.getAddress();
    }

    /**
     * Map of all (BluetoothDeviceRepresentation,BluetoothDevice)
     * pairs that we have ever come across.
     */
    private Map<BluetoothDeviceRepresentation,BluetoothDevice> knownDevices
        = new HashMap<>();

    /**
     * Return the BluetoothDevice matching a given
     * BluetoothDeviceRepresentation, creating it if necessary.
     *
     * The provided device object is *not* touched by this operation.
     *
     * @param device The representation to map to the "real" object.
     * @return The BluetoothDevice object matching device.
     * @throws IllegalArgumentException If the transportPersistentId
     *                                  of the device could not be
     *                                  understood.
     * @throws BluetoothAccessException if Bluetooth cannot be
     *                                  accessed for some (specified
     *                                  by exception subclass) reason.
     */
    private BluetoothDevice
        androidDeviceFromDevice(BluetoothDeviceRepresentation device)
               throws BluetoothAccessException, IllegalArgumentException {

        // Is this a known device?
        BluetoothDevice btDev = knownDevices.get(device);

        if (btDev == null) {
            // No, then get/create it. 
            btDev = androidDeviceFromHwString(device.getHardwareString());
            knownDevices.put(device,btDev);
        }
        return btDev;
    }

    /**
     * Return the fully updated BluetoothDeviceRepresentation matching
     * a given BluetoothDevice, creating it if necessary.
     *
     * The returned device object will be fully updated (bulk update)
     * with Bluetooth profiles attached to relevant endpoints. (As a
     * side-effect, this implies that subscribers of changes in this
     * device will be automatically notified/updated.)
     *
     * @param btDev The BluetoothDevice object to map to the
     *              representation object.
     * @return The representation matching btDev.
     */
    BluetoothDeviceRepresentation
                         deviceFromAndroidDevice(BluetoothDevice btDev) {

        // Lookup/generate the device
        BluetoothDeviceRepresentation device
            = bluetoothDeviceRepresentationFactory
            .getForHardwareString(hwStringFromAndroidDevice(btDev));


        // Save if this device was previously unknown
        BluetoothDevice btDev2 = knownDevices.putIfAbsent(device, btDev);
        if (btDev2 != null) {
            // Sanity check - should never happen!
            assert btDev == btDev2 : "BluetoothDevice mismatch";
        }

        // Update the device - disabling notifications
        BluetoothDeviceRepresentation.bulkChangeMe
            (device, (BluetoothDeviceRepresentation d) -> {

                // Copy the details...
                d.setTransportLinkCapable(true);
                d.setTransportUnlinkCapable(true);
                d.setBD_ADDR(btDev.getAddress());
                d.setName(btDev.getName());
                d.setBonded(btDev.getBondState()
                            == BluetoothDevice.BOND_BONDED);
                switch (btDev.getType()) {
                case BluetoothDevice.DEVICE_TYPE_CLASSIC:
                    d.setClassic(true);
                    d.setLE(false);
                    break;
                case BluetoothDevice.DEVICE_TYPE_LE:
                    d.setClassic(false);
                    d.setLE(true);
                    break;
                case BluetoothDevice.DEVICE_TYPE_DUAL:
                    d.setClassic(true);
                    d.setLE(true);
                    break;
                default:
                    d.setClassic(false);
                    d.setLE(false);
                }

                
                // GATT profile always exist on BLE devices
                if (d.isLE()) {
                    BluetoothDeviceRepresentation.GenericAttributeProfileRepresentation gpRep = null;
                    for (BluetoothDeviceRepresentation.BluetoothProfileRepresentation bpr : d.getProfiles()) {
                        if (bpr instanceof BluetoothDeviceRepresentation.GenericAttributeProfileRepresentation) {
                            gpRep = (BluetoothDeviceRepresentation.GenericAttributeProfileRepresentation)bpr;
                            break;
                        }
                    }

                    // Profile not created yet. Create and bind it
                    if (gpRep == null) {
                        GATTServerRepresentation gattServer = new GATTHandler(btDev, gattEndpoint, bluetoothAdapterHandler);
                        gpRep = d.new GenericAttributeProfileRepresentation(gattServer, gattEndpoint.getHandler(),
                                                                            gattEndpoint.getHandleForServer(gattServer));
                        d.getProfileSet().add(gpRep);
                    } else if (gpRep.getGATTServer() == null) {
                        GATTServerRepresentation gattServer = new GATTHandler(btDev, gattEndpoint, bluetoothAdapterHandler);
                        gpRep.setGATTServer(gattServer, gattEndpoint.getHandler(), gattEndpoint.getHandleForServer(gattServer));
                    }
                }

                // Walk through the remaining profiles
                ParcelUuid[] serviceUuids = btDev.getUuids();

                // Android returns null for pure BLE devices. When the
                // device is BLE-only, we are done as these devices
                // must expose a GATT server (which was added above)
                if (serviceUuids == null) return;

                // FIXME: Temporary hacked solution - we only support
                // devices offering a single serial port profile

                boolean hasSerial = false;
                for (ParcelUuid parcelService : serviceUuids) {
                    if (parcelService.getUuid()
                        .equals(BluetoothUUID.SERIAL_PORT_PROFILE)) {
                        hasSerial = true;
                        break;
                    }
                }

                if (hasSerial) {

                    BluetoothDeviceRepresentation.SerialPortProfileRepresentation
                        sppRep = null;
                    for (BluetoothDeviceRepresentation.BluetoothProfileRepresentation
                             bpr : d.getProfiles()) {
                        if (bpr instanceof BluetoothDeviceRepresentation
                                           .SerialPortProfileRepresentation) {
                            sppRep = (BluetoothDeviceRepresentation
                                      .SerialPortProfileRepresentation)bpr;
                            break;
                        }
                    }

                    // Profile not created yet. Create and bind it
                    if (sppRep == null) {
                        SerialPortHandler port
                            = new SerialPortHandlerWithIntercept(btDev, sppEndpoint);
                        sppRep = d.new SerialPortProfileRepresentation(port);
                        d.getProfileSet().add(sppRep);
                    } else if (sppRep.getSerialPort() == null) {
                        SerialPortHandler port
                            = new SerialPortHandlerWithIntercept(btDev, sppEndpoint);
                        sppRep.setSerialPort(port);
                    }
                }
            });

        return device;
    }

    /**
     * A serial port handler working around the bug in the Android
     * Bluetooth serial port createRfcommSocketToServiceRecord()
     * method causing a bonding with devices that are already bonded:
     * We notify the BluetoothAdapterHandler that a potential bonding
     * may be happening so that it can intercept the Pairing pop-ups.
     */
    private class SerialPortHandlerWithIntercept extends SerialPortHandler {
        public SerialPortHandlerWithIntercept(BluetoothDevice device, SerialPortProducerEndpoint sppEndpoint) {
            super(device, sppEndpoint);
        }
        protected void beforeOpenHook(BluetoothDevice device) {
            bluetoothAdapterHandler.interceptPairingBegin(device);
        }

        protected void afterOpenHook(BluetoothDevice device) {
            bluetoothAdapterHandler.interceptPairingEnd(device);
        }
    }



    /* ********************************************************************** */
    /*                                                                        */
    /*             Callbacks from the BluetoothAdapterHandler                 */
    /*                                                                        */
    /* ********************************************************************** */

    BluetoothAdapterCallback mCallback = new BluetoothAdapterCallback() {

        @Override
        public void onBondResponse(int bondStatus) {
            // Invoke on module thread
            enqueueActConsumer.accept(() -> {
                    // Not currently bonding? Bail out
                    if (bondCallback == null) return;

                    if(bondStatus == BluetoothDevice.BOND_BONDED) {
                        // accept(null) means "success"
                        bondCallback.accept(null);
                    } else if(bondStatus == BluetoothDevice.BOND_NONE) {
                        // FIXME: Errors not properly defined yet.
                        bondCallback.accept(   null    );
                    }
                    bondCallback = null;
                });
        }


        @Override
        public void onSearchResult(BluetoothDevice androidDev) {
            // Invoke on module thread
            enqueueActConsumer.accept(() -> {
                    // Active search? Otherwise bail out
                    if (foundDevices == null) return;

                    BluetoothDeviceRepresentation bdr
                        = deviceFromAndroidDevice(androidDev);
                    if (foundDevices.add(bdr)) {
                        // This was a new addition to the set. Report it to
                        // all collectors
                        for (SearchResultCollector<BluetoothDeviceRepresentation>
                                 collector : activeSearches) {
                            collector.addResult(bdr);
                        }
                    }
                });
        }
    };

    /* ********************************************************************** */
    /*                                                                        */
    /*                    Bluetooth Low Energy / GATT                         */
    /*                                                                        */
    /* ********************************************************************** */



    /**
     * Only used for testing
     */
    @Deprecated
    protected void discoverNewDevice(Context context, Intent intent) {
        bluetoothAdapterHandler.discoverNewDevice(context, intent);
    }
}
