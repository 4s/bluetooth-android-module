package dk.s4.phg.messages.support.device;




/**
 * A read-only representation of the BluetoothDevice class
 * defined in classes/Device.proto.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public interface BluetoothDevice extends DeviceTransport {

    /**
     * Get the BD_ADDR property
     *
     * @return The Bluetooth device address on the form
     *         "11:22:33:44:55:66"
     */
    String getBD_ADDR();
    
    /**
     * Get the Bluetooth name property
     *
     * @return The Bluetooth device name (as provided by the Generic
     *         Access Profile)
     */
    String getName();
    
    /**
     * Get the classic property
     *
     * @return This is a Bluetooth BD/EDR device
     */
    boolean isClassic();
    
    /**
     * Get the low energy property
     *
     * @return This is a Bluetooth Low Energy device
     */
    boolean isLE();
    
    /**
     * Get the bonded property
     *
     * @return We are bonded with this device at the Bluetooth protocol layer
     */
    boolean isBonded();

    /**
     * Get the BluetoothProfiles supported by this Bluetooth device
     *
     * @return An iterator over the profiles
     */
    Iterable<? extends BluetoothProfile> getProfiles();
    
    /**
     * A read-only representation of the BluetoothProfile class
     * defined in classes/Device.proto.
     */
    public interface BluetoothProfile {

        /**
         * Get the name of the service provided by the device
         * manufacturer in the SDP record's "Service Name" field.
         *
         * @return The name of this service
         */
        String getServiceName();

        /**
         * Get the description of the service provided by the device
         * manufacturer in the SDP record's "Service Description"
         * field.
         *
         * @return The service description
         */
        String getServiceDescription();

        /**
         * Get the name of the manufacturer provided by the device
         * manufacturer in the SDP record's "Provider Name" field.
         *
         * @return The manufacturer name
         */
        String getProviderName();
    }
    
    /**
     * This BluetoothProfile is an SPP (Serial Port Profile)
     */
    interface SerialPortProfile extends BluetoothProfile {
    }

    /**
     * This BluetoothProfile is an HDP (Health Device Profile)
     */
    interface HealthDeviceProfile extends BluetoothProfile {
    }

    /**
     * This BluetoothProfile is a GATT (Generic Attribute) profile
     */
    interface GenericAttributeProfile extends BluetoothProfile {
    }
}


