package dk.s4.phg.messages.support.serial_port;

import java.util.HashMap;
import java.util.function.Consumer;

import dk.s4.phg.baseplate.Callback;
import dk.s4.phg.baseplate.EventImplementation;
import dk.s4.phg.baseplate.FunctionImplementation;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.MetaData;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.baseplate.ObjId;
import dk.s4.phg.messages.interfaces.serial_port.Close;
import dk.s4.phg.messages.interfaces.serial_port.Open;
import dk.s4.phg.messages.interfaces.serial_port.OpenSuccess;
import dk.s4.phg.messages.interfaces.serial_port.Write;
import dk.s4.phg.messages.interfaces.serial_port.WriteSuccess;

/**
 * Serial port producer endpoint implementation.
 *
 * The serial port producer endpoint exposes serial port objects
 * extending the SerialPortRepresentation class for remote serial port
 * consumers.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public class SerialPortProducerEndpoint {

    /**
     * The interface endpoint implemented by this serial port producer
     * instance
     */
    InterfaceEndpoint interfaceEP;

    /**
     * Queue up a task for execution on the module thread
     */
    Consumer<Runnable> enqueueActionConsumer;

    /**
     * All existing SerialPortRepresentations. New elements are added
     * by the SerialPortRepresentation constructor, and elements are
     * never removed.
     */
    HashMap<ObjId,SerialPortRepresentation> ports = new HashMap<>();


    /**
     * Create a new endpoint for producing SerialPorts.
     *
     * Any SerialPortRepresentation created using this object will be
     * served at the interface endpoint represented by this object.
     * @param epFactory The module endpoint factory used to create
     *                  this endpoint.
     * @param enqueueActConsumer A thread-safe Consumer for enqueuing
     *                  runnable actions for execution on the module thread.
     */
    public SerialPortProducerEndpoint(ModuleBase.EndpointFactory epFactory, Consumer<Runnable> enqueueActConsumer) {

        enqueueActionConsumer = enqueueActConsumer;

        interfaceEP = epFactory.implementsProducerInterface("SerialPort");

        interfaceEP.addFunctionHandler("Open", new FunctionImplementation<Open, OpenSuccess>() {
            @Override
            public void apply(Open args, Callback<OpenSuccess> callback, MetaData meta) {

                final ObjId handle = ObjId.fromInt64(args.getSerialPortHandle());

                SerialPortRepresentation port = ports.get(handle);
                if (port == null) {
                    callback.error(interfaceEP.createError(1, "Unknown handle"), false);
                    return;
                }

                port.endpointOpen(callback, meta.sender());
            }
        });

        interfaceEP.addFunctionHandler("Write", new FunctionImplementation<Write, WriteSuccess>() {
            @Override
            public void apply(Write args, Callback<WriteSuccess> callback, MetaData meta) {

                final ObjId handle = ObjId.fromInt64(args.getSerialPortHandle());

                SerialPortRepresentation port = ports.get(handle);
                if (port == null) {
                    callback.error(interfaceEP.createError(1, "Unknown handle"), false);
                    return;
                }

                port.endpointWrite(callback, meta.sender(),
                        args.getMessage().toByteArray(),
                        args.getFlush());
            }
        });

        interfaceEP.addEventHandler("Close", new EventImplementation<Close>() {
            @Override
            public void accept(Close args, MetaData meta) {

                final ObjId handle = ObjId.fromInt64(args.getSerialPortHandle());

                SerialPortRepresentation port = ports.get(handle);
                if (port == null) {
                    // Ignore unknown port
                    return;
                }

                port.endpointClose(meta.sender());
            }
        });
    }
}
