package dk.s4.phg.messages.support.device;

import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.baseplate.ObjId;
import dk.s4.phg.baseplate.Peer;

import static dk.s4.phg.messages.classes.device.BluetoothDevice.BluetoothProfile
    .Profile.GENERIC_ATTRIBUTE_PROFILE;
import static dk.s4.phg.messages.classes.device.BluetoothDevice.BluetoothProfile
    .Profile.HEALTH_DEVICE_PROFILE;
import static dk.s4.phg.messages.classes.device.BluetoothDevice.BluetoothProfile
    .Profile.SERIAL_PORT_PROFILE;
import dk.s4.phg.messages.support.gatt.GATTServerRepresentation;
import dk.s4.phg.messages.support.serial_port.SerialPortRepresentation;
import dk.s4.phg.messages.support.gatt.GATTServerRepresentation;
import dk.s4.phg.messages.support.x73_20601_transport.X73_20601TransportRepresentation;

import java.util.AbstractSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;




/**
 * The business functionality covering the provided
 * BluetoothDevice class defined in classes/Device.proto
 *
 * This class provides the handles to manipulate a BluetoothDevice
 * object for the business part of a BluetoothDevice producer module.
 * This is done using a range of setters and getters for the
 * BluetoothDevice properties. Calling a setter will
 * automatically notify all consumers who subscribe to the particular
 * BluetoothDevice. To avoid too many such notifications,
 * updating multiple properties may be grouped together by temporarily
 * disabling the notification mechanism. See bulkChangeMe().
 *
 * Constructing a BluetoothDeviceRepresentation object is
 * only possible through the BluetoothDeviceRepresentationFactory
 * getForId() method.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public class BluetoothDeviceRepresentation
    extends DeviceTransportRepresentation implements BluetoothDevice {

    private String   bd_addr = "",
                     name = "",
                     standardPasskey = "";
    private boolean  classic = false,
                     le = false,
                     bonded = false;
    
    
    /**
     * Construction is only allowed from the inner
     * BluetoothDeviceRepresentationFactory class.
     */
    private BluetoothDeviceRepresentation(String tpid, ObjId handler,
                               DeviceTransportRepresentationFactory parent) {
        super(tpid, handler, parent);
    }

    /**
     * Set the BD_ADDR property
     *
     * @param bd_addr The Bluetooth device address on the form
     *                "11:22:33:44:55:66"
     * @throws IllegalArgumentException if the argument is null
     */
    public void setBD_ADDR(String bd_addr) {
        if (bd_addr == null) {
            throw new IllegalArgumentException("bd_addr cannot be null");
        }
        if (!this.bd_addr.equals(bd_addr)) {
            this.bd_addr = bd_addr;
            generalNotify();
        }
    }
    /**
     * Get the BD_ADDR property
     *
     * @return The Bluetooth device address on the form
     *         "11:22:33:44:55:66"
     */
    @Override
    public String getBD_ADDR() { return bd_addr; }

    /**
     * Set the Bluetooth name property
     *
     * @param name The Bluetooth device name (as provided by the
     *             Generic Access Profile)
     */
    public void setName(String name) {
        if (name == null) name = "";
        if (!this.name.equals(name)) {
            this.name = name;
            generalNotify();
        }
    }
    /**
     * Get the Bluetooth name property
     *
     * @return The Bluetooth device name (as provided by the Generic
     *         Access Profile)
     */
    @Override
    public String getName() { return name; }

    /**
     * Set the standard passkey property
     *
     * @param standardPasskey The PIN/passkey value to be used for
     * pairing. null or "" means 'no value set'
     */
    public void setStandardPasskey(String standardPasskey) {
        if (standardPasskey == null) standardPasskey = "";
        this.standardPasskey = standardPasskey;
    }
    /**
     * Get the standard passkey property
     *
     * The passkey may be set to something different than "" by other
     * modules. On pairing/bonding with a device, the Bluetooth
     * controller should check out this passkey and attempt to use it
     * for legacy (a.k.a. PIN) or passkey pairing before asking the
     * user for a code. For legacy pairing this string can be up to 16
     * characters (in UTF-8). For passkey pairing, the string must be
     * a 6-digit (base-10) number e.g. "012345".
     *
     * @return The passkey
     */
    public String getStandardPasskey() { return standardPasskey; }

    /**
     * Set the classic property
     *
     * @param classic This is a Bluetooth BD/EDR device
     */
    public void setClassic(boolean classic) {
        if (this.classic != classic) {
            this.classic = classic;
            generalNotify();
        }
    }
    /**
     * Get the classic property
     *
     * @return This is a Bluetooth BD/EDR device
     */
    @Override
    public boolean isClassic() { return classic; }

    /**
     * Set the low-energy property
     *
     * @param le This is a Bluetooth Low Energy device
     */
    public void setLE(boolean le) {
        if (this.le != le) {
            this.le = le;
            generalNotify();
        }
    }
    /**
     * Get the low energy property
     *
     * @return This is a Bluetooth Low Energy device
     */
    @Override
    public boolean isLE() { return le; }

    /**
     * Set the bonded property
     *
     * @param bonded This device is bonded at the Bluetooth protocol
     *               level
     */
    public void setBonded(boolean bonded) {
        if (this.bonded != bonded) {
            this.bonded = bonded;
            generalNotify();
        }
    }
    /**
     * Get the bonded property
     *
     * @return This deviceis bonded at the Bluetooth protocol level
     */
    @Override
    public boolean isBonded() { return bonded; }

    /**
     * Bluetooth profile representations
     */
    public abstract class BluetoothProfileRepresentation
        implements BluetoothDevice.BluetoothProfile {

        // Set to true when this profile is member of the
        // parent object's list of profiles
        boolean registered = false;
        private String name = "",
                       description = "",
                       provider = "";

        /**
         * Set the name of the service provided by the device
         * manufacturer in the SDP record's "Service Name" field.
         *
         * @param name The name of this service
         */
        public void setServiceName(String name) {
            if (name == null) name = "";
            if (registered && !this.name.equals(name)) {
                this.name = name;
                generalNotify();
            }
        }
        /**
         * Get the name of the service provided by the device
         * manufacturer in the SDP record's "Service Name" field.
         *
         * @return The name of this service
         */
        @Override
        public String getServiceName() { return name; }


        /**
         * Set the description of the service provided by the device
         * manufacturer in the SDP record's "Service Description"
         * field.
         *
         * @param description The service description
         */
        public void setServiceDescription(String description) {
            if (description == null) description = "";
            if (registered && !this.description.equals(description)) {
                this.description = description;
                generalNotify();
            }
        }
        /**
         * Get the description of the service provided by the device
         * manufacturer in the SDP record's "Service Description"
         * field.
         *
         * @return The service description
         */
        @Override
        public String getServiceDescription() { return description; }

        /**
         * Set the name of the manufacturer provided by the device
         * manufacturer in the SDP record's "Provider Name" field.
         *
         * @param name The manufacturer name
         */
        public void setProviderName(String name) {
            if (name == null) name = "";
            if (registered && !this.provider.equals(name)) {
                this.provider = name;
                generalNotify();
            }
        }
        /**
         * Get the name of the manufacturer provided by the device
         * manufacturer in the SDP record's "Provider Name" field.
         *
         * @return The manufacturer name
         */
        @Override
        public String getProviderName() { return provider; }
    }
    


    /**
     * This BluetoothProfileRepresentation is an SPP (Serial Port
     * Profile).
     *
     * The profile holds a SerialPortRepresentation instance which
     * connects to the actual serial port implementation. This value
     * must be != null when this profile is registered with a
     * BluetoothDeviceRepresentation object (added to its list of
     * profiles) which is constructed. I.e. the following invariant
     * applies:
     * 
     *    !registered || !constructed || serialPort != null
     */
    public class SerialPortProfileRepresentation
        extends BluetoothProfileRepresentation
        implements BluetoothDevice.SerialPortProfile {
        
        private SerialPortRepresentation serialPort;

        /** Default constructor */
        public SerialPortProfileRepresentation() {}

        /** Construct and initialize the serialPort */
        public SerialPortProfileRepresentation
            (SerialPortRepresentation serialPort) {
            this.serialPort = serialPort;
        }
        /**
         * Set the serial port representation.
         *
         * This must be done before the profile is registered with its
         * parent device or before the construction of the parent
         * device itself is complete.
         */
        public void setSerialPort(SerialPortRepresentation serialPort) {
            if (registered && constructed) {
                throw new IllegalStateException
                    ("Cannot change a registered profile");
            }
            this.serialPort = serialPort;
            // Check to see if this action completed the construction
            if (!constructed) checkConstructed();
        }
        /**
         * Get the serial port representation.
         */
        public SerialPortRepresentation getSerialPort() {
            return serialPort;
        }
    }
    /**
     * This BluetoothProfileRepresentation is an HDP (Health Device
     * Profile)
     *
     * The profile holds a X73_20601TransportRepresentation instance
     * which connects to the actual HDP implementation. This value
     * must be != null when this profile is registered with a
     * BluetoothDeviceRepresentation object (added to its list of
     * profiles) which is constructed. I.e. the following invariant
     * applies:
     * 
     *    !registered || !constructed || serialPort != null
     */
    public class HealthDeviceProfileRepresentation
        extends BluetoothProfileRepresentation
        implements BluetoothDevice.HealthDeviceProfile {

        private X73_20601TransportRepresentation x73Transport;
        
        /** Default constructor */
        public HealthDeviceProfileRepresentation() {}

        /** Construct and initialize the HDP representation */
        public HealthDeviceProfileRepresentation
            (X73_20601TransportRepresentation x73Transport) {
            this.x73Transport = x73Transport;
        }
        /**
         * Set the HDP representation.
         *
         * This must be done before the profile is registered with its
         * parent device or before the construction of the parent
         * device itself is complete.
         */
        public void setX73Transport
            (X73_20601TransportRepresentation x73Transport) {
            if (registered && constructed) {
                throw new IllegalStateException
                    ("Cannot change a registered profile");
            }
            this.x73Transport = x73Transport;
            // Check to see if this action completed the construction
            if (!constructed) checkConstructed();
        }
        /**
         * Get the HDP representation.
         */
        public X73_20601TransportRepresentation getX73Transport() {
            return x73Transport;
        }
    }
    /**
     * This BluetoothProfileRepresentation is a GATT (Generic
     * Attribute) profile
     *
     * The profile holds a GATTServerRepresentation instance implemented
     * by the actual GATT implementation. This value must be != null
     * when this profile is registered with a
     * BluetoothDeviceRepresentation object (added to its list of
     * profiles) which is constructed. I.e. the following invariant
     * applies:
     * 
     *    !registered || !constructed || serialPort != null
     */
    public class GenericAttributeProfileRepresentation
        extends BluetoothProfileRepresentation
        implements BluetoothDevice.GenericAttributeProfile {

        private GATTServerRepresentation gattServer;
        private Peer handler;
        private ObjId handle;
        
        /** Default constructor */
        public GenericAttributeProfileRepresentation() {}

        /** Construct and initialize the GATT server representation */
        public GenericAttributeProfileRepresentation(GATTServerRepresentation gattServer, Peer handler, ObjId handle) {
            this.gattServer = gattServer;
            this.handler = handler;
            this.handle = handle;
        }
        /**
         * Set the GATT server representation.
         *
         * This must be done before the profile is registered with its
         * parent device or before the construction of the parent
         * device itself is complete.
         */
        public void setGATTServer(GATTServerRepresentation gattServer, Peer handler, ObjId handle) {
            if (registered && constructed) {
                throw new IllegalStateException
                    ("Cannot change a registered profile");
            }
            this.gattServer = gattServer;
            this.handler = handler;
            this.handle = handle;
            // Check to see if this action completed the construction
            if (!constructed) checkConstructed();
        }
        /**
         * Get the GATT server representation.
         */
        public GATTServerRepresentation getGATTServer() {
            return gattServer;
        }
    }

    /**
     * The profileSet is a regular HashSet wrapped in a thin wrapper
     * to capture additions and deletions to and from the set and
     * forward it to the generalNotify() method.
     */
    private Set<BluetoothProfileRepresentation> profileSet
        = new AbstractSet<BluetoothProfileRepresentation>() {
                private HashSet<BluetoothProfileRepresentation> profiles
                    = new HashSet<BluetoothProfileRepresentation>();
                class ProfileIterator
                           implements Iterator<BluetoothProfileRepresentation> {
                    private Iterator<BluetoothProfileRepresentation> it;
                    private BluetoothProfileRepresentation last;
                    ProfileIterator() {
                        it = profiles.iterator();
                    }
                    public boolean hasNext() {
                        return it.hasNext();
                    }
                    public BluetoothProfileRepresentation next() {
                        return last = it.next();
                    }
                    public void remove() {
                        if (last != null) {
                            last.registered = false;
                            generalNotify();
                        }
                        it.remove();
                    }
                }
                
                @Override
                public int size() { return profiles.size(); }
                @Override
                public Iterator<BluetoothProfileRepresentation> iterator() {
                    return new ProfileIterator(); 
                }
                @Override
                public boolean add(BluetoothProfileRepresentation profile) {

                    if (profile instanceof SerialPortProfileRepresentation ||
                        profile instanceof HealthDeviceProfileRepresentation ||
                        profile instanceof GenericAttributeProfileRepresentation) {
                        boolean added = profiles.add(profile);
                        if (added) {
                            profile.registered = true;
                            generalNotify();
                        }
                        return added;
                    }
                    throw new IllegalArgumentException("Unknown profile class");
                }
            };
    

    /**
     * Get a set of BluetoothProfiles supported by this Bluetooth
     * device which may be modified
     *
     * @return An iterator over the profiles
     */
    public Set<BluetoothProfileRepresentation> getProfileSet() {
        return profileSet;
    }

    /**
     * Get the BluetoothProfiles supported by this Bluetooth device
     *
     * @return An iterator over the profiles
     */
    @Override
    public Iterable<BluetoothProfileRepresentation> getProfiles() {
        return getProfileSet();
    }
    
    /**
     * If this BluetoothDeviceRepresentation object is not fully
     * constructed, iterate through all profiles to check if this is
     * still the case. If all profiles turns out to be properly
     * initialized then do setConstructed() to finalize construction.
     */
    private void checkConstructed() {
        // Already constructed - nothing to do here...
        if (constructed) return;

        // If any non-initialized profile (containing a null) is
        // found, return with no further action
        for (BluetoothProfileRepresentation bpr : profileSet) {
            if (bpr instanceof SerialPortProfileRepresentation) {
                if (((SerialPortProfileRepresentation)bpr)
                    .getSerialPort() == null) {
                    return;
                }
            } else if (bpr instanceof HealthDeviceProfileRepresentation) {
                if (((HealthDeviceProfileRepresentation)bpr)
                    .getX73Transport() == null) {
                    return;
                }
            } else if (bpr instanceof GenericAttributeProfileRepresentation) {
                if (((GenericAttributeProfileRepresentation)bpr)
                    .getGATTServer() == null) {
                    return;
                }
            } else {
                // This should not be possible as unknown classes are
                // prohibited in the profileSet.
                throw new RuntimeException("Unknown profile class");
            }
        }

        // No non-initialized profile was found, so we can finish
        // construction.
        setConstructed();
    }


    /**
     * Produces and caches all previously produced
     * BluetoothDeviceRepresentation objects.
     *
     * The object will be constructed if it does not exist
     * already. The factory will keep track of all produced
     * BluetoothDeviceRepresentation objects such that only one will
     * ever exist for each transportPersistentId.
     */
    public static abstract class BluetoothDeviceRepresentationFactory
        extends DeviceTransportRepresentationFactory
                <BluetoothDeviceRepresentation> {
        /**
         * Constructor
         *
         * @param module The PHG core module (used to create ObjIDs)
         * @param tpidPrefix The transportPersistentId prefix for
         *                   devices managed by this factory.
         */
        protected BluetoothDeviceRepresentationFactory(ModuleBase module,
                                                       String tpidPrefix) {
            super(module, tpidPrefix);
        }
        
        @Override
        protected BluetoothDeviceRepresentation
            build(String transportPersistentId,ObjId handler,
                          DeviceTransportRepresentationFactory parent) {
            return new BluetoothDeviceRepresentation(transportPersistentId,
                                                     handler, parent);
        }

        /**
         * Provide a DeviceTransportRepresentation object with the
         * given transportPersistentId. If newly created, fill it out
         * using the information provided in the template argument.
         *
         * The object will be constructed if it does not exist
         * already. lastWasNew() will indicate whether the returned
         * object was created or not.
         *
         * When constructing a new object, Bluetooth profiles from the
         * template will not be initialized. The caller of this method
         * will be responsible for initializing the profiles, and the
         * newly constructed object will not be fully constructed
         * (visible and active on the endpoint) until this is done.
         *
         * @param transportPersistentId The id of the device to be
         *                              fetched or created.
         * @param template A protobuf template of the device.
         * @throws IllegalArgumentException If the template does not
         *                              represent a Bluetooth device
         *                              or if it contains an
         *                              unknown/unsupported
         *                              BluetoothProfile element.
         * @return The fetched or constructed object, or null if the
         *         tpid prefix did not match this factory
         */
        @Override
        public BluetoothDeviceRepresentation getForId
            (String transportPersistentId,
             dk.s4.phg.messages.classes.device.DeviceTransport template) {

            BluetoothDeviceRepresentation retval
                = super.getForId(transportPersistentId, template, false);

            if (lastWasNew()) {
                // Create device based on template

                if (!template.hasBluetoothDevice()) {
                    throw new IllegalArgumentException
                        ("The template argument must contain a BluetoothDevice");
                }
                dk.s4.phg.messages.classes.device.BluetoothDevice bd
                    = template.getBluetoothDevice();
                retval.bd_addr = bd.getBdAddr();
                retval.name = bd.getName();
                retval.classic = bd.getClassic();
                retval.le = bd.getLe();
                retval.bonded = bd.getBonded();

                if (bd.getBluetoothProfilesList().isEmpty()) {
                    // If there are no profiles, we are done
                    // constructing the device. Otherwise postpone the
                    // 'setConstructed' until all profiles are
                    // initialized.
                    retval.setConstructed();
                    
                } else for (dk.s4.phg.messages.classes.device.BluetoothDevice
                     .BluetoothProfile bp : bd.getBluetoothProfilesList()) {
                    // Iterate through the template profiles and add
                    // an un-initialized profile to the object under
                    // construction.
                    BluetoothProfileRepresentation bpr;
                    if (bp.getProfile() == SERIAL_PORT_PROFILE) {
                        bpr = retval.new SerialPortProfileRepresentation();
                    } else if (bp.getProfile() == HEALTH_DEVICE_PROFILE) {
                        bpr = retval.new HealthDeviceProfileRepresentation();
                    } else if (bp.getProfile() == GENERIC_ATTRIBUTE_PROFILE) {
                        bpr = retval.new GenericAttributeProfileRepresentation();
                    } else {
                        throw new IllegalArgumentException
                            ("Unknown BluetoothProfile value found in the " +
                             "template argument");
                    }
                    bpr.setServiceName(bp.getServiceName());
                    bpr.setServiceDescription(bp.getServiceDescription());
                    bpr.setProviderName(bp.getProviderName());
                    retval.profileSet.add(bpr);
                }
            }
            return retval;
        }
    }

    @Override
    protected dk.s4.phg.messages.classes.device.DeviceTransport.Builder
        buildDeviceTransport() {
        dk.s4.phg.messages.classes.device.BluetoothDevice.Builder btDevice =
            dk.s4.phg.messages.classes.device.BluetoothDevice.newBuilder()
            .setBdAddr(bd_addr)
            .setName(name)
            .setClassic(classic)
            .setLe(le)
            .setBonded(bonded);
        for (BluetoothProfileRepresentation bpr : profileSet) {
            Peer handler;
            ObjId handle;
            dk.s4.phg.messages.classes.device.BluetoothDevice
                .BluetoothProfile.Profile p;
            if (bpr instanceof SerialPortProfileRepresentation) {
                SerialPortRepresentation serialPort
                    = ((SerialPortProfileRepresentation)bpr).serialPort;
                p = SERIAL_PORT_PROFILE;
                handler = serialPort.getHandler();
                handle = serialPort.getHandle();
            } else if (bpr instanceof HealthDeviceProfileRepresentation) {
                p = HEALTH_DEVICE_PROFILE;
                handler = null; // FIXME: not yet supported
                handle = null;
            } else if (bpr instanceof GenericAttributeProfileRepresentation) {
                GenericAttributeProfileRepresentation gatt = (GenericAttributeProfileRepresentation)bpr;
                p = GENERIC_ATTRIBUTE_PROFILE;
                handler = gatt.handler;
                handle = gatt.handle;
            } else {
                throw new RuntimeException("Unknown Bluetooth profile found");
            }
            btDevice.addBluetoothProfiles
                (dk.s4.phg.messages.classes.device.BluetoothDevice
                 .BluetoothProfile
                 .newBuilder()
                 .setHandler(handler.toInt32())
                 .setHandle(handle.toInt64())
                 .setServiceName(bpr.getServiceName())
                 .setServiceDescription(bpr.getServiceDescription())
                 .setProviderName(bpr.getProviderName())
                 .setProfile(p));
        }
        return super.buildDeviceTransport().setBluetoothDevice(btDevice);
    }
}
