package dk.s4.phg.messages.support.device_control;

import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.baseplate.EventImplementation;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.MetaData;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.baseplate.ObjId;
import dk.s4.phg.baseplate.Peer;
import dk.s4.phg.messages.classes.device.DeviceTransport;
import dk.s4.phg.messages.interfaces.device_control.Connected;
import dk.s4.phg.messages.interfaces.device_control.CurrentlyConnected;
import dk.s4.phg.messages.interfaces.device_control.Disconnected;
import dk.s4.phg.messages.interfaces.device_control.DeviceTransportOrError;
import dk.s4.phg.messages.interfaces.device_control.ListConnected;
import dk.s4.phg.messages.interfaces.device_control.Search;
import dk.s4.phg.messages.interfaces.device_control.SearchInitiated;
import dk.s4.phg.messages.interfaces.device_control.SearchResult;
import dk.s4.phg.messages.interfaces.device_control.SolicitDeviceTransport;
import dk.s4.phg.messages.interfaces.device_control.StopSearch;
import dk.s4.phg.messages.support.device.DeviceTransportRepresentation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;




/**
 * The producer endpoint implementation of the DeviceControl
 * interface.
 *
 * This class implements the DeviceControl producer interface. The
 * implementation is directly linked to a
 * DeviceTransportRepresentation.DeviceTransportRepresentationFactory,
 * managing the devices that are served on this interface.
 *
 * The business functionality implementing this producer endpoint is
 * provided by an implementor of the DeviceControlProducer interface.
 * 
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public class DeviceControlProducerEndpoint
             <T extends DeviceTransportRepresentation> {

    // Due to potential races, searches are retained a while after
    // they are stopped. The queue limit counts the number of new
    // searches that will be started before a stopped search will be
    // destroyed.
    private static int DESTRUCTOR_COUNT = 10;

    // The endpoint of this producer
    private InterfaceEndpoint endpoint;

    // The business functionality implementing this producer endpoint
    private DeviceControlProducer<T> producer;

    // Device solicitations that have not yet received a response
    private Set<DeviceTransportRepresentation> solicitedDevices
        = new HashSet<>();

    // Currently connected devices
    private Set<DeviceTransportRepresentation> connectedDevices
        = new HashSet<>();

    /**
     * Create a DeviceControl producer endpoint.
     * 
     * @param producer       The implementation of the business
     *                       functionality of this producer endpoint
     * @param epFactory      The creator of endpoints of this module
     * @param deviceFactory  The manager/creator of devices.
     */
    public DeviceControlProducerEndpoint
        (DeviceControlProducer<T> producer,
         ModuleBase.EndpointFactory epFactory,
         DeviceTransportRepresentation.DeviceTransportRepresentationFactory<T>
         deviceFactory) {

        this.producer = producer;

        // Create the interface endpoint and attach handlers for all
        // events
        
        endpoint = epFactory.implementsProducerInterface("DeviceControl");

        // Start searching
        endpoint.addEventHandler("Search",
            new EventImplementation<Search>() {
                public void accept(Search args, MetaData meta) {

                    ObjId searchId = ObjId.fromInt64(args.getSearchId());
                    if (searchId == null) {
                        // Invalid or missing searchId. Bail out
                        return;
                    }
                    
                    search(searchId, meta.sender());
                }
            });

        // Stop an active search
        endpoint.addEventHandler("StopSearch",
            new EventImplementation<StopSearch>() {
                public void accept(StopSearch args, MetaData meta) {
                    
                    ObjId searchId = ObjId.fromInt64(args.getSearchId());
                    if (searchId == null) {
                        // Invalid or missing searchId. Bail out
                        return;
                    }
                    
                    stopSearch(searchId);
                }
            });

        // List currently connected devices
        endpoint.addEventHandler("ListConnected",
            new EventImplementation<ListConnected>() {
                public void accept(ListConnected args, MetaData meta) {

                    // Compile list of all connected devices
                    CurrentlyConnected.Builder response
                        = CurrentlyConnected.newBuilder();
                    for (DeviceTransportRepresentation device :
                             connectedDevices) {
                        response.addConnectedList(device
                                                  .buildTransportBundle());
                    }

                    // And send it unicast to the requesting peer
                    endpoint.postEvent("CurrentlyConnected", response.build(),
                                       meta.sender());
                }
            });

        // Ensure that a device is fully updated and then anounce its
        // current state - regardless of whether it changed or not.
        endpoint.addEventHandler("SolicitDeviceTransport",
            new EventImplementation<SolicitDeviceTransport>() {
                public void accept(SolicitDeviceTransport args,
                                   MetaData meta) {
                    
                    T device;

                    // Does 'handle' hold a value?
                    ObjId handle = ObjId.fromInt64(args.getHandle());
                    if (handle != null) {
                        // Yes. Lookup the device.
                        device = deviceFactory.lookup(handle);
                    } else {
                        // No. Try transportPersistentId instead.
                        String tpid = args.getTransportPersistentId();
                        if (tpid.isEmpty()) {
                            // Also not defined. Bail out
                            return;
                        }
                        if (args.hasDeviceTransport()) {
                            DeviceTransport template = args.getDeviceTransport();
                            try {
                                device = deviceFactory.getForId(tpid, template);
                            } catch (IllegalArgumentException e) {
                                // The tpid was addressed to me, but I
                                // could not parse the template.
                                // Send "Illegal template" error
                                CoreError ce = endpoint.createError
                                    (3, "Illegal template");
                                endpoint.postEvent("DeviceTransportOrError",
                                                   DeviceTransportOrError
                                                   .newBuilder()
                                                   .setError(ce.buildError())
                                                   .build(), meta.sender());
                                return;
                            }
                        } else {
                            device = deviceFactory.getForId(tpid);
                        }
                        
                    }
                    
                    // Bail out if we did not find a device.
                    if (device == null) {
                        if (meta.unicasted()) {
                            // This was a direct question to me, so I
                            // must send an error response. (For
                            // multicasted messages this solicitation
                            // could be met by another producer).
                           

                            // Send "Unknown device" error
                            CoreError ce = endpoint.createError
                                (2, "Unknown device");
                            endpoint.postEvent("DeviceTransportOrError",
                                               DeviceTransportOrError.newBuilder()
                                               .setError(ce.buildError())
                                               .build(), meta.sender());

                        }
                        return;
                    }
                    
                    // Add this device to set of solicitations that
                    // have not yet received a response. We do not
                    // care if it is already in the set as all
                    // responses are multicasted anyway, so a single
                    // response can satisfy multiple solicitations.
                    solicitedDevices.add(device);
                    
                    // Solicit the device update from the producer
                    producer.solicitDeviceTransport(device,
                                                    (CoreError e) -> {
                            // Check if a device update was already
                            // sent.
                            if (solicitedDevices.remove(device)) {
                                // Nothing has been sent yet. Send the response
                                if (e == null) {
                                    // Device ready
                                    generalNotify(device);
                                } else {
                                    // Device not available
                                    CoreError ce = endpoint.createError
                                        (4, "Device not available");
                                    ce.causedBy = e;
                                    endpoint.postEvent("DeviceTransportOrError",
                                             DeviceTransportOrError.newBuilder()
                                             .setError(ce.buildError())
                                             .build(), meta.sender());
                                }
                            }
                        });
                }
            });
    }


    // FIXME: Error handling for device communication has really not
    // been considered thoroughly yet!
    // /**
    //  * Create an error for this interface
    //  */
    // public CoreError createError(int errorCode, String message){
    //     return endpoint.createError(errorCode, message);
    // }

    /**
     * Multicast the properties of a device
     *
     * @param device The device for which properties are announced.
     */
    public void generalNotify(DeviceTransportRepresentation device) {
        // Remove the device from the set of unhandled solicitations,
        // if it is listed there
        solicitedDevices.remove(device);
        endpoint.postEvent("DeviceTransportOrError",
                           DeviceTransportOrError.newBuilder()
                           .setSubject(device.buildTransportBundle())
                           .build());
    }
    
    /**
     * Multicast the connectivity state of a device if it has changed
     *
     * @param device The device for which connectivity state is
     *               announced.
     */
    public void connectivityNotify(DeviceTransportRepresentation device,
                                   boolean connected) {
        if (connected) {
            // Only announce if it was not connected
            if (connectedDevices.add(device)) {
                endpoint.postEvent("Connected",
                                   Connected.newBuilder()
                                   .setSubject(device.buildTransportBundle())
                                   .build());
            }
        } else {
            // Only announce if it was connected
            if (connectedDevices.remove(device)) {
                endpoint.postEvent("Disconnected",
                                   Disconnected.newBuilder()
                                   .setSubject(device.buildTransportBundle())
                                   .build());
            }
        }            
    }




    //
    // Searching
    //
    
    private class Collector
        implements DeviceControlProducer.SearchResultCollector<T> {

        private Set<DeviceTransportRepresentation> devicesFound
            = new HashSet<>();
        private boolean active = true;
        private int destructionPoint = 0;
        private final ObjId searchId;

        private Collector(ObjId searchId) {
            this.searchId = searchId;
        }
        
        /**
         * Test if this search request is still active.
         * @return This search has been started using search() and has
         *         not yet been stopped using stopSearch()
         */
        @Override
        public boolean isActive() {
            return active;
        }
        
        /**
         * Add a device to this (active) search request.
         *
         * If this search request is no longer active, this is a no-op.
         *
         * @param device The device found during the search.
         */
        @Override
        public void addResult(DeviceTransportRepresentation device) {
            if (!active) return;
            if (devicesFound.add(device)) {
                endpoint.postEvent("SearchResult",
                                   SearchResult.newBuilder()
                                   .setSearchId(searchId.toInt64())
                                   .setResult(device.buildTransportBundle())
                                   .build());
                
            }
        }

        private void searchInitiated(Peer receiver) {
            // Ignoring "active" state as the search may have already
            // been stopped, but we still need to report the result

            // Compile list of all devices found so far
            SearchInitiated.Builder response
                = SearchInitiated.newBuilder().setSearchId(searchId.toInt64());
            for (DeviceTransportRepresentation device : devicesFound) {
                response.addFoundSoFar(device.buildTransportBundle());
            }
            
            // And send it unicast to the requesting peer
            endpoint.postEvent("SearchInitiated", response.build(), receiver);
        }
    }


    private Map<ObjId, Collector> searches = new HashMap<>();
    
    private LinkedList<Collector> destructorQueue = new LinkedList<>();

    private int destructorCounter = 0;

    private void search(ObjId searchId, Peer sender) {
        
        // Destroy any searches that have expired.
        destroyExpiredSearches();
        
        Collector collector = searches.get(searchId);
        if (collector == null) {
            // First mention of this searchId. Create Collector
            collector = new Collector(searchId);
            // and add it to the map of known searches
            searches.put(searchId, collector);
            // Return the SearchInitiated message to the sender
            collector.searchInitiated(sender);
            // Start the search
            producer.search(collector);
        } else {
            // Return the SearchInitiated message to the sender. Due
            // to potential races we do not care if the search is
            // still active or not
            collector.searchInitiated(sender);
        }
    }

    private void stopSearch(ObjId searchId) {
        
        Collector collector = searches.get(searchId);
        
        // Nonexistent or already destroyed - ignore
        if (collector == null) return;

        // Already stopped but not yet destroyed - ignore
        if (!collector.active) return;

        // Stop searching
        producer.stopSearch(collector);
        collector.active = false;

        // Mark the search record for future destruction
        collector.destructionPoint = destructorCounter + DESTRUCTOR_COUNT;
        destructorQueue.add(collector);
    }

    private void destroyExpiredSearches() {
        
        destructorCounter ++;
        
        // Remove all collectors marked for destruction at this
        // counter value
        while (true) {
            Collector collector = destructorQueue.peek();
            if (collector == null ||
                collector.destructionPoint != destructorCounter) {
                break;
            }
            // Remove from map of known searches as well as the
            // destruction queue
            searches.remove(destructorQueue.remove().searchId);
        }
    }
}
