package dk.s4.phg.messages.support.gatt;

import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

import dk.s4.phg.baseplate.CoreError;

/**
 * Bluetooth Generic Attribute access
 *
 */
public interface GATTServerRepresentation {

    void connect(Consumer<CoreError> callback);

    void disconnect();

    /**
     * @param serviceType The service UUID to search for ("Discover
     *                    Primary Service by Service UUID"). May be null,
     *                    which will be interpreted as "Discover All
     *                    Primary Services".
     */
    void discoverServices(UUID serviceType, Consumer<CoreError> callback);
    
    Set<? extends GATTServiceRepresentation> getServices();

}
