package dk.s4.phg.messages.support.serial_port;

import dk.s4.phg.baseplate.Callback;
import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.ObjId;
import dk.s4.phg.baseplate.Logger;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.baseplate.Peer;
import dk.s4.phg.messages.interfaces.serial_port.Closed;
import dk.s4.phg.messages.interfaces.serial_port.OpenSuccess;
import dk.s4.phg.messages.interfaces.serial_port.WriteSuccess;

import com.google.protobuf.ByteString;

/**
 * Abstract serial port implementation.
 *
 * This abstract class must be extended by a class implementing the
 * associated business functionality of a serial port. Instances of
 * this class will be made available for consumers of the
 * SerialPortProducer interface (exposed by the module from the
 * SerialPortProducerEndpoint) according to the protocol buffer file
 * interfaces/SerialPort.proto in phg-messages.
 *
 * Threading Note:
 * ---------------
 * As with all other PHG modules, the methods of this interface are
 * intended for the module thread only; EXCEPT the following methods:
 *  - openSuccessAsync()
 *  - openFailedAsync()
 *  - writeSuccessAsync()
 *  - writeFailedAsync()
 *  - readAsync()
 *  - closedAsync()
 * 
 * All of the above-mentioned methods are NOT re-entrant, but are safe
 * to call on another thread, as long as this is always the same
 * thread (or calls are guarded by full memory barriers).
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public abstract class SerialPortRepresentation {

    // The module producing this serial port
    private final ModuleBase module;

    // The object handle of this serial port
    private final ObjId handle;

    // The serial port producer endpoint exposing this serial port
    private final SerialPortProducerEndpoint sppEndpoint;

    // The interface endpoint this serial port is served on.
    private final InterfaceEndpoint interfaceEP;

    // The consumer currently using (locking) this serial port. When
    // this identity != null, access to the serial port is restricted
    // to that consumer identity only. This identity == null only in
    // the CLOSED state.
    private Peer consumer = null;

    // The callback for the open request stored during the OPENING,
    // OPEN, and WRITING states. null in all other states.
    private Callback<OpenSuccess> openCallback = null;

    // The callback for the write request stored during the WRITING
    // state. null in all other states.
    private Callback<WriteSuccess> writeCallback = null;

    /**
     * The states of the overall state machine managing the serial
     * port.
     */
    public enum State {
        CLOSED,
        OPENING,
        OPEN,
        WRITING,
        CLOSING,
        FAILED_PENDING,
        CLOSED_PENDING
    }

    // The state of the serial port.
    private State state = State.CLOSED;

    // Serial port interface error codes:
    private static final int ERR_ALREADY_OPEN = 2;
    private static final int ERR_NOT_OPEN = 3;
    private static final int ERR_BUSY = 4;
    private static final int ERR_IO = 5;
    

    
    
    /* ************************************************************ */
    /*                                                              */
    /*                         Constructor                          */
    /*                                                              */
    /* ************************************************************ */


    /**
     * A new port is created and bound to the SerialPort producer
     * endpoint.
     *
     * @param endpoint The SerialPort producer endpoint exposing this
     *                 serial port
     */
    protected SerialPortRepresentation(SerialPortProducerEndpoint sppEndpoint) {
        module = sppEndpoint.interfaceEP.getModule();
        handle = module.createObjId();
        this.sppEndpoint = sppEndpoint;
        interfaceEP = sppEndpoint.interfaceEP;
        
        // Introduce myself to the endpoint
        sppEndpoint.ports.put(handle, this);
    }


    

    /* ************************************************************ */
    /*                                                              */
    /*                Public identity of this port                  */
    /*                                                              */
    /* ************************************************************ */

    
    /**
     * Get the handler identity of this serial port object.
     * @return The handler
     */
    public Peer getHandler() {return module.getId(); }

    /**
     * Get the object handle of this serial port.
     * @return The serial port object handle
     */
    public ObjId getHandle() {return handle; }


    

    /* ************************************************************ */
    /*                                                              */
    /*  Communication between this abstract class and its subclass  */
    /*                                                              */
    /* ************************************************************ */

    
    /**
     * The state of the serial port.
     *
     * Subclasses may access the current state of this serial port.
     * Note that the state lives and changes on the module thread. Be
     * careful about inspecting the state on any other threads!
     * @return The current serial port state
     */
    protected State getState() { return state; }
    
    /**
     * Get the module logger.
     *
     * Subclasses should use the module logger to log relevant
     * information as well as build CoreError messages.
     * @return The module logger
     */
    protected Logger getLogger() { return module.getLogger(); }



    //
    //       Open
    //

    
    /**
     * Open the serial port
     *
     * This abstract method must be implemented by the derived class
     * in order to open the serial port. The method must not throw any
     * exceptions. The open operation must report its success or
     * failure by invoking either openSuccess{Async}() OR
     * openFailure{Async}() exactly once.
     *
     * If the open() call can respond immediately, it may use the
     * synchronous versions of the callback methods, openSuccess() and
     * openFailure() immediately. Otherwise, the response is typically
     * coming from an external thread (asynchronous source) and the
     * openSuccessAsync() and openFailureAsync() are used instead.
     */
    protected abstract void open();
    
    /**
     * The open() operation succeeded and the port is open.
     * 
     * This method can be invoked from the body of the open() method.
     */
    protected void openSuccess() { portOpenSuccess(); }

    /**
     * The open() operation succeeded and the port is open.
     * 
     * This method can be invoked from an external thread serving
     * asynchronous events in response to open(). If close() or
     * closed() has been invoked in the meantime, this call is
     * ignored.
     */
    protected void openSuccessAsync() {
        sppEndpoint.enqueueActionConsumer.accept(this::portOpenSuccess);
    }

    /**
     * Error codes reported by the openFailed() and openFailedAsync()
     * methods.
     */
    public enum OpenError {
        /**
         * The port was already open (likely by some other process, as
         * the state machine will ensure that this module will never
         * be asked to open an already open port).
         *
         * If this error code is used, the implementation MUST report
         * when the port is becoming available by calling the closed()
         * method when this happens. If this can not be guaranteed,
         * the IO_ERROR code SHALL be used instead.
         */
        ALREADY_OPEN,

        /**
         * The port was closed by either end during the open
         * operation. This response must be coupled with a closed
         * signal (may be provided in either order).
         */
        NOT_OPEN,
        
        /**
         * Catch-all for I/O errors during open.
         *
         * The error message should provide more details.
         */
        IO_ERROR;
    }
    
    /**
     * The open() operation failed.
     *
     * This method can be invoked from the body of the open() method.
     *
     * @param code     One of the OpenError error codes categorising
     *                 the type of the error
     * @param details  An optional (longer) description of the error
     * @param causedBy An optional (but recommended) CoreError object
     *                 containing more details. By creating a CoreError
     *                 using the getLogger().createError() method, the
     *                 location in the source code will also be
     *                 captured, potentially easing the debugging.
     */
    protected void openFailed(OpenError code, String details,
                              CoreError causedBy) {
        portOpenFailed(code, details, causedBy);
    }

    /**
     * The open() operation failed.
     *
     * This method can be invoked from an external thread serving
     * asynchronous events in response to open(). If close() or
     * closed() has been invoked in the meantime, this call is
     * ignored.
     *
     * @param code     One of the OpenError error codes categorising
     *                 the type of the error
     * @param details  An optional (longer) description of the error
     * @param causedBy An optional (but recommended) CoreError object
     *                 containing more details. By creating a CoreError
     *                 using the getLogger().createError() method, the
     *                 location in the source code will also be
     *                 captured, potentially easing the debugging.
     */
    protected void openFailedAsync(OpenError code, String details, CoreError causedBy) {
        sppEndpoint.enqueueActionConsumer.accept(() -> {
                portOpenFailed(code, details, causedBy);
            });
    }


    

    //
    //       Close
    //


    /**
     * Close the serial port from the consumer end.
     *
     * This abstract method must be implemented by the derived class
     * in order to close the serial port. The method must not throw any
     * exceptions.
     *
     * This is a no-op if the port is not open.
     *
     * Closing a port with a pending write buffer (i.e. a previous
     * write operation that did not have the flush flag set) may
     * abandon the written data such that it is only partially
     * transmitted. In order to ensure that all data are properly
     * transmitted before closing the port, the final write() call
     * must have the flush flag set and wait for the writeSuccess()
     * before calling close().
     *
     * It is allowed to use the close() method to force cancel a
     * pending open() or write() function (even with the flush flag
     * set). This will result in the return of error code 3 (Port not
     * open)
     * 
     * When the close operation is completed (meaning that the port
     * has become available and can be opened again), one of the
     * closed() or closedAsync() methods must be called.
     */
    protected abstract void close();

    /**
     * The serial port was closed at the producer end.
     *
     * This method can be invoked from the body of the open() or
     * write() methods.
     */
    protected void closed() { portClosed(); }

    /**
     * The serial port was closed at the producer end.
     *
     * This method can be invoked from an external thread serving
     * asynchronous events.
     */
    protected void closedAsync() {
        sppEndpoint.enqueueActionConsumer.accept(this::portClosed);
    }




    //
    //       Write
    //

    
    /**
     * Write data to the serial port.
     *
     * This abstract method must be implemented by the derived class
     * in order to write to the serial port. The method must not throw
     * any exceptions. The write operation must report its success or
     * failure by invoking either writeSuccess{Async}() OR
     * writeFailure{Async}() exactly once.
     *
     * If the write() call can respond immediately, it may use the
     * synchronous versions of the callback methods, writeSuccess() and
     * writeFailure() immediately. Otherwise, the response is typically
     * coming from an external thread (asynchronous source) and the
     * writeSuccessAsync() and writeFailureAsync() are used instead.
     *
     * At any time, only one write() can be active - i.e. the consumer
     * must wait for a writeSuccess() or writeFailed() before sending
     * a new write() request. The base class will make sure this rule
     * is enforced, so any derived classes may assume that this is
     * always the case.
     *
     * @param buffer The bytes to send
     * @param flush Wait until the data is physically sent (for
     *              hardware serial ports) or at least pushed to the
     *              operating system buffers (for "logical" - e.g. USB
     *              or Bluetooth - based ports) before signalling
     *              writeSuccessAsync().
     */
    protected abstract void write(byte[] buffer, boolean flush);

    /**
     * The write() operation succeeded.
     *
     * The data was sent (if flush==true), or at least added to a
     * queue (if flush==false)
     *
     * This method can be invoked from the body of the write() method.
     */
    protected void writeSuccess() { portWriteSuccess(); }

    /**
     * The write() operation succeeded.
     *
     * The data was sent (if flush==true), or at least added to a
     * queue (if flush==false)
     *
     * This method can be invoked from an external thread serving
     * asynchronous events in response to write(). If close() or
     * closed() has been invoked in the meantime, this call is
     * ignored.
     */
    protected void writeSuccessAsync() {
        sppEndpoint.enqueueActionConsumer.accept(this::portWriteSuccess);
    }

    /**
     * Error codes reported by the writeFailed() and
     * writeFailedAsync() methods.
     */
    public enum WriteError {
        /**
         * The port was not open.
         * 
         * The port was not open at the time this write operation was
         * executed, probably because it has been closed in the
         * meantime by either end.
         */
        NOT_OPEN,

        /**
         * Catch-all for I/O errors during write.
         *
         * The error message should provide more details.
         */
        IO_ERROR
    }
    
    
    /**
     * The write() operation failed.
     *
     * This method can be invoked from the body of the write() method.
     *
     * @param code     One of the WriteError error codes categorising
     *                 the type of the error
     * @param details  An optional (longer) description of the error
     * @param causedBy An optional (but recommended) CoreError object
     *                 containing more details. By creating a CoreError
     *                 using the getLogger().createError() method, the
     *                 location in the source code will also be
     *                 captured, potentially easing the debugging.
     */
    protected void writeFailed(WriteError code, String details,
                               CoreError causedBy) {
        portWriteFailed(code, details, causedBy);
    }

    /**
     * The write() operation failed.
     *
     * This method can be invoked from an external thread serving
     * asynchronous events in response to write(). If close() or
     * closed() has been invoked in the meantime, this call is
     * ignored.
     *
     * @param code     One of the WriteError error codes categorising
     *                 the type of the error
     * @param details  An optional (longer) description of the error
     * @param causedBy An optional (but recommended) CoreError object
     *                 containing more details. By creating a CoreError
     *                 using the getLogger().createError() method, the
     *                 location in the source code will also be
     *                 captured, potentially easing the debugging.
     */
    protected void writeFailedAsync(WriteError code, String details, CoreError causedBy) {
        sppEndpoint.enqueueActionConsumer.accept(() -> portWriteFailed(code, details, causedBy));
    }



    
    //
    //       Read
    //

    
    /**
     * Data input from the serial port.
     *
     * This method can be invoked from the body of the open() or
     * write() method.
     *
     * @param buffer The bytes read from the port.
     */
    protected void read(byte[] buffer) { portRead(buffer); }

    /**
     * Data input from the serial port.
     *
     * This method can be invoked from an external thread serving
     * asynchronous events. If close() or closed() has been invoked in
     * the meantime, this call is ignored.
     *
     * @param buffer The bytes read from the port.
     */
    protected void readAsync(byte[] buffer) {
        sppEndpoint.enqueueActionConsumer.accept(() -> portRead(buffer));
    }




    //
    //       General task (used for logging)
    //


    /**
     * Execute a task on the module thread.
     *
     * @param task The task to execute
     */
    protected void runAsync(Runnable task) {
        sppEndpoint.enqueueActionConsumer.accept(task);
    }




    /* ************************************************************ */
    /*                                                              */
    /*         Internal state machine based implementation          */
    /*                                                              */
    /* ************************************************************ */


    // Open request received on the endpoint
    void endpointOpen(Callback<OpenSuccess> callback, Peer consumer) {
        switch (state) {
        case CLOSED:
            // Update port state
            state = State.OPENING;
            
            // Save the callback and sender for later
            openCallback = callback;
            this.consumer = consumer;
            
            // Open port
            open();
            
            break;

        default:
            // Already open
            callback.error(createError(ERR_ALREADY_OPEN), false);
        }            
    }

    // Write request received on the endpoint
    void endpointWrite(Callback<WriteSuccess> callback, Peer consumer,
                       byte[] message, boolean flush) {
                    
        // Check that the consumer actually has access to the port
        if (!consumer.equals(this.consumer)) {
            // Reject access
            callback.error(createError(ERR_NOT_OPEN), false);
            return;
        }
                    
        switch (state) {
        case OPEN:
            // Update the state and save the callback
            state = State.WRITING;
            writeCallback = callback;

            if (message.length == 0) {
                // Nothing to send. Acknowledge immediately
                portWriteSuccess();
            } else {
                // Send the payload
                write(message, flush);
            }
            break;

        case OPENING:
        case WRITING:
            // Bad state
            callback.error(createError(ERR_BUSY), false);
            break;

        default: // CLOSING, FAILED_PENDING, CLOSED_PENDING
            // Reject access
            callback.error(createError(ERR_NOT_OPEN), false);
        }
    }

    // Close event received on the endpoint
    void endpointClose(Peer consumer) {

        // Check that the consumer actually has access to the port
        if (!consumer.equals(this.consumer)) {
            // Ignore request
            return;
        }
                    
        switch (state) {
        case OPENING:
            state = State.CLOSING;
            // Send openFailed(3) as opening was aborted
            openCallback.error(createError(ERR_NOT_OPEN), false);
            openCallback = null;
            
            break;

        case OPEN:
            state = State.CLOSED_PENDING;
            // send EOS
            endOfStream();
            
            break;

        case WRITING:
            state = State.CLOSING;
            // send EOS
            endOfStream();
            
            // Send writeFailed(3) as writing was aborted
            writeCallback.error(createError(ERR_NOT_OPEN), false);
            writeCallback = null;
            
            break;

        default: // CLOSING, FAILED_PENDING, CLOSED_PENDING
            // No-op we do NOT call close
            return;
        }
        
        // close the port
        close();
    }

    // Open success signalled from the port
    private void portOpenSuccess() {
        switch (state) {
        case OPENING:
            // Port was opened:
            state = State.OPEN;
            
            // Send an empty Open callback response, switching to
            // "streaming mode" with the more flag set to true
            openCallback.success(OpenSuccess.getDefaultInstance(), true);
            break;

        case CLOSING:
            // In the closing state we may still experience a
            // successful open, as long as it is followed by a closed
            state = State.CLOSED_PENDING;
            break;

        default: // OPEN, WRITING, CLOSED_/FAILED_PENDING, CLOSED
            // In all other states, openSuccess would be illegal
            stateError("openSuccess");
        }
    }

    // Open failed signalled from the port
    private void portOpenFailed(OpenError code, String details,
                                CoreError causedBy) {
        switch (state) {
        case OPENING:
            // Port failed to open:
            switch (code) {
            case IO_ERROR:
                // This was due to an I/O error and we do not know
                // whether a closed() event will follow, so we close
                // the port immediately
                state = State.CLOSED;
                consumer = null;

                // Send the error response
                openCallback.error(enrichError(createError(ERR_IO),
                                               details, causedBy), false);
                openCallback = null;
                
                // And notify that the port was closed
                endpointClosed();
                break;
                
            default: // ALREADY_OPEN, NOT_OPEN
                // When these error codes are reported, the serial
                // port implementation is required to follow-up by
                // signalling closed() when the port becomes
                // available.
                state = State.CLOSED_PENDING;

                // Send the error response
                CoreError err = createError(code == OpenError.ALREADY_OPEN?
                                            ERR_ALREADY_OPEN: ERR_NOT_OPEN);
                openCallback.error(enrichError(err, details, causedBy), false);
                openCallback = null;
            }
            break;

        case CLOSING:
            // In the closing state we may get an error, which we will
            // ignore, as long as it is followed by a closed
            state = State.CLOSED_PENDING;
            break;

        case FAILED_PENDING:
            // In this state we expect to receive an error, which we
            // shall ignore and then proceed to the CLOSED state
            state = State.CLOSED;
            consumer = null;

            // And notify that the port was closed
            endpointClosed();
            break;

        default: // OPEN, WRITING, CLOSED_PENDING, CLOSED
            // In all other states, openSuccess would be illegal
            stateError("openFailed");
        }
    }

        
    // Write success signalled from the port
    private void portWriteSuccess() {
        switch (state) {
        case WRITING:
            // Successful write:
            state = State.OPEN;
            
            // Send the Write callback response
            writeCallback.success(WriteSuccess.getDefaultInstance(), false);
            writeCallback = null;
            break;

        case CLOSING:
            // In the closing state we may still experience a
            // successful write, as long as it is followed by a closed
            state = State.CLOSED_PENDING;
            break;

        case FAILED_PENDING:
            // In this state we actually expect to receive an error,
            // but a success is also acceptable. We shall ignore and
            // then proceed to the CLOSED state
            state = State.CLOSED;
            consumer = null;

            // And notify that the port was closed
            endpointClosed();
            break;

        default: // OPEN, OPENING, FAILED_PENDING, CLOSED
            // In all other states, writeSuccess would be illegal
            stateError("writeSuccess");
        }
    }

    // Write failed signalled from the port
    private void portWriteFailed(WriteError code, String details, CoreError causedBy) {
        switch (state) {
        case WRITING:
            // Failed write:
            state = State.CLOSED_PENDING;

            // send EOS
            endOfStream();
            
            // Send the error response
            writeCallback.error(createError(code == WriteError.NOT_OPEN?
                                            ERR_NOT_OPEN: ERR_IO), false);
            writeCallback = null;

            // In case of an I/O error we do not know if the port
            // shuts down itself, so we explicitly close it to be
            // sure.
            if (code == WriteError.IO_ERROR) {
                close();
            }
            
            break;

        case CLOSING:
            // In the closing state we may get an error, which we will
            // ignore, as long as it is followed by a closed
            state = State.CLOSED_PENDING;
            break;

        case FAILED_PENDING:
            // In this state we expect to receive an error, which we
            // shall ignore and then proceed to the CLOSED state
            state = State.CLOSED;
            consumer = null;

            // And notify that the port was closed
            endpointClosed();
            break;

        default: // OPEN, OPENING, CLOSED_PENDING, CLOSED
            // In all other states, openSuccess would be illegal
            stateError("writeFailed");
        }
    }

    // Closed signalled from the port
    private void portClosed() {
        switch (state) {
        case OPENING:
            // Await openFailed from the port
            state = State.FAILED_PENDING;

            // Send openFailed(3) as opening was aborted
            openCallback.error(createError(ERR_NOT_OPEN), false);
            openCallback = null;
            
            break;

        case OPEN:
            // We can close the port immediately
            state = State.CLOSED;
            consumer = null;
            
            // send EOS
            endOfStream();
            
            // And notify that the port was closed
            endpointClosed();
            
            break;

        case WRITING:
            // We expect a writeFailed
            state = State.FAILED_PENDING;

            // send EOS
            endOfStream();
            
            // Send writeFailed(3) as writing was aborted
            writeCallback.error(createError(ERR_NOT_OPEN), false);
            writeCallback = null;
            
            break;

        case CLOSING:
            // We still expect a -failed signal
            state = State.FAILED_PENDING;
            
            break;

        case CLOSED_PENDING:
            // We close the port
            state = State.CLOSED;
            consumer = null;
            
            // And notify that the port was closed
            endpointClosed();
            
            break;
            
        default: // CLOSED, FAILED_PENDING
            // No-op
        }
    }

    // Incoming data received from the port
    private void portRead(byte[] buffer) {
        switch (state) {
        case OPEN:
        case WRITING:
            // Forward the data
            openCallback.success(OpenSuccess.newBuilder()
                                 .setMessage(ByteString.copyFrom(buffer))
                                 .build(), true);

            break;
        default: // CLOSED, OPENING, CLOSING, FAILED_/CLOSED_PENDING
            // No-op: we ignore incoming data
        }
    }

    // End of the read stream: Send an empty openSuccess which marks
    // the end of the stream (more flag is false) to clear up
    // resources.
    private void endOfStream() {
        openCallback.success(OpenSuccess.getDefaultInstance(), false);
        openCallback = null;
    }

    // Signal the closed event to the serial port consumers
    private void endpointClosed() {
        interfaceEP.postEvent("Closed", Closed.newBuilder().setSerialPortHandle
                              (handle.toInt64()).build());
    }
    
    // Report a state error on the log and kill the port.
    // All states except CLOSING could experience illegal signals
    private void stateError(String transition) {
        String node = "UNKNOWN";
        switch (state) {
        case CLOSED:
            node = "CLOSED";
            break;
            
        case OPENING:
            node = "OPENING";

            state = State.CLOSED_PENDING;
            
            // Send openFailed(3) as opening was aborted
            openCallback.error(createError(ERR_NOT_OPEN), false);
            openCallback = null;

            // close the port explicitly, just to be safe
            close();
            
            break;
            
        case OPEN:
            node = "OPEN";

            state = State.CLOSED_PENDING;

            // send EOS
            endOfStream();
            // close the port explicitly, just to be safe
            close();

            break;
            
        case WRITING:
            node = "WRITING";
            
            state = State.CLOSED_PENDING;

            // send EOS
            endOfStream();
            
            // Send the error response
            String details = "State machine error: Received " + transition
                + " event during writing";
            writeCallback.error(enrichError(createError(ERR_IO),
                                            details, null), false);
            writeCallback = null;

            // close the port explicitly, just to be safe
            close();
            
            break;
            
        case FAILED_PENDING:
            node = "FAILED_PENDING";

            state = State.CLOSED;
            consumer = null;

            // Notify that the port was closed
            endpointClosed();
            
            // close the port explicitly, just to be safe
            close();
            
            break;
            
        case CLOSED_PENDING:
            node = "CLOSED_PENDING";

            // close the port explicitly, just to be safe
            close();
        }

        // Log the error
        getLogger().error("State machine error: Received event "
                          + transition + " while in state " + node 
                          + ". The serial port was closed.");
    }

    // Create a CoreError
    private CoreError createError(int errorCode) {
        String message;
        switch (errorCode) {
        case ERR_ALREADY_OPEN:
            message = "Port already open";
            break;
        case ERR_NOT_OPEN:
            message = "Port not open";
            break;
        case ERR_BUSY:
            message = "Operation in progress";
            break;
        default: // ERR_IO
            message = "I/O error";
        }
        return interfaceEP.createError(errorCode, message);
    }
    
    
    // Add optional details and causedBy elements to a CoreError
    // object
    private CoreError enrichError(CoreError base, String details,
                                  CoreError causedBy) {
        base.details = details;
        base.causedBy = causedBy;
        return base;
    }
}
