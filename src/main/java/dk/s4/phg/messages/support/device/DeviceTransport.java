package dk.s4.phg.messages.support.device;

import dk.s4.phg.baseplate.ObjId;




/**
 * A read-only representation of the DeviceTransport class defined in
 * classes/Device.proto.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public interface DeviceTransport {


    /**
     * Object handle of this DeviceTransport object.
     */
    ObjId getHandle();
    
    /**
     * Get the transport_link_capable property.
     * 
     * This property defines whether transport linking is supported
     * for this particular device (on this particular implementation
     * and platform). For BluetoothDeviceTransports "transport
     * linking" is the same as "bonding", so this propety would
     * indicate whether the bond() method of the
     * BluetoothDeviceControl interface is supported for this object.
     */
    boolean isTransportLinkCapable();

    /**
     * Get the transport_unlink_capable property.
     * 
     * This property defines whether transport unlinking is supported
     * for this particular device (on this particular implementation
     * and platform). For BluetoothDeviceTransports "transport
     * unlinking" is the same as "unbonding", so this propety would
     * indicate whether the unbond() method of the
     * BluetoothDeviceControl interface is supported for this object.
     */
    boolean isTransportUnlinkCapable();
}

