package dk.s4.phg.messages.support.gatt;

import dk.s4.phg.baseplate.Callback;
import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.baseplate.EventImplementation;
import dk.s4.phg.baseplate.FunctionImplementation;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.MetaData;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.baseplate.ObjId;
import dk.s4.phg.baseplate.Peer;

import dk.s4.phg.messages.interfaces.gatt.*;

import com.google.protobuf.ByteString;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

public class GATTProducerEndpoint {

    // The interface endpoint implemented by this GATT producer instance
    private final InterfaceEndpoint endpoint;

    // The GATT server instances represented by this producer - mapping back and forth
    private final HashMap<ObjId, GATTServerRepresentation> representedServerByID = new HashMap<>();
    private final HashMap<GATTServerRepresentation, ObjId> representedServerByObject = new HashMap<>();

    // The GATT service instances found on all servers represented by this producer - mapping back and forth
    private final HashMap<ObjId, GATTServiceRepresentation> representedServiceByID = new HashMap<>();
    private final HashMap<GATTServiceRepresentation, ObjId> representedServiceByObject = new HashMap<>();

    // The GATT characteristic instances found on all servers represented by this producer - mapping back and forth
    private final HashMap<ObjId, GATTCharacteristicRepresentation> representedCharacteristicByID = new HashMap<>();
    private final HashMap<GATTCharacteristicRepresentation, ObjId> representedCharacteristicByObject = new HashMap<>();

    // The GATT descriptors instances found on all servers represented by this producer - mapping back and forth
    private final HashMap<ObjId, GATTDescriptorRepresentation> representedDescriptorByID = new HashMap<>();
    private final HashMap<GATTDescriptorRepresentation, ObjId> representedDescriptorByObject = new HashMap<>();


    
    /**
     * Create a new endpoint for producing GATT servers.
     *
     * Any GATTServerRepresentation created using this object will be
     * served at the interface endpoint represented by this object.
     * @param epFactory The module endpoint factory used to create
     *                  this endpoint.
     */
    public GATTProducerEndpoint(ModuleBase.EndpointFactory epFactory) {

        endpoint = epFactory.implementsProducerInterface("GATT");
        
        endpoint.addFunctionHandler("Connect",
                new FunctionImplementation<Connect, ConnectSuccess>() {
                    public void apply(Connect args,
                                      Callback<ConnectSuccess> callback,
                                      MetaData meta) {

                        GATTServerRepresentation server = getServerFor(args.getGattHandle());
                        if (server == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                        } else {
                            server.connect((CoreError ce) -> {
                                    if (ce == null) {
                                        callback.success(ConnectSuccess.getDefaultInstance(), false);
                                    } else {
                                        callback.error(ce, false);
                                    }
                                });
                        }
                    }
                });

        endpoint.addEventHandler("Disconnect",
                new EventImplementation<Disconnect>() {
                    public void accept(Disconnect args, MetaData meta) {
                        GATTServerRepresentation server = getServerFor(args.getGattHandle());
                        if (server != null) {
                            server.disconnect();
                        }
                    }
                });
        
        endpoint.addFunctionHandler("DiscoverServices",
                new FunctionImplementation<DiscoverServices, DiscoverServicesSuccess>() {
                    public void apply(DiscoverServices args,
                                      Callback<DiscoverServicesSuccess> callback,
                                      MetaData meta) {

                        GATTServerRepresentation server = getServerFor(args.getGattHandle());
                        UUID uuid;
                        try {
                            uuid = bt2uuid(args.getUuid());
                        } catch (IllegalArgumentException e) {
                            CoreError ce = createError(GATTError.INVALID_ARGUMENT);
                            ce.causedBy = endpoint.getModule().getLogger().createError(e);
                            callback.error(ce, false);
                            return;
                        }
                        if (server == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        server.discoverServices(uuid, (CoreError ce) -> {
                                if (ce == null) {
                                    DiscoverServicesSuccess.Builder responseBuilder
                                        = DiscoverServicesSuccess.newBuilder();
                                    
                                    for (GATTServiceRepresentation service : server.getServices()) {
                                        responseBuilder.addServices(buildGattService(service));
                                    }
                                    
                                    callback.success(responseBuilder.build(), false);
                                } else {
                                    callback.error(ce, false);
                                }
                            });
                    }
                });

        endpoint.addFunctionHandler("FindIncludedServices",
                new FunctionImplementation<FindIncludedServices, FindIncludedServicesSuccess>() {
                    public void apply(FindIncludedServices args,
                                      Callback<FindIncludedServicesSuccess> callback,
                                      MetaData meta) {

                        GATTServiceRepresentation service = getServiceFor(args.getService());
                        if (service == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        service.findIncludedServices((CoreError ce) -> {
                                if (ce == null) {
                                    FindIncludedServicesSuccess.Builder responseBuilder
                                        = FindIncludedServicesSuccess.newBuilder();
                                    
                                    for (GATTServiceRepresentation inclService : service.getIncludedServices()) {
                                        responseBuilder.addServices(buildGattService(inclService));
                                    }
                                    
                                    callback.success(responseBuilder.build(), false);
                                } else {
                                    callback.error(ce, false);
                                }
                            });
                    }
                });

        endpoint.addFunctionHandler("DiscoverCharacteristics",
                new FunctionImplementation<DiscoverCharacteristics, DiscoverCharacteristicsSuccess>() {
                    public void apply(DiscoverCharacteristics args,
                                      Callback<DiscoverCharacteristicsSuccess> callback,
                                      MetaData meta) {

                        GATTServiceRepresentation service = getServiceFor(args.getService());
                        UUID uuid;
                        try {
                            uuid = bt2uuid(args.getUuid());
                        } catch (IllegalArgumentException e) {
                            CoreError ce = createError(GATTError.INVALID_ARGUMENT);
                            ce.causedBy = endpoint.getModule().getLogger().createError(e);
                            callback.error(ce, false);
                            return;
                        }
                        if (service == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        service.discoverCharacteristics(uuid, (CoreError ce) -> {
                                if (ce == null) {
                                    DiscoverCharacteristicsSuccess.Builder responseBuilder
                                        = DiscoverCharacteristicsSuccess.newBuilder();
                                    
                                    for (GATTCharacteristicRepresentation characteristic : service.getCharacteristics()) {
                                        responseBuilder.addCharacteristics(buildGattCharacteristic(characteristic));
                                    }
                                    
                                    callback.success(responseBuilder.build(), false);
                                } else {
                                    callback.error(ce, false);
                                }
                            });
                    }
                });

        endpoint.addFunctionHandler("DiscoverDescriptors",
                new FunctionImplementation<DiscoverDescriptors, DiscoverDescriptorsSuccess>() {
                    public void apply(DiscoverDescriptors args,
                                      Callback<DiscoverDescriptorsSuccess> callback,
                                      MetaData meta) {

                        GATTCharacteristicRepresentation characteristic = getCharacteristicFor(args.getCharacteristic());
                        if (characteristic == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        characteristic.discoverDescriptors((CoreError ce) -> {
                                if (ce == null) {
                                    DiscoverDescriptorsSuccess.Builder responseBuilder
                                        = DiscoverDescriptorsSuccess.newBuilder();
                                    
                                    for (GATTDescriptorRepresentation descriptor : characteristic.getDescriptors()) {
                                        responseBuilder.addDescriptors(buildGattDescriptor(descriptor));
                                    }
                                    
                                    callback.success(responseBuilder.build(), false);
                                } else {
                                    callback.error(ce, false);
                                }
                            });
                    }
                });

        endpoint.addFunctionHandler("ReadCharacteristic",
                new FunctionImplementation<ReadCharacteristic,ReadCharacteristicSuccess>() {
                    public void apply(ReadCharacteristic args,
                                      Callback<ReadCharacteristicSuccess> callback,
                                      MetaData meta) {

                        GATTCharacteristicRepresentation characteristic = getCharacteristicFor(args.getCharacteristic());
                        if (characteristic == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        characteristic.readValue((CoreError ce) -> {
                                if (ce == null) {
                                    callback.success(ReadCharacteristicSuccess.newBuilder()
                                                     .setValue(ByteString.copyFrom(characteristic.getValue()))
                                                     .build(), false);
                                } else {
                                    callback.error(ce, false);
                                }
                            });
                    }
                });

        endpoint.addFunctionHandler("WriteCharacteristic",
                new FunctionImplementation<WriteCharacteristic,WriteCharacteristicSuccess>() {
                    public void apply(WriteCharacteristic args,
                                      Callback<WriteCharacteristicSuccess> callback,
                                      MetaData meta) {

                        GATTCharacteristicRepresentation characteristic = getCharacteristicFor(args.getCharacteristic());
                        if (characteristic == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        ByteString value = args.getValue();
                        if (value == null || value.isEmpty()) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        characteristic.writeValueWithResponse(value.toByteArray(), (CoreError ce) -> {
                                if (ce == null) {
                                    callback.success(WriteCharacteristicSuccess.getDefaultInstance(), false);
                                } else {
                                    callback.error(ce, false);
                                }
                            });
                    }
                });

        endpoint.addFunctionHandler("WriteCharacteristicWithoutResponse",
                new FunctionImplementation<WriteCharacteristicWithoutResponse,WriteCharacteristicWithoutResponseSuccess>() {
                    public void apply(WriteCharacteristicWithoutResponse args,
                                      Callback<WriteCharacteristicWithoutResponseSuccess> callback,
                                      MetaData meta) {

                        GATTCharacteristicRepresentation characteristic = getCharacteristicFor(args.getCharacteristic());
                        if (characteristic == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        ByteString value = args.getValue();
                        if (value == null || value.isEmpty()) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        characteristic.writeValueWithoutResponse(value.toByteArray(), (CoreError ce) -> {
                                if (ce == null) {
                                    callback.success(WriteCharacteristicWithoutResponseSuccess.getDefaultInstance(), false);
                                } else {
                                    callback.error(ce, false);
                                }
                            });
                    }
                });
        
        endpoint.addFunctionHandler("ReadDescriptor",
                new FunctionImplementation<ReadDescriptor,ReadDescriptorSuccess>() {
                    public void apply(ReadDescriptor args,
                                      Callback<ReadDescriptorSuccess> callback,
                                      MetaData meta) {

                        GATTDescriptorRepresentation descriptor = getDescriptorFor(args.getDescr());
                        if (descriptor == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        descriptor.readValue((CoreError ce) -> {
                                if (ce == null) {
                                    callback.success(ReadDescriptorSuccess.newBuilder()
                                                     .setValue(buildGattDescriptorValue(descriptor))
                                                     .build(), false);
                                } else {
                                    callback.error(ce, false);
                                }
                            });
                    }
                });

        endpoint.addFunctionHandler("WriteDescriptor",
                new FunctionImplementation<WriteDescriptor,WriteDescriptorSuccess>() {
                    public void apply(WriteDescriptor args,
                                      Callback<WriteDescriptorSuccess> callback,
                                      MetaData meta) {

                        GATTDescriptorRepresentation descriptor = getDescriptorFor(args.getDescr());
                        if (descriptor == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        GattDescriptorValue value = args.getValue();
                        if (value == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        Consumer<CoreError> cb = (CoreError ce) -> {
                            if (ce == null) {
                                callback.success(WriteDescriptorSuccess.getDefaultInstance(), false);
                            } else {
                                callback.error(ce, false);
                            }
                        };
                        try {
                            switch (value.getTypeCase()) {
                            case CHARACTERISTIC_EXTENDED_PROPERTIES:
                                // Not permitted
                                break;
                            case CHARACTERISTIC_USER_DESCRIPTION:
                                ((GATTDescriptorRepresentation.CharacteristicUserDescription)descriptor)
                                    .writeValue(value.getCharacteristicUserDescription().getUserDescription(), cb);
                                return;
                            case CLIENT_CHARACTERISTIC_CONFIGURATION:
                                // Write not permitted in this case - Use enableNI() / disableNI() instead
                                break;
                            case SERVER_CHARACTERISTIC_CONFIGURATION:
                                ((GATTDescriptorRepresentation.ServerCharacteristicConfiguration)descriptor)
                                    .writeValue(value.getServerCharacteristicConfiguration().getBroadcast(), cb);
                                return;
                            case CHARACTERISTIC_PRESENTATION_FORMAT:
                            case CHARACTERISTIC_AGGREGATE_FORMAT:
                                // Not permitted
                                break;
                            case USER_DEFINED_DESCRIPTOR:
                                ByteString rawValue = value.getUserDefinedDescriptor().getValue();
                                if (rawValue == null || rawValue.isEmpty()) {
                                    callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                                    return;
                                }
                                ((GATTDescriptorRepresentation.UserDefinedDescriptor)descriptor)
                                    .writeValue(rawValue.toByteArray(), cb);
                                return;
                            default: // TYPE_NOT_SET
                                callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                                return;
                            }
                            callback.error(createError(GATTError.NOT_PERMITTED), false);
                        } catch (ClassCastException e) {
                            // The value argument has the wrong type
                            CoreError ce = createError(GATTError.INVALID_ARGUMENT);
                            ce.causedBy = endpoint.getModule().getLogger().createError(e);
                            callback.error(ce, false);
                        }
                    }
                });

        endpoint.addFunctionHandler("SetNotification",
                new FunctionImplementation<SetNotification,SetNotificationSuccess>() {
                    public void apply(SetNotification args,
                                      Callback<SetNotificationSuccess> callback,
                                      MetaData meta) {

                        GATTCharacteristicRepresentation characteristic = getCharacteristicFor(args.getCharacteristic());
                        if (characteristic == null) {
                            callback.error(createError(GATTError.INVALID_ARGUMENT), false);
                            return;
                        }
                        boolean enableNotification = args.getEnableNotification();
                        Consumer<CoreError> c2 = (CoreError ce) -> {
                            if (ce == null) {
                                callback.success(SetNotificationSuccess.getDefaultInstance(), false);
                            } else {
                                callback.error(ce, false);
                            }
                        };
                        if (enableNotification) {
                            Peer receiver = meta.sender();
                            characteristic.enableNI(c2, (GATTCharacteristicRepresentation c, byte[] value) ->
                                                    characteristicNotified(receiver, c, value));
                        } else {
                            characteristic.disableNI(c2);
                        }
                    }
                });
    }

    /** Get this endpoint's handler */
    public Peer getHandler() {
        return endpoint.getModule().getId();
    }

    /** Create one of this endpoint's officially defined errors */
    public CoreError createError(GATTError error) {
        return endpoint.createError(error.errorCode, error.errorMessage);
    }
    

    
    /* ********************************************************************** */
    /*                                                                        */
    /*                             Endpoint events                            */
    /*                                                                        */
    /* ********************************************************************** */


    // 
    // Disconnected event
    // 

    /** The possible reasons for a disconnect event */
    public enum DisconnectedReason {
        MY_DISCONNECT   (0),    
        OUT_OF_RANGE    (1),
        PEER_DISCONNECT (2),
        TIMEOUT         (3),
        IO_ERROR        (4);

        public final int reasonValue;
        DisconnectedReason(int reasonValue) {
            this.reasonValue = reasonValue;
        }
    }

    /** Multicast a Disconnected event to all consumers */
    public void serverDisconnected(GATTServerRepresentation server, DisconnectedReason reason) {
        ObjId id = representedServerByObject.get(server);
        if (id == null) {
            throw new IllegalArgumentException("Attempt to disconnect a non-registered GATT server");
        }

        endpoint.postEvent("Disconnected", Disconnected.newBuilder()
                           .setGattHandle(id.toInt64())
                           .setReasonValue(reason.reasonValue)
                           .build());
    }
        


    // 
    // Notification event
    // 

    /** Unicast a notification/indication event to a registered consumer */
    private void characteristicNotified(Peer receiver, GATTCharacteristicRepresentation characteristic, byte[] value) {
        ObjId handle = representedCharacteristicByObject.get(characteristic);
        endpoint.postEvent("Notification", Notification.newBuilder()
                           .setCharacteristic(handle.toInt64())
                           .setValue(ByteString.copyFrom(value))
                           .build(), receiver);
    }


    
    /* ********************************************************************** */
    /*                                                                        */
    /*   Mapping between the PHG ObjID ids and the represented Java objects   */
    /*                                                                        */
    /* ********************************************************************** */

    /** Get the handle for the given GATT server - creating it if necessary */
    public ObjId getHandleForServer(GATTServerRepresentation server) {
        return getHandleForT(server, representedServerByID,  representedServerByObject);
    }

    /** Get the GATT server for he given handle */
    private GATTServerRepresentation getServerFor(long id) {
        return representedServerByID.get(ObjId.fromInt64(id));
    }

    /** Get the handle for the given GATT service - creating it if necessary */
    private ObjId getHandleForService(GATTServiceRepresentation service) {
        return getHandleForT(service, representedServiceByID, representedServiceByObject);
    }

    /** Get the GATT service for he given handle */
    private GATTServiceRepresentation getServiceFor(long id) {
        return representedServiceByID.get(ObjId.fromInt64(id));
    }

    /** Get the handle for the given GATT characteristic - creating it if necessary */
    private ObjId getHandleForCharacteristic(GATTCharacteristicRepresentation characteristic) {
        return getHandleForT(characteristic, representedCharacteristicByID, representedCharacteristicByObject);
    }

    /** Get the GATT characteristic for he given handle */
    private GATTCharacteristicRepresentation getCharacteristicFor(long id) {
        return representedCharacteristicByID.get(ObjId.fromInt64(id));
    }

    /** Get the handle for the given GATT descriptor - creating it if necessary */
    private ObjId getHandleForDescriptor(GATTDescriptorRepresentation descriptor) {
        return getHandleForT(descriptor, representedDescriptorByID, representedDescriptorByObject);
    }

    /** Get the GATT descriptor for he given handle */
    private GATTDescriptorRepresentation getDescriptorFor(long id) {
        return representedDescriptorByID.get(ObjId.fromInt64(id));
    }
    
    private <T> ObjId getHandleForT(T t, Map<ObjId, T> tById, Map<T, ObjId> idByT) {
        ObjId id = idByT.get(t);
        if (id == null) {
            // Unknown t. Create fresh ID
            id = endpoint.getModule().createObjId();
            
            // Insert the mapping in both maps
            if (idByT.put(t,id) != null || tById.put(id,t) != null) {
                // Should never happen
                throw new RuntimeException("Bad mapping happened.");
            }
        }
        return id;
    }


    
    /* ********************************************************************** */
    /*                                                                        */
    /*   Mapping between the interface's BTUUID type and the Java UUID type   */
    /*                                                                        */
    /* ********************************************************************** */

    private final long BT_UUID_MSB = 0x0000000000001000L;
    private final long BT_UUID_LSB = 0x800000805f9b34fbL;
    
    /** Convert the protobuf BTUUID type to a java.util.UUID  */
    private UUID bt2uuid(BTUUID btUUID) {
        long mostSignificant = 0, leastSignificant = 0;
        switch (btUUID.getSizeCase()) {
        case SHORT:
            mostSignificant = btUUID.getShort();
            mostSignificant = (mostSignificant << 32) | BT_UUID_MSB;
            leastSignificant = BT_UUID_LSB;
            break;
        case LONG:
            byte[] longUUID = btUUID.getLong().toByteArray();
            
            if (longUUID.length != 16) {
                throw new IllegalArgumentException("Invalid Long UUID size. Must be 16 bytes long.");
            }

            for (int i = 0; i < 8; i++) mostSignificant = (mostSignificant << 8) | longUUID[i];
            for (int i = 8; i < 16; i++) leastSignificant = (leastSignificant << 8) | longUUID[i];
            
            break;
        default: // SIZE_NOT_SET
            return null;
        }
        return new UUID(mostSignificant, leastSignificant);
    }

    /** Convert the java.util.UUID to a protobuf BTUUID */
    private BTUUID uuid2bt(UUID uuid) {

        BTUUID.Builder builder = BTUUID.newBuilder();
        long mostSignificant = uuid.getMostSignificantBits();
        long leastSignificant = uuid.getLeastSignificantBits();
        
        if (leastSignificant == BT_UUID_LSB && (mostSignificant & 0x00000000FFFFFFFFL) == BT_UUID_MSB) {

            // Short version
            builder.setShort((int)(mostSignificant >> 32));
        } else {

            // Long version

            byte[] bytes = new byte[16];
            for (int i = 7; i >= 0; i--, mostSignificant >>= 8) bytes[i] = (byte)(mostSignificant & 0xFF);
            for (int i = 15; i > 7; i--, leastSignificant >>= 8) bytes[i] = (byte)(leastSignificant & 0xFF);

            builder.setLong(ByteString.copyFrom(bytes));
        }

        return builder.build();
    }



    /* ********************************************************************** */
    /*                                                                        */
    /*    Build the protobuf-equivalent versions of -Representation objects   */
    /*                                                                        */
    /* ********************************************************************** */

    private GattService.Builder buildGattService(GATTServiceRepresentation service) {
        ObjId handle = getHandleForService(service);
        BTUUID btUuid = uuid2bt(service.getType());
        return GattService.newBuilder()
            .setHandle(handle.toInt64())
            .setUuid(btUuid)
            .setPrimary(service.isPrimary());
    }
                                        
    private GattCharacteristic.Builder buildGattCharacteristic(GATTCharacteristicRepresentation characteristic) {
        ObjId handle = getHandleForCharacteristic(characteristic);
        BTUUID btUuid = uuid2bt(characteristic.getType());
        return GattCharacteristic.newBuilder()
            .setHandle(handle.toInt64())
            .setUuid(btUuid)
            .setBroadcast(characteristic.canBroadcast())
            .setRead(characteristic.canRead())
            .setWriteWithoutResponse(characteristic.canWriteWithoutResponse())
            .setWrite(characteristic.canWriteWithResponse())
            .setNotify(characteristic.canNotify())
            .setIndicate(characteristic.canIndicate())
            .setAuthenticatedSignedWrite(characteristic.canAuthenticatedSignedWrite())
            .setExtendedProperties(characteristic.hasExtendedProperties());
    }

    private GattDescriptor.Builder buildGattDescriptor(GATTDescriptorRepresentation descriptor) {

        ObjId handle = getHandleForDescriptor(descriptor);

        GattDescriptor.Type type = GattDescriptor.Type.UNRECOGNIZED;
        if (descriptor instanceof GATTDescriptorRepresentation.CharacteristicExtendedProperties) {
            type = GattDescriptor.Type.CHARACTERISTIC_EXTENDED_PROPERTIES;
        } else if (descriptor instanceof GATTDescriptorRepresentation.CharacteristicUserDescription) {
            type = GattDescriptor.Type.CHARACTERISTIC_USER_DESCRIPTION;
        } else if (descriptor instanceof GATTDescriptorRepresentation.ClientCharacteristicConfiguration) {
            type = GattDescriptor.Type.CLIENT_CHARACTERISTIC_CONFIGURATION;
        } else if (descriptor instanceof GATTDescriptorRepresentation.ServerCharacteristicConfiguration) {
            type = GattDescriptor.Type.SERVER_CHARACTERISTIC_CONFIGURATION;
        } else if (descriptor instanceof GATTDescriptorRepresentation.CharacteristicPresentationFormat) {
            type = GattDescriptor.Type.CHARACTERISTIC_PRESENTATION_FORMAT;
        } else if (descriptor instanceof GATTDescriptorRepresentation.CharacteristicAggregateFormat) {
            type = GattDescriptor.Type.CHARACTERISTIC_AGGREGATE_FORMAT;
        } else if (descriptor instanceof GATTDescriptorRepresentation.UserDefinedDescriptor) {
            type = GattDescriptor.Type.USER_DEFINED_DESCRIPTOR;
        }
        
        return GattDescriptor.newBuilder().setHandle(handle.toInt64()).setType(type);
    }

    private GattDescriptorValue.Builder buildGattDescriptorValue(GATTDescriptorRepresentation descriptor) {

        GattDescriptorValue.Builder builder = GattDescriptorValue.newBuilder();            
        
        if (descriptor instanceof GATTDescriptorRepresentation.CharacteristicExtendedProperties) {
            GATTDescriptorRepresentation.CharacteristicExtendedProperties d =
                (GATTDescriptorRepresentation.CharacteristicExtendedProperties)descriptor;
            builder.setCharacteristicExtendedProperties(GattDescriptorValue.CharacteristicExtendedProperties.newBuilder()
                                                        .setReliableWrite(d.getReliableWriteValue())
                                                        .setWritableAuxiliaries(d.getWritableAuxilliariesValue()));
        } else if (descriptor instanceof GATTDescriptorRepresentation.CharacteristicUserDescription) {
            GATTDescriptorRepresentation.CharacteristicUserDescription d =
                (GATTDescriptorRepresentation.CharacteristicUserDescription)descriptor;
            builder.setCharacteristicUserDescription(GattDescriptorValue.CharacteristicUserDescription.newBuilder()
                                                     .setUserDescription(d.getDescriptionValue()));
        } else if (descriptor instanceof GATTDescriptorRepresentation.ClientCharacteristicConfiguration) {
            GATTDescriptorRepresentation.ClientCharacteristicConfiguration d =
                (GATTDescriptorRepresentation.ClientCharacteristicConfiguration)descriptor;
            builder.setClientCharacteristicConfiguration(GattDescriptorValue.ClientCharacteristicConfiguration.newBuilder()
                                                         .setNotification(d.getNotificationValue())
                                                         .setIndication(d.getIndicationValue()));
        } else if (descriptor instanceof GATTDescriptorRepresentation.ServerCharacteristicConfiguration) {
            GATTDescriptorRepresentation.ServerCharacteristicConfiguration d =
                (GATTDescriptorRepresentation.ServerCharacteristicConfiguration)descriptor;
            builder.setServerCharacteristicConfiguration(GattDescriptorValue.ServerCharacteristicConfiguration.newBuilder()
                                                         .setBroadcast(d.getBroadcastValue()));
        } else if (descriptor instanceof GATTDescriptorRepresentation.CharacteristicPresentationFormat) {
            GATTDescriptorRepresentation.CharacteristicPresentationFormat d =
                (GATTDescriptorRepresentation.CharacteristicPresentationFormat)descriptor;
            builder.setCharacteristicPresentationFormat(GattDescriptorValue.CharacteristicPresentationFormat.newBuilder()
                                                        .setFormat(ByteString.copyFrom(d.getFormatValue())));
        } else if (descriptor instanceof GATTDescriptorRepresentation.CharacteristicAggregateFormat) {
            GATTDescriptorRepresentation.CharacteristicAggregateFormat d =
                (GATTDescriptorRepresentation.CharacteristicAggregateFormat)descriptor;
            for (GATTDescriptorRepresentation.CharacteristicPresentationFormat format : d.getFormatValue()) {
                builder.setCharacteristicAggregateFormat(GattDescriptorValue.CharacteristicAggregateFormat.newBuilder()
                                                         .addDescriptors(getHandleForDescriptor(format).toInt64()));
            }
        } else if (descriptor instanceof GATTDescriptorRepresentation.UserDefinedDescriptor) {
            GATTDescriptorRepresentation.UserDefinedDescriptor d =
                (GATTDescriptorRepresentation.UserDefinedDescriptor)descriptor;
            builder.setUserDefinedDescriptor(GattDescriptorValue.UserDefinedDescriptor.newBuilder()
                                             .setType(uuid2bt(d.getUUID()))
                                             .setValue(ByteString.copyFrom(d.getValue())));
        }
        
        return builder;
    }
}
