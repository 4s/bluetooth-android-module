package dk.s4.phg.messages.support.gatt;

import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

import dk.s4.phg.baseplate.CoreError;

public interface GATTServiceRepresentation {

    UUID getType();
    boolean isPrimary();

    void findIncludedServices(Consumer<CoreError> callback);

    Set<? extends GATTServiceRepresentation> getIncludedServices();

    /**
     * @param characteristicType The characteristic UUID to search for
     *                           ("Discover Characteristic by
     *                           UUID"). May be null, which will be
     *                           interpreted as "Discover All
     *                           Characteristics of a Service".
     */
    void discoverCharacteristics(UUID characteristicType, Consumer<CoreError> callback);

    Set<? extends GATTCharacteristicRepresentation> getCharacteristics();

}
