package dk.s4.phg.messages.support.bluetooth_device_control;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

import dk.s4.phg.baseplate.Callback;
import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.baseplate.EventImplementation;
import dk.s4.phg.baseplate.FunctionImplementation;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.MetaData;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.baseplate.ObjId;
import dk.s4.phg.baseplate.Peer;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.BluetoothPairing;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.BluetoothPairingSuccess;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.Bond;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.BondSuccess;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.SetStandardPasskey;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.Unbond;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.UnbondSuccess;
import dk.s4.phg.messages.support.device.BluetoothDevice;
import dk.s4.phg.messages.support.device.BluetoothDeviceRepresentation;




/**
 * Implementation of the producer-side of the BluetoothDeviceControl
 * interface.
 *
 * The corresponding business functionality must be available in an
 * object implementing the BluetoothDeviceControlProducer interface
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public class BluetoothDeviceControlProducerEndpoint
    implements BluetoothDeviceControlProducer.BluetoothPairing {

    /**
     * The endpoint
     */
    private InterfaceEndpoint endpoint;

    /**
     * Keeping track of the source of bonding requests in order to
     * direct pairing requests back to the source.
     */
    private Map<ObjId, Peer> activeBondings = new HashMap<>();
    
    /**
     * BluetoothDeviceControl interface producer endpoint constructor
     *
     * @param producer The business functionality implementation
     * @param epFactory The EndpointFactory of the parent module
     * @param deviceFactory The factory managing DeviceTransport instances
     */
    public BluetoothDeviceControlProducerEndpoint(BluetoothDeviceControlProducer producer,
         ModuleBase.EndpointFactory epFactory,
         BluetoothDeviceRepresentation.BluetoothDeviceRepresentationFactory
         deviceFactory) {

        endpoint = epFactory.implementsProducerInterface("BluetoothDeviceControl");

        endpoint.addFunctionHandler("Bond",
                new FunctionImplementation<Bond, BondSuccess>() {
                    @Override
                    public void apply(Bond args, Callback<BondSuccess> callback, MetaData meta) {

                        final ObjId handle = ObjId.fromInt64(args.getHandle());
                        if (handle == null) {
                            callback.error(endpoint.createError(0, "FIXME: error no handle"), false);
                            return;
                        }
                        BluetoothDeviceRepresentation device = deviceFactory.lookup(handle);
                        if (device == null) {
                            callback.error(endpoint.createError(0, "FIXME: unknown device"), false);
                            return;
                        }
                        try {
                            // Register bonding session for pairing requests
                            activeBondings.put(device.getHandle(), meta.sender());
                            producer.bond(device, new Consumer<CoreError>() {
                                public void accept(CoreError e) {
                                    // Pairing requests no longer allowed
                                    activeBondings.remove(device.getHandle());
                                    if (e != null) {
                                        CoreError e2 = endpoint.createError(0,
                                                "FIXME: Exception in BluetoothDeviceControlProducer.bond()");
                                        e2.causedBy = e;
                                        callback.error(e2, false);
                                    } else {
                                        callback.success(BondSuccess.newBuilder().build(), false);
                                    }
                                }
                            });
                        } catch (RuntimeException e) {
                            // Pairing requests no longer allowed
                            activeBondings.remove(device.getHandle());
                            callback.error(endpoint.createError(0,
                                    "FIXME: RuntimeException in BluetoothDeviceControlProducer.bond()"), false);
                        }
                    }
                });

        endpoint.addFunctionHandler("Unbond",
                new FunctionImplementation<Unbond, UnbondSuccess>() {
                    @Override
                    public void apply(Unbond args, Callback<UnbondSuccess> callback, MetaData meta) {

                        final ObjId handle = ObjId.fromInt64(args.getHandle());
                        if (handle == null) {
                            callback.error(endpoint.createError(0, "FIXME: error no handle"), false);
                            return;
                        }
                        BluetoothDeviceRepresentation device = deviceFactory.lookup(handle);
                        if (device == null) {
                            callback.error(endpoint.createError(0,
                                    "FIXME: unknown device"), false);
                            return;
                        }
                        try {
                            producer.unbond(device, new Consumer<CoreError>() {
                                public void accept(CoreError e) {
                                    if (e != null) {
                                        CoreError e2 = endpoint.createError(0,
                                                "FIXME: Exception in BluetoothDeviceControlProducer.unbond()");
                                        e2.causedBy = e;
                                        callback.error(e2, false);
                                    } else {
                                        callback.success(UnbondSuccess.newBuilder().build(), false);
                                    }
                                }
                            });
                        } catch (RuntimeException e) {
                            callback.error(endpoint.createError(0,
                                    "FIXME: RuntimeException in BluetoothDeviceControlProducer.unbond()"), false);
                        }
                    }
                });

        endpoint.addEventHandler("SetStandardPasskey",
                new EventImplementation<SetStandardPasskey>() {
                    @Override
                    public void accept(SetStandardPasskey args, MetaData meta) {
                        final ObjId handle = ObjId.fromInt64(args.getHandle());
                        // Ignore illegal argument
                        if (handle == null) return;

                        BluetoothDeviceRepresentation device = deviceFactory.lookup(handle);
                        // Ignore unknown device
                        if (device == null) return;

                        String passkey = args.getPasskey();

                        // Set the standard passkey of the device
                        device.setStandardPasskey(passkey);

                        // ... and notify
                        producer.setStandardPasskey(device);
                    }
                });
    }
    

    
    /**
     * Create a callback for handling responses from the pairing
     * function calls.
     */
    private <T> Callback<BluetoothPairingSuccess>
        pairingCallback(BluetoothPairingSuccess.MethodCase method,
                        Consumer<T> consumer,
                        Function<BluetoothPairingSuccess,T> getValue,
                        T failValue) {


        return new Callback<BluetoothPairingSuccess>() {
            boolean alive = true;

            @Override
            public void success(BluetoothPairingSuccess s, boolean lastCall) {
                // Guard against multiple answers
                if (!alive) return;
                alive = false;

                // Check the type of answer matches the request
                if (s.getMethodCase() == method) {
                    consumer.accept(getValue.apply(s));
                } else {
                    // Error, the answer did not match the question
                    consumer.accept(failValue);
                }
            }

            @Override
            public void error(CoreError e, boolean lastCall) {
                // Guard against multiple answers
                if (!alive) return;
                alive = false;

                // FIXME: We ignore the contents of the error
                //        response for now. The reason for the
                //        error does not seem relevant for any
                //        further actions. But perhaps we should
                //        log it?
                consumer.accept(failValue);
            }
        };
    }
    
    /**
     * Legacy (Bluetooth v1) pairing.
     *
     * A.k.a. "PIN code" although a full UTF-8 string of 16 bytes
     * is in fact allowed. In practice (ASCII representation) of
     * a string of digits are almost always used.
     */
    @Override
    public void pairingLegacy(BluetoothDevice bd,
                              Consumer<String> pinCode) {
        Peer dest = activeBondings.get(bd.getHandle());
        if (dest == null)
            throw new IllegalStateException("Pairing requests are only allowed when bonding");

        endpoint.callFunction("BluetoothPairing",
             BluetoothPairing.newBuilder()
             .setHandle(bd.getHandle().toInt64())
             .setMethod(BluetoothPairing.Method.LEGACY)
             .build(),
             dest,
             pairingCallback(BluetoothPairingSuccess.MethodCase.LEGACY,
                             pinCode, BluetoothPairingSuccess::getLegacy, null)
             );
    }

    /**
     * Just-works pairing.
     *
     * "Press OK to accept pairing or Cancel to abort"
     */
     @Override
     public void pairingJustWorks(BluetoothDevice bd,
                                  Consumer<Boolean> accept) {
        Peer dest = activeBondings.get(bd.getHandle());
        if (dest == null)
            throw new IllegalStateException("Pairing requests are only allowed when bonding");

        endpoint.callFunction("BluetoothPairing",
             BluetoothPairing.newBuilder()
             .setHandle(bd.getHandle().toInt64())
             .setMethod(BluetoothPairing.Method.JUST_WORKS)
             .build(),
             dest,
             pairingCallback(BluetoothPairingSuccess.MethodCase.ACCEPT,
                             accept, BluetoothPairingSuccess::getAccept, false)
             );
     }

    /**
     * Numeric comparison pairing.
     *
     * A 6 digit (base-10) number is presented to the user on both
     * devices. The user must compare the numbers and accept or
     * reject the pairing.
     */
     @Override
     public void pairingNumericComparison(BluetoothDevice bd, long value,
                                          Consumer<Boolean> accept) {
        Peer dest = activeBondings.get(bd.getHandle());
        if (dest == null)
            throw new IllegalStateException("Pairing requests are only allowed when bonding");

        endpoint.callFunction("BluetoothPairing",
             BluetoothPairing.newBuilder()
             .setHandle(bd.getHandle().toInt64())
             .setMethod(BluetoothPairing.Method.NUMERIC_COMPARISON)
             .setNumericComparisonValue(value)
             .build(),
             dest,
             pairingCallback(BluetoothPairingSuccess.MethodCase.ACCEPT,
                             accept, BluetoothPairingSuccess::getAccept, false)
             );
     }

    /**
     * Passkey pairing.
     *
     * The user is asked to enter a 6 digit (base-10) number.
     */
     @Override
     public void pairingPasskey(BluetoothDevice bd,
                                Consumer<Long> passkey) {
        Peer dest = activeBondings.get(bd.getHandle());
        if (dest == null)
            throw new IllegalStateException("Pairing requests are only allowed when bonding");

        endpoint.callFunction("BluetoothPairing",
             BluetoothPairing.newBuilder()
             .setHandle(bd.getHandle().toInt64())
             .setMethod(BluetoothPairing.Method.PASSKEY)
             .build(),
             dest,
             pairingCallback(BluetoothPairingSuccess.MethodCase.PASSKEY,
                             passkey, BluetoothPairingSuccess::getPasskey, null)
             );
     }    
}
