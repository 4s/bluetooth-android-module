package dk.s4.phg.messages.support.device_control;

import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.messages.support.device.DeviceTransportRepresentation;

import java.util.function.Consumer;




/**
 * The business functionality covering the provided
 * interfaces/DeviceControl.proto interface.
 *
 * This interface defines the handles to be implemented by the
 * business part of a DeviceControl producer module.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public interface DeviceControlProducer
                 <T extends DeviceTransportRepresentation> {

    /**
     * Represents a device search, collecting search results.
     *
     * This object represents a device search request, started using
     * the search() method. As long as stopSearch() has not been
     * called, more search results can be added to the result set of
     * this active search.
     */
    public interface SearchResultCollector
                     <T extends DeviceTransportRepresentation> {
        /**
         * Test if this search request is still active.
         * @return This search has been started using search() and has
         *         not yet been stopped using stopSearch()
         */
        boolean isActive();

        /**
         * Add a device to this (active) search request.
         *
         * If this search request is no longer active, this is a no-op.
         *
         * @param device The device found during the search.
         */
        void addResult(T device);
    }

    /**
     * Request start searching for new devices.
     *
     * Some device transport types, such as Bluetooth, support
     * searching for new devices. This method will start such a
     * search. Device transports that do not support searching may
     * just provide an empty implementation.
     *
     * Every search must be treated as an independent entity (they may
     * originate from different consumers), so even if a device was
     * found and reported once in a search, it must be reported again
     * in other (subsequent as well as concurrent) searches - if it is
     * still available, that is.
     *
     * @param SearchResultCollector An object representing the search
     *                              and used to collect search
     *                              results.
     */
    void search(SearchResultCollector<T> collector);

    /**
     * Stop a search started with search()
     *
     * Device transports that do not support searching may just
     * provide an empty implementation.
     *
     * @param SearchResultCollector An object representing the search
     *                              and used to collect search
     *                              results.
     */
    void stopSearch(SearchResultCollector<T> collector);

    /**
     * Request the latest information regarding the given device.
     *
     * The DeviceControlProducer should check and update any and all
     * properties of the provided device (using the bulkChangeMe()
     * method) to make sure it is up-to-date. When done, the callback
     * must be executed.
     * @param deviceTransport The device
     * @param callback Invoke when the update is finished. Provide a
     *                 null on success and a CoreError if the device
     *                 could not be created. This may return a "Device
     *                 not available" error with this CoreError as
     *                 cause.
     */
    void solicitDeviceTransport(T deviceTransport,
                                Consumer<CoreError> callback);
    
}

