package dk.s4.phg.messages.support.gatt;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

import dk.s4.phg.baseplate.CoreError;

public interface GATTDescriptorRepresentation {
    
    void readValue(Consumer<CoreError> callback);

    interface CharacteristicExtendedProperties extends GATTDescriptorRepresentation {
        boolean getReliableWriteValue();
        boolean getWritableAuxilliariesValue();
    }

    interface CharacteristicUserDescription extends GATTDescriptorRepresentation {
        String getDescriptionValue();
        void writeValue(String descriptionValue, Consumer<CoreError> callback);
    }

    interface ClientCharacteristicConfiguration extends GATTDescriptorRepresentation {
        boolean getNotificationValue();
        boolean getIndicationValue();
        // Writing not allowed. We use the enableNI() and disableNI()
        // methods of the characteristic to write this descriptor
    }

    interface ServerCharacteristicConfiguration extends GATTDescriptorRepresentation {
        boolean getBroadcastValue();
        void writeValue(boolean broadcastValue, Consumer<CoreError> callback);
    }

    interface CharacteristicPresentationFormat extends GATTDescriptorRepresentation {
        // Currently we just provide the raw format description
        byte[] getFormatValue();
    }

    interface CharacteristicAggregateFormat extends GATTDescriptorRepresentation {
         List<? extends CharacteristicPresentationFormat> getFormatValue();
    }

    interface UserDefinedDescriptor extends GATTDescriptorRepresentation {
        UUID getUUID();
        byte[] getValue();
        void writeValue(byte[] value, Consumer<CoreError> callback);
    }
}
