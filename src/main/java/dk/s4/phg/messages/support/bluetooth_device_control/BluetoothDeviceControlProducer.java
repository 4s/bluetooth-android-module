package dk.s4.phg.messages.support.bluetooth_device_control;

import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.messages.support.device.BluetoothDevice;
import dk.s4.phg.messages.support.device.BluetoothDeviceRepresentation;

import java.util.function.Consumer;




/**
 * The business functionality covering the provided
 * interfaces/BluetoothDeviceControl.proto interface.
 *
 * This interface defines the handles to be implemented by the
 * business part of a bluetooth module in order to cover the
 * BluetoothDeviceControl interface.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public interface BluetoothDeviceControlProducer {
    /**
     * Request to perform a bond with the given device.
     * @param device The device to bond
     * @param callback Invoke this with null as argument if the
     *                 bonding succeeded or with a CoreError object
     *                 representing an error, if the bonding failed.
     */
    // FIXME: The bond call may fail in different ways - unknown
    // device, wrong pin, bluetooth not available - just to name a
    // few. There should be different error codes at the interface
    // level, which means that a simple Callback is insufficient for
    // the callback. It should be changed to a class with the
    // different error options
    void bond(BluetoothDeviceRepresentation device,
              Consumer<CoreError> callback);

    /**
     * Request to unbond the given device.
     * @param device The device to unbond
     * @param callback Invoke this with null as argument if the
     *                 bonding succeeded or with a CoreError object
     *                 representing an error, if the bonding failed.
     */
    // FIXME: Same as bond() above
    void unbond(BluetoothDeviceRepresentation device,
                Consumer<CoreError> callback);

    /**
     * Notfication that the standard passkey has been set/changed for
     * this device. 
     *
     * The updated key may be inspected using
     * device.getStandardPasskey()
     *
     * @param device The device affected by the announced update.
     */
    void setStandardPasskey(BluetoothDeviceRepresentation device);

    /**
     * Access the BluetoothPairing pairing process facilitator.
     *
     * The BluetoothPairing interface is implemented by the
     * BluetoothDeviceControlProducerEndpoint. A reference to this
     * object is provided by this method, which the implementor of the
     * BluetoothdeviceControlProducer is supposed to leave abstract.
     */
    BluetoothPairing getBluetoothPairing();
    
    /**
     * Handler of the Bluetooth pairing process
     *
     * This interface is implemented by the
     * BluetoothDeviceControlProducerEndpoint and accessed using the
     * method getBluetoothPairing(). The implementer of the
     * BluetoothDeviceControlProducer interface should keep this
     * method abstract and leave it to the creator to override this
     * method to return the endpoint instance.
     *
     * The BluetoothDeviceControlProducer uses the BluetoothPairing
     * for the user interaction associated with the pairing process
     * (provided that the pairing process could not be performed using
     * the standard passkey). The methods of this interface may ONLY
     * be used from the implementation of the bond() method, i.e
     * between the invocation of bond() and the invocation of the
     * Consumer callback provided as argument to bond(). They will
     * throw an IllegalStateException if bonding with the device is
     * not underway.
     */
    public interface BluetoothPairing {

        /**
         * Legacy (Bluetooth v1) pairing.
         *
         * A.k.a. "PIN code" although a full UTF-8 string of 16 bytes
         * is in fact allowed. In practice (ASCII representation) of
         * a string of digits are almost always used.
         * @param device  The device we want to pair with.
         * @param pinCode Returning the pin code or null if the
         *                pairing was cancelled/rejected by the user
         *                or an error occured
         */
        void pairingLegacy(BluetoothDevice device,
                           Consumer<String> pinCode);

        /**
         * Just-works pairing.
         *
         * "Press OK to accept pairing or Cancel to abort"
         * @param device  The device we want to pair with.
         * @param accept  Returning true for accept or false if the
         *                pairing was cancelled/rejected by the user
         *                or an error occured
         */
        void pairingJustWorks(BluetoothDevice device,
                              Consumer<Boolean> accept);

        /**
         * Numeric comparison pairing.
         *
         * A 6 digit (base-10) number is presented to the user on both
         * devices. The user must compare the numbers and accept or
         * reject the pairing.
         * @param device  The device we want to pair with.
         * @param value   The 6 digit code to present to the user
         * @param accept  Returning true for accept or false if the
         *                pairing was cancelled/rejected by the user
         *                or an error occured
         */
        void pairingNumericComparison(BluetoothDevice device, long value,
                                      Consumer<Boolean> accept);

        /**
         * Passkey pairing.
         *
         * The user is asked to enter a 6 digit (base-10) number.
         * @param device  The device we want to pair with.
         * @param accept  Returning the passkey or null if the
         *                pairing was cancelled/rejected by the user
         *                or an error occured
         */
        void pairingPasskey(BluetoothDevice device,
                            Consumer<Long> passkey);
    }
    
}

