package dk.s4.phg.messages.support.gatt;

import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.BiConsumer;

import dk.s4.phg.baseplate.CoreError;

public interface GATTCharacteristicRepresentation {

    UUID getType();
    boolean canBroadcast();
    boolean canRead();
    boolean canWriteWithoutResponse();
    boolean canWriteWithResponse();
    boolean canNotify();
    boolean canIndicate();
    boolean canAuthenticatedSignedWrite();
    boolean hasExtendedProperties();

    void discoverDescriptors(Consumer<CoreError> callback);

    Set<? extends GATTDescriptorRepresentation> getDescriptors();

    void readValue(Consumer<CoreError> callback);

    byte[] getValue();

    void writeValueWithResponse(byte[] value, Consumer<CoreError> callback);
    void writeValueWithoutResponse(byte[] value, Consumer<CoreError> callback);

    /// Must also update the CCC Descriptor
    void enableNI(Consumer<CoreError> callback, BiConsumer<GATTCharacteristicRepresentation, byte[]> niHandler);
    void disableNI(Consumer<CoreError> callback);
}
