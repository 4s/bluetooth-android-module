package dk.s4.phg.messages.support.gatt;


public enum GATTError {

    /** A mandatory argument is missing or not valid */
    INVALID_ARGUMENT     ( 1, "Invalid or missing argument"),

    /** Attempt to connect to an already connected server */
    ALREADY_CONNECTED    ( 2, "Already connected"),

    /** A request (read or write) timed out */
    TIMEOUT              ( 3, "Timeout"),

    /** The attribute handle given was not valid on this server */
    INVALID_HANDLE       ( 4, "Invalid Handle"),

    /** The attribute cannot be read or written */
    NOT_PERMITTED        ( 5, "Not Permitted"),

    /** The attribute requires authentication before it can be read or written */
    BAD_AUTHENTICATION   ( 6, "Insufficient Authentication"),

    /** The attribute requires authorization before it can be read or written */
    BAD_AUTHORIZATION    ( 7, "Insufficient Authorization"),

    /** The attribute requires encryption before it can be read or written, or the key size is insufficient */
    BAD_ENCRYPTION       ( 8, "Insufficient Encryption"),

    /** Attribute server does not support the request received from the client */
    NOT_SUPPORTED        ( 9, "Request Not Supported"),

    /** Too many prepare writes have been queued */
    QUEUE_FULL           (10, "Prepare Queue Full"),

    /** The attribute value length is invalid for the operation */
    BAD_ATTRIBUTE        (11, "Invalid Attribute Value or Length"),

    /** The operation aborted by closing the connection */
    ABORTED              (12, "Operation Aborted"),

    /** Request was sent to a server which is currently not connected */
    NOT_CONNECTED        (13, "Not Connected"),
    
    /**  A general unspecified I/O error caused the operation to fail */
    IO_ERROR             (31, "Communication error");



    public final int errorCode;
    public final String errorMessage;
    private GATTError(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}
